package com.procuro.androidtruckerlite;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;

import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.Circle;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.procuro.androidlocationservice.LocationService;
import com.procuro.apimmdatamanagerlib.AssignmentInfo;
import com.procuro.apimmdatamanagerlib.SdrGisSite;
import com.procuro.apimmdatamanagerlib.SdrPointOfInterest;
import com.procuro.apimmdatamanagerlib.SdrStop;
import com.procuro.apimmdatamanagerlib.aPimmDataManager;

import java.util.ArrayList;


public class MapFragment extends Fragment implements OnMapReadyCallback {


    private GoogleMap mMap;

    aPimmDataManager dataManager = aPimmDataManager.getInstance();
    final TruckerDataManager truckerDataManager = TruckerDataManager.getInstance();
    LocationManager locationManager;
    private static final int REQUEST_LOCATION_PERMISSION = 1;
    Marker marker;
    ArrayList<LatLng> markers = new ArrayList<LatLng>();
    LocationListener locationListener;

    CameraUpdate cu = null;

    LatLngBounds.Builder builder = new LatLngBounds.Builder();

    ImageButton move_camera , add_poi , routes;
    private ArrayList<Routes> mDataSet;
    private Context mContext;

    public MapFragment() {
        // Required empty public constructor
    }
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        final View rootView = inflater.inflate(R.layout.activity_maps, container, false);

        move_camera = rootView.findViewById(R.id.move_camera);


        move_camera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showAllMapMarkers();
            }
        });

        SupportMapFragment mapFragment = (SupportMapFragment) getChildFragmentManager()
                .findFragmentById(R.id.maps);
        mapFragment.getMapAsync(MapFragment.this);

        return rootView;
    }

    @Override
    public void onResume() {
        super.onResume();

        if (ActivityCompat.checkSelfPermission(getContext().getApplicationContext(),
                Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(getActivity(), new String[]
                            {Manifest.permission.ACCESS_FINE_LOCATION},
                    REQUEST_LOCATION_PERMISSION);
        }
    }

    private void setLocationChangeLister() {
        LocationService.getInstance().setUserLocationChangeListener(new LocationService.OnUserLocationChangeListener() {
            @Override
            public void onUserLocationChanged(Location location) {
                TruckerDataManager.getInstance().postGPSToServer(location.getLatitude(), location.getLongitude());
            }
        });
    }

    public void create_marks() {

        ArrayList <Routes> routes = TruckerDataManager.getInstance().getRoutes();
        Routes item = null;
         ArrayList<SdrGisSite> Sites = truckerDataManager.getSdrReport().Hierarchy.GIS.Sites;
         SdrGisSite site;
         ArrayList<SdrStop> Stops = truckerDataManager.getSdrReport().Hierarchy.GIS.Stops;
         SdrStop stop = null;
         ArrayList<SdrPointOfInterest> Poi = truckerDataManager.getSdrReport().Hierarchy.GIS.PointsOfInterest;
         SdrPointOfInterest poi = null;

        int count = 0;
        int count_for_poi = 0;
        int count_for_stops = 0;
        int counter_s = 0;
        int counter_p = 0;
        String site_name;
        double lat, lon;
        int map_icon = 0;


        for (int i = 0; i < routes.size()-1; i++) {
            item =routes.get(i);
            if (item.getSite_desc().equalsIgnoreCase("Site_departure")){
                site_name = item.getRoute_name();
                LatLng latLng = new LatLng(item.getLat(), item.getLongt());
                markers.add(latLng);

                if (Poi.size() > 0 || item.getDeparture_time() != null) {
                    map_icon = R.drawable.map_dc_blue;
                } else {
                    map_icon = R.drawable.map_dc_black;
                }

                marker = mMap.addMarker(new MarkerOptions()
                        .position(latLng)
                        .title(site_name)
                        .icon(BitmapDescriptorFactory.fromResource(map_icon)

                        )
                );
            }else if (item.getSite_desc().equalsIgnoreCase("Poi")){
                site_name = item.getRoute_name();
                LatLng latLng = new LatLng(item.getLat(),item.getLongt());
                markers.add(latLng);
                System.out.println("POI POSITION : " + latLng);
                if (item.getPoi_type() == 50) {
                    map_icon = R.drawable.map_start_point;
                } else if (item.getPoi_type() == 9) {
                    map_icon = R.drawable.map_poi_coffee;
                } else if (item.getPoi_type() == 0) {
                    map_icon = R.drawable.map_poi_delivery;
                } else if (item.getPoi_type() == 2) {
                    map_icon = R.drawable.map_poi_fuel;
                } else if (item.getPoi_type() == 3) {
                    map_icon = R.drawable.map_poi_plant;
                    //DeliveryPOIType_OutOfTerritory = 4
                }  else if (item.getPoi_type() == 5) {
                    map_icon = R.drawable.map_poi_rest;
                } else if (item.getPoi_type() == 6) {
                    map_icon = R.drawable.map_poi_inspection;
                } else if (item.getPoi_type() == 7) {
                    map_icon = R.drawable.map_poi_service;
                } else if (item.getPoi_type() == 8) {
                    map_icon = R.drawable.map_poi_layover;
                } else if (item.getPoi_type() == 11) {
                    map_icon = R.drawable.map_poi_traffic;
                } else if (item.getPoi_type() == 13) {
                    map_icon = R.drawable.map_poi_wash;
                }  else if (item.getPoi_type() == 51) {
                    map_icon = R.drawable.map_poi_end_point;
                } else{
                    map_icon = R.drawable.map_poi_undefined;
                }

                marker=  mMap.addMarker(new MarkerOptions()
                        .position(latLng)
                        .title(site_name)
                        .icon(BitmapDescriptorFactory.fromResource(map_icon))
                );
            }else if (item.getSite_desc().equalsIgnoreCase("Stop")){
                site_name = item.getRoute_name();
                LatLng latLng = new LatLng(item.getLat(),item.getLongt());
                markers.add(latLng);
                if (item.getArrival_time() != null) {
                    map_icon = R.drawable.map_store_blue;
                    if (item.getDeparture_time() != null) {
                        map_icon = R.drawable.map_store_blue_green_check;
                        if (item.getDelivery_confirmation_time() == null) {
                            map_icon = R.drawable.map_store_blue_red_check;
                        }
                    } else {
                        if (item.getDelivery_confirmation_time() != null) {
                            map_icon = R.drawable.map_store_blue_gray_check;
                        }
                    }
                } else {
                    map_icon = R.drawable.map_store_gray;
                }
                marker = mMap.addMarker(new MarkerOptions()
                        .position(latLng)
                        .title(site_name)
                        .icon(BitmapDescriptorFactory.fromResource(map_icon))
                        .visible(true)
                );
            }
            count++;
        }



//        for (int i = 0; i < Stops.size() + Sites.size() + Poi.size(); i++) {
//
//            if (count == 0) {
//                site = Sites.get(0);
//                site_name = site.SiteName;
//                LatLng latLng = new LatLng(site.LatLon.Lat, site.LatLon.Lon);
//                markers.add(latLng);
//
//                if (Poi.size() > 0) {
//                    map_icon = R.drawable.map_dc_blue;
//                } else {
//                    map_icon = R.drawable.map_dc_black;
//                }
//
//               marker = mMap.addMarker(new MarkerOptions()
//                        .position(latLng)
//                        .title(site_name)
//                        .icon(BitmapDescriptorFactory.fromResource(map_icon)
//
//                        )
//                );
//                count++;
//
//            } else if (count < Poi.size() + Sites.size()) {
//                poi = Poi.get(counter_p);
//                site_name = poi.Label;
//                LatLng latLng = new LatLng(poi.LatLon.Lat, poi.LatLon.Lon);
//                markers.add(latLng);
//                System.out.println("POI POSITION : " + latLng);
//
//                if (poi.Type.getValue() == 50) {
//                    map_icon = R.drawable.map_start_point;
//                } else if (poi.Type.getValue() == 9) {
//                    map_icon = R.drawable.map_poi_coffee;
//                } else if (poi.Type.getValue() == -1) {
//                    map_icon = R.drawable.map_poi_undefined;
//                } else if (poi.Type.getValue() == 0) {
//                    map_icon = R.drawable.map_poi_delivery;
//                } else if (poi.Type.getValue() == 1) {
//                    map_icon = R.drawable.map_poi_undefined;
//                } else if (poi.Type.getValue() == 2) {
//                    map_icon = R.drawable.map_poi_fuel;
//                } else if (poi.Type.getValue() == 3) {
//                    map_icon = R.drawable.map_poi_plant;
//                    //DeliveryPOIType_OutOfTerritory = 4
//                } else if (poi.Type.getValue() == 4) {
//                    map_icon = R.drawable.map_poi_undefined;
//                } else if (poi.Type.getValue() == 5) {
//                    map_icon = R.drawable.map_poi_rest;
//                } else if (poi.Type.getValue() == 6) {
//                    map_icon = R.drawable.map_poi_inspection;
//                } else if (poi.Type.getValue() == 7) {
//                    map_icon = R.drawable.map_poi_service;
//                } else if (poi.Type.getValue() == 8) {
//                    map_icon = R.drawable.map_poi_layover;
//                } else if (poi.Type.getValue() == 10) {
//                    map_icon = R.drawable.map_poi_undefined;
//                } else if (poi.Type.getValue() == 11) {
//                    map_icon = R.drawable.map_poi_traffic;
//                } else if (poi.Type.getValue() == 13) {
//                    map_icon = R.drawable.map_poi_wash;
//                } else if (poi.Type.getValue() == 20) {
//                    map_icon = R.drawable.map_poi_undefined;
//                } else if (poi.Type.getValue() == 21) {
//                    map_icon = R.drawable.map_poi_undefined;
//                } else if (poi.Type.getValue() == 51) {
//                    map_icon = R.drawable.map_poi_end_point;
//                } else if (poi.Type.getValue() == 60) {
//                    map_icon = R.drawable.map_poi_undefined;
//                    //DeliveryPOIType_DCFacility = 61,
//                } else if (poi.Type.getValue() == 61) {
//                    map_icon = R.drawable.map_poi_undefined;
//                    //DeliveryPOIType_StoppedWhileUnassigned = 62,
//                } else if (poi.Type.getValue() == 62) {
//                    map_icon = R.drawable.map_poi_undefined;
//                } else if (poi.Type.getValue() == 100) {
//                    map_icon = R.drawable.map_poi_undefined;
//                } else if (poi.Type.getValue() == 101) {
//                    map_icon = R.drawable.map_poi_undefined;
//                }
//                marker=  mMap.addMarker(new MarkerOptions()
//                        .position(latLng)
//                        .title(site_name)
//                        .icon(BitmapDescriptorFactory.fromResource(map_icon))
//                );
//                counter_p++;
//                count++;
//            } else if (count <= Sites.size() + Poi.size() + Stops.size()) {
//                System.out.println("COunter stop : " + counter_s);
//                stop = Stops.get(counter_s);
//                site_name = stop.SiteName;
//                LatLng latLng = new LatLng(stop.LatLon.Lat, stop.LatLon.Lon);
//                markers.add(latLng);
//
//                if (stop.ArrivalTime != null) {
//                    map_icon = R.drawable.map_store_blue;
//                    if (stop.DepartureTime != null) {
//                        map_icon = R.drawable.map_store_blue_green_check;
//                        if (stop.DeliveryConfirmationTime == null) {
//                            map_icon = R.drawable.map_store_blue_red_check;
//                        }
//                    } else {
//                        if (stop.DeliveryConfirmationTime != null) {
//                            map_icon = R.drawable.map_store_blue_gray_check;
//                        }
//                    }
//                } else {
//                    map_icon = R.drawable.map_store_gray;
//                }
//                marker = mMap.addMarker(new MarkerOptions()
//                        .position(latLng)
//                        .title(site_name)
//                        .icon(BitmapDescriptorFactory.fromResource(map_icon))
//                        .visible(true)
//                );
//                count++;
//                counter_s++;
//
//            }
//        }

    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        if (checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED && checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }

        ArrayList<AssignmentInfo> assignmentInfos = truckerDataManager.assignmentInfo();
        if (assignmentInfos.size() >0){
            create_marks();
            for (int i = 0; i < markers.size(); i++) {
                builder.include(markers.get(i));
            }

            googleMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
                @Override
                public boolean onMarkerClick(Marker markers) {
                    String title = (String) markers.getTitle();
                    ArrayList<StopListItem> stopListItems = TruckerDataManager.getInstance().getStopListItems();
                    for (StopListItem stopListItem : stopListItems) {
                        if(title.equalsIgnoreCase(stopListItem.siteName)){
                            System.out.println("TITLE : " + title);
                            System.out.println("ADDRESS : " + stopListItem.address);
                            System.out.println("ARRIVAL TIME: " +stopListItem.sched_arrival_time);
                        }

                    }
                    return false;
                }
            });
        }

        mMap.setMyLocationEnabled(true);
        mMap.getUiSettings().setMyLocationButtonEnabled(false);
        mMap.getUiSettings().setCompassEnabled(false);

        setLocationChangeLister();
        showAllMapMarkers();
        createcircle();
    }

    private void showAllMapMarkers() {
        LatLngBounds bounds = builder.build();
        int padding = 200;
        CameraUpdate cu = CameraUpdateFactory.newLatLngBounds(bounds, padding);
        mMap.animateCamera(cu);
    }

    public void createcircle(){
        ArrayList<Routes>routesArrayList = TruckerDataManager.getInstance().getRoutes();
        ArrayList<Circle>circleArrayList = new ArrayList<>();
        for (Routes item: routesArrayList) {
            LatLng latLng = new LatLng(item.getLat(),item.getLongt());
             mMap.addCircle(new CircleOptions()
                    .center(latLng)
                    .radius(item.getGeofenceRadius())
                    .strokeColor(Color.parseColor("#2F43FF")));

             circleArrayList.add(mMap.addCircle(new CircleOptions()
                     .center(latLng)
                     .radius(item.getGeofenceRadius())
                     .strokeColor(Color.parseColor("#2F43FF"))));

        }
        TruckerDataManager.getInstance().setAllCircles(circleArrayList);
    }

    private int checkSelfPermission(String accessFineLocation) {
        return 0;
    }
    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case REQUEST_LOCATION_PERMISSION: {
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Log.v("LOGIN", "ACCESS_FINE_LOCATION is now granted");
                    setLocationChangeLister();
                }
                return;
            }
        }
    }
}