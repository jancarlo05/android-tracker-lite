package com.procuro.androidtruckerlite;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.Toast;

import com.procuro.apimmdatamanagerlib.AssignmentInfo;
import com.procuro.apimmdatamanagerlib.OnCompleteListeners;
import com.procuro.apimmdatamanagerlib.SdrReport;
import com.procuro.apimmdatamanagerlib.User;
import com.procuro.apimmdatamanagerlib.aPimmDataManager;

import java.security.PublicKey;
import java.util.ArrayList;

public class DeliveryFragment extends Fragment {

    LinearLayout status;
    LinearLayout segmented;

 public static RadioButton pending;
 public static   RadioButton active;
 public static RadioButton completed;

    ImageButton save;
    ImageButton add;
    ImageButton reload;

    public DeliveryFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_delivery, container, false);

        status = view.findViewById(R.id.status);
        segmented = view.findViewById(R.id.segmented_main);
        pending = (RadioButton) view.findViewById(R.id.pending);
        active = (RadioButton) view.findViewById(R.id.active);
        completed = (RadioButton) view.findViewById(R.id.completed);

        save = view.findViewById(R.id.save);
        add = view.findViewById(R.id.add);
        reload = view.findViewById(R.id.reload);

        loadView();

        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

    private void loadView() {
        TruckerDataManager truckerDataManager = TruckerDataManager.getInstance();
        final ArrayList<AssignmentInfo> assignmentInfoList = truckerDataManager.assignmentInfo();

        if (assignmentInfoList.size() > 0) {
            active();
            active.setChecked(true);
            getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container_app_bar,
                    new Segmented_active_fragment()).commit();
        } else {
            active();
            active.setChecked(true);
            getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container_app_bar,
                    new Segmented_Completed_Fragment()).commit();
        }

        pending.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pending();
                if (assignmentInfoList.size() > 1) {
                    getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container_app_bar,
                            new Segmented_Pending_Fragment()).commit();

                } else {
                    getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container_app_bar,
                            new Segmented_Completed_Fragment()).commit();
                }
            }
        });

        active.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                active();
                if (assignmentInfoList.size() > 0) {
                    Toast.makeText(getContext(), "Active", Toast.LENGTH_LONG).show();
                    getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container_app_bar,
                            new Segmented_active_fragment()).commit();
                } else {
                    pending();
                    getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container_app_bar,
                            new Segmented_Completed_Fragment()).commit();
                }
            }
        });

        completed.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getContext(), "Completed", Toast.LENGTH_LONG).show();
                getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container_app_bar,
                        new Segmented_Completed_Fragment()).commit();

                completed();
            }
        });

        reload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (pending.isChecked()) {
                    reload();
                    pending();
                    if (assignmentInfoList.size() > 1) {
                        pending();
                        getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container_app_bar,
                                new Segmented_Pending_Fragment()).commit();

                    } else {
                        getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container_app_bar,
                                new Segmented_Completed_Fragment()).commit();
                    }
                } else if (active.isChecked()) {
                    reload();
                    active();
                    if (assignmentInfoList.size() > 0) {
                        getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container_app_bar,
                                new Segmented_active_fragment()).commit();
                    } else {
                        pending();
                        getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container_app_bar,
                                new Segmented_Completed_Fragment()).commit();
                    }
                }
            }
        });
    }

    public void reload() {
        final aPimmDataManager dataManager = aPimmDataManager.getInstance();
        final TruckerDataManager truckerDataManager = TruckerDataManager.getInstance();
        final User user = truckerDataManager.getUser();
        dataManager.getAssignmentForDCSite(user.defaultSiteID, new OnCompleteListeners.getAssignmentForDCSiteListener() {
            @Override
            public void getAssignmentForDCSite(ArrayList<AssignmentInfo> assignmentInfoList, Error error) {
                truckerDataManager.setAssignmentInfoList(assignmentInfoList);
                String totalRoutes = String.valueOf(assignmentInfoList.size());
                if (assignmentInfoList.size()> 0) {
                    AssignmentInfo assignmentInfo1 = assignmentInfoList.get(0);
                    dataManager.getSdrReportForDeliveryId(assignmentInfo1.shipmentID, false, null, new OnCompleteListeners.getSdrReportForDeliveryIdListener() {
                        @Override
                        public void GetSdrReportForDeliveryId(SdrReport sdrReport, Error error) {
                            truckerDataManager.setSdrReport(sdrReport);
                        }
                    });
                }
            }
        });
    }

    public void pending() {
        pending.setChecked(true);
        add.setVisibility(View.VISIBLE);
        reload.setVisibility(View.VISIBLE);
        save.setVisibility(View.GONE);
    }

    public void active() {
        active.setChecked(true);
        add.setVisibility(View.VISIBLE);
        reload.setVisibility(View.VISIBLE);
        save.setVisibility(View.VISIBLE);
    }

    public void completed() {
        save.setVisibility(View.GONE);
        reload.setVisibility(View.GONE);
        add.setVisibility(View.GONE);
    }


}
