package com.procuro.androidtruckerlite;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;
import android.widget.TextView;

import androidx.fragment.app.Fragment;

import com.procuro.apimmdatamanagerlib.AssignmentInfo;
import com.procuro.apimmdatamanagerlib.SdrGisSite;
import com.procuro.apimmdatamanagerlib.SdrPointOfInterest;
import com.procuro.apimmdatamanagerlib.SdrStop;
import com.procuro.apimmdatamanagerlib.aPimmDataManager;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Locale;
import java.util.TimeZone;

public class Stops_info_Instructions extends Fragment {


    View rootView;
    aPimmDataManager dataManager = aPimmDataManager.getInstance();
    final TruckerDataManager truckerDataManager = TruckerDataManager.getInstance();

    public ArrayList<SdrGisSite> Sites = truckerDataManager.getSdrReport().Hierarchy.GIS.Sites;
    public SdrGisSite site;
    public ArrayList<SdrStop> Stops = truckerDataManager.getSdrReport().Hierarchy.GIS.Stops;
    public  SdrStop stop = null;
    public ArrayList<SdrPointOfInterest> Poi = truckerDataManager.getSdrReport().Hierarchy.GIS.PointsOfInterest;
    public SdrPointOfInterest poi = null;
    public int value;
    ArrayList<AssignmentInfo> assignmentInfos = truckerDataManager.assignmentInfo();


    TextView trailer_name,tractor_name,driver_name,dispatch;
    RadioButton store_info,instructions;
    TextView dc_name, dc_address,manager_contact,dc_phone,dc_email;
    TextView route_name,stops_sched_arrival,stops_actual_arrival,stops_delivery_start,
    stops_actual_departure,stops_ontime_performance,stops_duration;

    public Stops_info_Instructions() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.stops_instructions, container, false);

        Bundle bundle = this.getArguments();
        if (bundle != null) {
            value = bundle.getInt("position");
            System.out.println(value);
        }
        else{
            System.out.println("Wlang laman");
            System.out.println(value);
        }

        return rootView;
    }

    }