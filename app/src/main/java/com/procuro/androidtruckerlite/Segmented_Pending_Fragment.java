package com.procuro.androidtruckerlite;

import android.app.Activity;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.SimpleAdapter;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.baoyz.swipemenulistview.SwipeMenu;
import com.baoyz.swipemenulistview.SwipeMenuCreator;
import com.baoyz.swipemenulistview.SwipeMenuItem;
import com.baoyz.swipemenulistview.SwipeMenuListView;
import com.procuro.apimmdatamanagerlib.AssignmentInfo;
import com.procuro.apimmdatamanagerlib.OnCompleteListeners;
import com.procuro.apimmdatamanagerlib.SdrGisSite;
import com.procuro.apimmdatamanagerlib.SdrPointOfInterest;
import com.procuro.apimmdatamanagerlib.SdrStop;
import com.procuro.apimmdatamanagerlib.User;
import com.procuro.apimmdatamanagerlib.aPimmDataManager;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;

import static androidx.constraintlayout.widget.Constraints.TAG;

public class Segmented_Pending_Fragment extends Fragment {
    aPimmDataManager dataManager = aPimmDataManager.getInstance();
    final TruckerDataManager truckerDataManager = TruckerDataManager.getInstance();

    public ArrayList<SdrGisSite> Sites = truckerDataManager.getSdrReport().Hierarchy.GIS.Sites;
    public SdrGisSite site;
    public ArrayList<SdrStop> Stops = truckerDataManager.getSdrReport().Hierarchy.GIS.Stops;
    public  SdrStop stop = null;
    public ArrayList<SdrPointOfInterest> Poi = truckerDataManager.getSdrReport().Hierarchy.GIS.PointsOfInterest;
    public SdrPointOfInterest poi = null;

    View rootView;

    public Segmented_Pending_Fragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.segmented_pending_fragment, container, false);
        active();
        return rootView;
    }

    public void active() {
        TruckerDataManager truckerDataManager = TruckerDataManager.getInstance();

        final ArrayList<AssignmentInfo> assignmentInfoList = truckerDataManager.assignmentInfo();
        SimpleDateFormat inputFormat = new SimpleDateFormat(
                "EEE MMM dd HH:mm:ss 'GMT' yyyy", Locale.US);
        inputFormat.setTimeZone(TimeZone.getTimeZone("Etc/UTC"));
        SimpleDateFormat outputFormat = new SimpleDateFormat("MMM dd, yyyy h:mm a");


        List<HashMap<String, String>> aList = new ArrayList<HashMap<String, String>>();
            AssignmentInfo assignmentInfo;
            for (int i = 1; i < assignmentInfoList.size(); i++) {
                assignmentInfo = assignmentInfoList.get(i);
                String route_id = assignmentInfo.routeName;
                String tractor_id = assignmentInfo.tractorName;
                String date = outputFormat.format(assignmentInfo.dispatch);

                HashMap<String, String> hm = new HashMap<String, String>();
                hm.put("listview_title", "Route :" + route_id);
                hm.put("listview_discription", tractor_id);
                hm.put("listview_image", Integer.toString(R.drawable.route_icon_dispatch_gray));
                hm.put("time", (date));
                aList.add(hm);
            }
            String[] from = {"listview_image", "listview_title", "listview_discription", "time"};
            int[] to = {R.id.listview_image, R.id.listview_item_title, R.id.listview_item_short_description, R.id.time};
            SimpleAdapter simpleAdapter = new SimpleAdapter(rootView.getContext().getApplicationContext(), aList, R.layout.segmented_pending_data, from, to);
            SwipeMenuListView androidListView = (SwipeMenuListView) rootView.findViewById(R.id.listView);
            androidListView.setAdapter(simpleAdapter);


            androidListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {


                    Bundle bundle = new Bundle();
                    System.out.println(position+1);
                    bundle.putInt("position",position+1);
                    Fragment fragment2 =  new Segmented_Pending_route_info();
                    fragment2.setArguments(bundle);

                    getFragmentManager()
                            .beginTransaction()
                            .replace(R.id.fragment_container_app_bar, fragment2)
                            .commit();

                    MainActivity.hide();
                    final AssignmentInfo assignmentInfo = assignmentInfoList.get(position+1);
                    MainActivity.fullname.setText("Route: "+assignmentInfo.routeName);

                }
            });
            //swipe

            SwipeMenuCreator creator = new SwipeMenuCreator() {
                @Override
                public void create(SwipeMenu menu) {
                    // create "delete" item
                    SwipeMenuItem end_route = new SwipeMenuItem(
                            getActivity().getApplicationContext());
                    // set item background
                    end_route.setBackground(new ColorDrawable(Color.parseColor("#009444")));
                    // set item width
                    end_route.setWidth(200);
                    // set a icon
                    end_route.setTitle("Start");
                    end_route.setTitleSize(18);
                    end_route.setTitleColor(Color.WHITE);
                    // add to menu
                    menu.addMenuItem(end_route);
                }
            };
            androidListView.setMenuCreator(creator);
            androidListView.setOnMenuItemClickListener(new SwipeMenuListView.OnMenuItemClickListener() {
                @Override
                public boolean onMenuItemClick(int position, SwipeMenu menu, int index) {
                    switch (index) {
                        case 0:
                            Log.d(TAG, "onMenuItemClick: clicked item " + index);
                            break;
                        case 1:
                            Log.d(TAG, "onMenuItemClick: clicked item " + index);
                            break;
                    }
                    // false : close the menu; true : not close the menu
                    return false;
                }
            });

        }
    }
