package com.procuro.androidtruckerlite;

import android.view.contentcapture.DataRemovalRequest;

import com.google.android.gms.location.Geofence;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

public class Routes implements Serializable {
    private static final long serialVersionUID = 1L;
    private String route_name;
    private int specific_position;
    private String address;
    private String site_desc;
    private Date arrival_time;
    private Date departure_time;
    private Date delivery_confirmation_time;
    private double lat;
    private double longt;
    private int Delivery_number;
    private float geofenceRadius;
    private int poi_type;
    private String site_id;


    public int getPoi_type() {
        return poi_type;
    }

    public void setPoi_type(int poi_type) {
        this.poi_type = poi_type;
    }

    public float getGeofenceRadius() {
        return geofenceRadius;
    }

    public void setGeofenceRadius(float geofenceRadius) {
        this.geofenceRadius = geofenceRadius;
    }

    public Date getDelivery_confirmation_time() {
        return delivery_confirmation_time;
    }

    public void setDelivery_confirmation_time(Date delivery_confirmation_time) {
        this.delivery_confirmation_time = delivery_confirmation_time;
    }

    public double getLat() {
        return lat;
    }

    public void setLat(Long lat) {
        this.lat = lat;
    }

    public double getLongt() {
        return longt;
    }

    public void setLongt(long longt) {
        this.longt = longt;
    }

    public int getDelivery_number() {
        return Delivery_number;
    }

    public void setDelivery_number(int delivery_number) {
        Delivery_number = delivery_number;
    }


    public Date getArrival_time() {
        return arrival_time;
    }

    public void setArrival_time(Date arrival_time) {
        this.arrival_time = arrival_time;
    }

    public Date getDeparture_time() {
        return departure_time;
    }

    public void setDeparture_time(Date departure_time) {
        this.departure_time = departure_time;
    }


    public Routes(String route_name, int specific_position , String address , String site_desc, Date arrival_time, Date departure_time
    , Date delivery_confirmation_time, double lat, double longt, int delivery_number, float geofenceRadius, int poi_type,String site_id) {
        this.route_name = route_name;
        this.specific_position = specific_position;
        this.address = address;
        this.site_desc = site_desc;
        this.arrival_time = arrival_time;
        this.departure_time = departure_time;
        this.delivery_confirmation_time = delivery_confirmation_time;
        this.lat = lat;
        this.longt = longt;
        this.Delivery_number = delivery_number;
        this.geofenceRadius = geofenceRadius;
        this.poi_type = poi_type;
        this.site_id = site_id;
    }

    public String getSite_id() {
        return site_id;
    }

    public void setSite_id(String site_id) {
        this.site_id = site_id;
    }

    public String getRoute_name() {
        return route_name;
    }
    public void setRoute_name(String route_name) {
        this.route_name = route_name;
    }
    public int getSpecific_position() {
        return specific_position;
    }
    public void setSpecific_position(int specific_position) {
        this.specific_position = specific_position;
    }
    
    public String getAddress() {
        return address;
    }
    public void setAddress(String address) {
        this.address = address;
    }
    public String getSite_desc() {
        return site_desc;
    }
    public void setSite_desc(String address) {
        this.site_desc = site_desc;
    }



}
