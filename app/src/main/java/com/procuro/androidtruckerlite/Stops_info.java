package com.procuro.androidtruckerlite;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;
import android.widget.SimpleAdapter;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;

import com.baoyz.swipemenulistview.SwipeMenuListView;
import com.procuro.apimmdatamanagerlib.AssignmentInfo;
import com.procuro.apimmdatamanagerlib.SdrGisSite;
import com.procuro.apimmdatamanagerlib.SdrPointOfInterest;
import com.procuro.apimmdatamanagerlib.SdrStop;
import com.procuro.apimmdatamanagerlib.User;
import com.procuro.apimmdatamanagerlib.aPimmDataManager;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class Stops_info extends Fragment {


    View rootView;
    aPimmDataManager dataManager = aPimmDataManager.getInstance();
    final TruckerDataManager truckerDataManager = TruckerDataManager.getInstance();

    public ArrayList<SdrGisSite> Sites = truckerDataManager.getSdrReport().Hierarchy.GIS.Sites;
    public SdrGisSite site;
    public ArrayList<SdrStop> Stops = truckerDataManager.getSdrReport().Hierarchy.GIS.Stops;
    public  SdrStop stop = null;
    public ArrayList<SdrPointOfInterest> Poi = truckerDataManager.getSdrReport().Hierarchy.GIS.PointsOfInterest;
    public SdrPointOfInterest poi = null;
    public int value;
    public Bundle bundle = null;

    TextView trailer_name,tractor_name,driver_name,dispatch;
    RadioButton store_info,instructions;

    public Stops_info() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.stops_info, container, false);
        store_info = rootView.findViewById(R.id.store_info);
        instructions = rootView.findViewById(R.id.instructions);


        bundle = this.getArguments();
        if (bundle != null) {
            value = bundle.getInt("position");
            System.out.println(value);

        }
        else{
            System.out.println("Wlang laman");
            System.out.println(value);
        }

        if (store_info.isChecked()) {

            bundle = new Bundle();
            bundle.putInt("position",value);
            Fragment fragment2 =  new Stops_info_store_info();
            fragment2.setArguments(bundle);
            ((AppCompatActivity) rootView.getContext()).getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.fragment_container_stops, fragment2)
                    .commit();
        }else{
            bundle = new Bundle();
            bundle.putInt("position",value);
            Fragment fragment2 =  new Stops_info_Instructions();
            fragment2.setArguments(bundle);
            ((AppCompatActivity) rootView.getContext()).getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.fragment_container_stops, fragment2)
                    .commit();
        }

        store_info.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                bundle = new Bundle();
                bundle.putInt("position",value);
                Fragment fragment2 =  new Stops_info_store_info();
                fragment2.setArguments(bundle);
                ((AppCompatActivity) rootView.getContext()).getSupportFragmentManager()
                        .beginTransaction()
                        .replace(R.id.fragment_container_stops, fragment2)
                        .commit();
            }
        });

        instructions.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                bundle = new Bundle();
                bundle.putInt("position",value);
                Fragment fragment2 =  new Stops_info_Instructions();
                fragment2.setArguments(bundle);
                ((AppCompatActivity) rootView.getContext()).getSupportFragmentManager()
                        .beginTransaction()
                        .replace(R.id.fragment_container_stops, fragment2)
                        .commit();

            }
        });
        return rootView;
    }
    }

