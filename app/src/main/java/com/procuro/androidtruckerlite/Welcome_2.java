package com.procuro.androidtruckerlite;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.CustomTarget;
import com.bumptech.glide.request.transition.Transition;
import com.google.android.gms.location.Geofence;
import com.google.android.material.navigation.NavigationView;
import com.procuro.androidlocationservice.LocationService;
import com.procuro.androidtruckerlite.DBHelper.DBHelper;
import com.procuro.apimmdatamanagerlib.AssignmentInfo;
import com.procuro.apimmdatamanagerlib.OnCompleteListeners;
import com.procuro.apimmdatamanagerlib.SdrDriver;
import com.procuro.apimmdatamanagerlib.SdrReport;
import com.procuro.apimmdatamanagerlib.User;
import com.procuro.apimmdatamanagerlib.aPimmDataManager;


import de.hdodenhof.circleimageview.CircleImageView;


import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class Welcome_2 extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {
    aPimmDataManager dataManager;
    TextView route , tractor,trailer,helper;
    CircleImageView imageView;
    DBHelper dbHelper;

    private int show_popup = 0;
    private int arrival = 0 ;
    private int departure = 0 ;
    private double lat = 0;
    private double longt = 0;
    private String location_names ="";
    private String site_name ="";
    private String site_type ="";
    CountDownTimer countDownTimer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_welcome_2);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Route");
        ImageButton next = findViewById(R.id.next);
        route = findViewById(R.id.route);
        tractor = findViewById(R.id.tractor);
        trailer = findViewById(R.id.trailer);
        helper = findViewById(R.id.driver);
        imageView = findViewById(R.id.pp);
        dbHelper = new DBHelper(this);

        show_popup = 0;
        location_service();
        show_pop_timer();


        Glide.with(this)
                .asBitmap()
                .load(dbHelper.gimage())
                .into(new CustomTarget<Bitmap>() {
                    @Override
                    public void onResourceReady(@NonNull Bitmap resource, @Nullable Transition<? super Bitmap> transition) {
                        imageView.setImageBitmap(resource);
                    }
                    @Override
                    public void onLoadCleared(@Nullable Drawable placeholder) {
                    }
                });



        dataManager = aPimmDataManager.getInstance();
        final TruckerDataManager truckerDataManager = TruckerDataManager.getInstance();
        User user = truckerDataManager.getUser();
        ArrayList<AssignmentInfo> assignmentInfoList = truckerDataManager.assignmentInfo();


        if (assignmentInfoList.size()>0) {
            final AssignmentInfo assignmentInfo = assignmentInfoList.get(0);
            SdrReport sdrReport = truckerDataManager.getSdrReport();
            route.setText(assignmentInfo.routeName);
            tractor.setText(assignmentInfo.tractorName);
            trailer.setText(assignmentInfo.trailerName);
            if(sdrReport.Hierarchy.CoDriver != null){
                helper.setText(sdrReport.Hierarchy.CoDriver.FullName);
            }

        }
        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Welcome_2.this, MainActivity.class);
                startActivity(intent);
                finish();
                countDownTimer.cancel();
            }
        });

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        NavigationView navigationView = findViewById(R.id.nav_view);

        View headerview = navigationView.getHeaderView(0);
        CircleImageView imageView1 = headerview.findViewById(R.id.drawer_profile_Image);
        TextView profile_name1  = headerview.findViewById(R.id.header_profile_name);
        imageView1.setImageBitmap(dbHelper.gimage());
        profile_name1.setText(user.getFullName());


        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);

        drawer.addDrawerListener(toggle);
        toggle.syncState();
        navigationView.setNavigationItemSelectedListener(this);
    }


    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
            Intent intent = new Intent(Welcome_2.this, WelcomeScreen.class);
            startActivity(intent);
            finish();
        }
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();
        if (id == R.id.nav_home) {
            Intent intent = new Intent(Welcome_2.this, WelcomeScreen.class);
            startActivity(intent);
            countDownTimer.cancel();
            finish();
            // Handle the camera action
        } else if (id == R.id.nav_gallery) {
            Intent intent = new Intent(Welcome_2.this, Profile_2.class);
            startActivity(intent);
            finish();
            countDownTimer.cancel();
        } else if (id == R.id.nav_slideshow) {
            Intent intent = new Intent(Welcome_2.this, Help_2.class);
            startActivity(intent);
            finish();
            countDownTimer.cancel();
        } else if (id == R.id.nav_tools) {
            Intent intent = new Intent(Welcome_2.this, Login.class);
            startActivity(intent);
            finish();
            countDownTimer.cancel();
        }

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }





    public void unsched_stop_alert(){
        show_popup = 0;
        LayoutInflater li = LayoutInflater.from(Welcome_2.this);
        View view = li.inflate(R.layout.unsched_stop_alert, null);
        final android.app.AlertDialog.Builder alertDialogBuilder = new android.app.AlertDialog.Builder(Welcome_2.this);
        TextView address = view.findViewById(R.id.location_name);
        address.setText(location_names);
        alertDialogBuilder.setView(view);

        final android.app.AlertDialog alertDialogCongratulations = alertDialogBuilder.create();
        alertDialogCongratulations.getWindow().setLayout(LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);//
        alertDialogCongratulations.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        alertDialogCongratulations.setCancelable(false);
        alertDialogCongratulations.show();

        new CountDownTimer(5000, 1000) {
            public void onTick(long millisUntilFinished) {
            }
            public void onFinish() {
                alertDialogCongratulations.dismiss();
                show_popup = 0 ;
            }
        }.start();

    }

    public void show_pop_timer(){
        countDownTimer = new  CountDownTimer(1000, 1000) {
            public void onTick(long millisUntilFinished) {
            }
            public void onFinish() {

                if (show_popup ==1){
                    unsched_stop_alert();
                }else if(arrival ==1){
                    arrival_alert();
                }else if(departure ==1){
                    departure_alert();
                }
                else {

                }
                start();
            }
        }.start();
    }

    public void arrival_alert(){
        arrival = 0;
        LayoutInflater li = LayoutInflater.from(Welcome_2.this);
        View view = li.inflate(R.layout.poi_arrival_alert, null);
        final android.app.AlertDialog.Builder alertDialogBuilder = new android.app.AlertDialog.Builder(Welcome_2.this);
        TextView address = view.findViewById(R.id.alert_sitename);
        TextView type = view.findViewById(R.id.departure_type);
        type.setText(" ARRIVAL ");
        address.setText(site_name);
        alertDialogBuilder.setView(view);
        final android.app.AlertDialog alertDialogCongratulations = alertDialogBuilder.create();
        alertDialogCongratulations.getWindow().setLayout(LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);//
        alertDialogCongratulations.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        alertDialogCongratulations.setCancelable(false);
        alertDialogCongratulations.show();

        new CountDownTimer(2000, 1000) {
            public void onTick(long millisUntilFinished) {
            }
            public void onFinish() {
                alertDialogCongratulations.dismiss();
                arrival = 0;
            }
        }.start();
    }

    public void departure_alert(){
        departure = 0;
        LayoutInflater li = LayoutInflater.from(Welcome_2.this);
        View view = li.inflate(R.layout.poi_departure_alert, null);
        final android.app.AlertDialog.Builder alertDialogBuilder = new android.app.AlertDialog.Builder(Welcome_2.this);
        TextView address = view.findViewById(R.id.alert_sitename);
        TextView type = view.findViewById(R.id.departure_type);
        type.setText("DEPARTURE");
        address.setText(site_name);
        alertDialogBuilder.setView(view);

        final android.app.AlertDialog alertDialogCongratulations = alertDialogBuilder.create();
        alertDialogCongratulations.getWindow().setLayout(LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);//
        alertDialogCongratulations.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        alertDialogCongratulations.setCancelable(false);
        alertDialogCongratulations.show();

        new CountDownTimer(2000, 1000) {
            public void onTick(long millisUntilFinished) {
            }
            public void onFinish() {
                alertDialogCongratulations.dismiss();
                show_popup = 0 ;
                departure = 0;
            }
        }.start();

    }

    public void location_service(){

        LocationService.getInstance().setGeofenceTransitionListener(new LocationService.OnGeofenceTransitionListener() {
            @Override
            public void didEnterGeofence(Geofence geofence) {
                String siteName = TruckerDataManager.getInstance().getSiteNameForSiteID(geofence.getRequestId());
                Log.v("Location Service", "triggeringGeofences GEOFENCE_TRANSITION_ENTER: " + siteName);
                displayMessage("GEOFENCE_TRANSITION_ENTER: " + siteName);
                site_name = siteName;
                arrival++;

                // TODO: Display Geofence Enter pop-up
            }
            @Override
            public void didExitGeofence(Geofence geofence) {
                String siteName = TruckerDataManager.getInstance().getSiteNameForSiteID(geofence.getRequestId());
                String type = TruckerDataManager.getInstance().getSiteNameForSiteID(geofence.getRequestId());
                Log.v("Location Service", "triggeringGeofences GEOFENCE_TRANSITION_EXIT: " + siteName);
                displayMessage("GEOFENCE_TRANSITION_EXIT: " + siteName);
                site_name = siteName;
                departure++;
                // TODO: Display Geofence Exit pop-up
            }
        });
        LocationService.getInstance().setOnUnscheduledStopDetectedListener(new LocationService.OnUnscheduledStopDetectedListener() {
            @Override
            public void onUnscheduledStopDetectedListener(Location location) {
                Log.v("Location Service", "unscheduledStopDetected : " + location);
//                displayMessage("UNSCHEDULED_STOP_DETECTION: " + location);
                if (location != null){
                    lat = location.getLatitude();
                    longt = location.getLongitude();
                    List<Address> addresses;
                    Geocoder geocoder = new Geocoder(Welcome_2.this, Locale.getDefault());
                    try {
                        addresses = geocoder.getFromLocation(lat, longt, 1);
                        location_names = addresses.get(0).getAddressLine(0);
                        // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }

                // TODO: Display Unscheduled Stop Detection pop-up
                show_popup++;

            }
        });
    }

    private void displayMessage(final String message) {
        Handler handler = new Handler(Looper.getMainLooper());
        handler.post(new Runnable() {
            @Override
            public void run() {
                Toast.makeText(getApplicationContext(),
                        message,
                        Toast.LENGTH_LONG).show();
            }
        });
    }

    public void alert(){

        AlertDialog alertDialog = new AlertDialog.Builder(this)
                //set icon
                .setIcon(R.drawable.logo)
                //set title
                .setTitle("Warning!")
                //set message
                .setMessage("Do you want to exit?")
                //set positive button
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                        Intent intent = new Intent(Welcome_2.this, Login.class);
                        startActivity(intent);
                        finish();
                        countDownTimer.cancel();
                    }
                })
                //set negative button
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                    }
                })
                .show();
    }




}
