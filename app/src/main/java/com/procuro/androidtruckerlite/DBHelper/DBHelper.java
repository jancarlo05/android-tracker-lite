package com.procuro.androidtruckerlite.DBHelper;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteQueryBuilder;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.strictmode.SqliteObjectLeakedViolation;

import com.readystatesoftware.sqliteasset.SQLiteAssetHelper;

public class DBHelper extends SQLiteAssetHelper {

 private static final String DB_Name="Truckerlite.db";
    private static final int DB_VER = 1;
    private static final String TBL_SPID="SPID";
    private static final String COL_SPID_ID ="spid_id";
    private static final String COL_SPID_DESC ="spid";

    private static final String TBL_IMAGE="Profile_Picture";
    private static final String COL_IMAGE_ID ="image_id";
    private static final String COL_IMAGE ="image";

    public DBHelper(Context context) {
        super(context, DB_Name, null, DB_VER);
    }


    public void addImage(byte[]image)throws SQLiteException{
        SQLiteDatabase database = this.getWritableDatabase();
        ContentValues cv = new ContentValues();
        cv.put(COL_IMAGE,image);
        cv.put(COL_IMAGE_ID,1);
        database.insert(TBL_IMAGE,null,cv);
    }
    public void signature1(byte[]image)throws SQLiteException{
        SQLiteDatabase database = this.getWritableDatabase();
        ContentValues cv = new ContentValues();
        cv.put(COL_IMAGE,image);
        cv.put(COL_IMAGE_ID,2);
        database.insert(TBL_IMAGE,null,cv);
    }

    public void signature2(byte[]image)throws SQLiteException{
        SQLiteDatabase database = this.getWritableDatabase();
        ContentValues cv = new ContentValues();
        cv.put(COL_IMAGE,image);
        cv.put(COL_IMAGE_ID,3);
        database.insert(TBL_IMAGE,null,cv);
    }
    public void signature3(byte[]image)throws SQLiteException{
        SQLiteDatabase database = this.getWritableDatabase();
        ContentValues cv = new ContentValues();
        cv.put(COL_IMAGE,image);
        cv.put(COL_IMAGE_ID,4);
        database.insert(TBL_IMAGE,null,cv);
    }
    public void signature4(byte[]image)throws SQLiteException{
        SQLiteDatabase database = this.getWritableDatabase();
        ContentValues cv = new ContentValues();
        cv.put(COL_IMAGE,image);
        cv.put(COL_IMAGE_ID,5);
        database.insert(TBL_IMAGE,null,cv);
    }

    public byte[] getBitmapImage(String image){
        SQLiteDatabase database = this.getWritableDatabase();
        SQLiteQueryBuilder qb = new SQLiteQueryBuilder();
        String[] select = {COL_IMAGE};
        qb.setTables(TBL_IMAGE);

        Cursor c = (Cursor) qb.query(database, select, "image_id = ?", new String[]{image}, null, null, null);
        byte[] result = null;
        if (c.moveToFirst()){

            do {
                result = c.getBlob(c.getColumnIndex(COL_IMAGE));
            }while (c.moveToNext());
        }
        return result;
    }


    public Cursor image(){
        SQLiteDatabase database = this.getWritableDatabase();
        Cursor cursor = database.rawQuery("select image from Profile_picture where image_id = 1",null);
        return cursor;
    }


    public Bitmap gimage(){
        SQLiteDatabase database = this.getWritableDatabase();
        Bitmap bt = null;
        Cursor cursor = database.rawQuery("select image from Profile_picture where image_id = 1",null);
        if (cursor.moveToNext()){
            byte[] image =cursor.getBlob(0);
            bt = BitmapFactory.decodeByteArray(image,0,image.length);
        }
        return bt;
    }

    public Bitmap getsignature1(){
        SQLiteDatabase database = this.getWritableDatabase();
        Bitmap bt = null;
        Cursor cursor = database.rawQuery("select image from Profile_picture where image_id = 2",null);
        if (cursor.moveToNext()){
            byte[] image =cursor.getBlob(0);
            bt = BitmapFactory.decodeByteArray(image,0,image.length);
        }
        return bt;
    }

    public Bitmap getsignature2(){
        SQLiteDatabase database = this.getWritableDatabase();
        Bitmap bt = null;
        Cursor cursor = database.rawQuery("select image from Profile_picture where image_id = 3",null);
        if (cursor.moveToNext()){
            byte[] image =cursor.getBlob(0);
            bt = BitmapFactory.decodeByteArray(image,0,image.length);
        }
        return bt;
    }

    public Bitmap getsignature3(){
        SQLiteDatabase database = this.getWritableDatabase();
        Bitmap bt = null;
        Cursor cursor = database.rawQuery("select image from Profile_picture where image_id = 4",null);
        if (cursor.moveToNext()){
            byte[] image =cursor.getBlob(0);
            bt = BitmapFactory.decodeByteArray(image,0,image.length);
        }
        return bt;
    }

    public Bitmap getsignature4(){
        SQLiteDatabase database = this.getWritableDatabase();
        Bitmap bt = null;
        Cursor cursor = database.rawQuery("select image from Profile_picture where image_id = 5",null);
        if (cursor.moveToNext()){
            byte[] image =cursor.getBlob(0);
            bt = BitmapFactory.decodeByteArray(image,0,image.length);
        }
        return bt;
    }

    public Bitmap deletesignature1(){
        SQLiteDatabase database = this.getWritableDatabase();
        Bitmap bt = null;
        Cursor cursor = database.rawQuery("delete from Profile_picture where image_id = 2",null);
        if (cursor.moveToNext()){
            byte[] image =cursor.getBlob(0);
            bt = BitmapFactory.decodeByteArray(image,0,image.length);
        }
        return bt;
    }

    public Bitmap deletesignature2(){
        SQLiteDatabase database = this.getWritableDatabase();
        Bitmap bt = null;
        Cursor cursor = database.rawQuery("delete from Profile_picture where image_id = 3",null);
        if (cursor.moveToNext()){
            byte[] image =cursor.getBlob(0);
            bt = BitmapFactory.decodeByteArray(image,0,image.length);
        }
        return bt;
    }

    public Bitmap deletesignature3(){
        SQLiteDatabase database = this.getWritableDatabase();
        Bitmap bt = null;
        Cursor cursor = database.rawQuery("delete from Profile_picture where image_id = 4",null);
        if (cursor.moveToNext()){
            byte[] image =cursor.getBlob(0);
            bt = BitmapFactory.decodeByteArray(image,0,image.length);
        }
        return bt;
    }
    public Bitmap deletesignature4(){
        SQLiteDatabase database = this.getWritableDatabase();
        Bitmap bt = null;
        Cursor cursor = database.rawQuery("delete from Profile_picture where image_id = 5",null);
        if (cursor.moveToNext()){
            byte[] image =cursor.getBlob(0);
            bt = BitmapFactory.decodeByteArray(image,0,image.length);
        }
        return bt;
    }

    public Bitmap delete(){
        SQLiteDatabase database = this.getWritableDatabase();
        Bitmap bt = null;
        Cursor cursor = database.rawQuery("delete from Profile_picture where image_id = 1",null);
        if (cursor.moveToNext()){
            byte[] image =cursor.getBlob(0);
            bt = BitmapFactory.decodeByteArray(image,0,image.length);
        }
        return bt;

    }

}


