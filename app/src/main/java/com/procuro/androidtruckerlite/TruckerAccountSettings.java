package com.procuro.androidtruckerlite;

import com.procuro.apimmdatamanagerlib.PropertyValue;
import com.procuro.apimmdatamanagerlib.StoreProviderSiteSettings;

public class TruckerAccountSettings {
    private static TruckerAccountSettings instance;

    private StoreProviderSiteSettings storeProviderSiteSettings;

    public boolean GPSPostGeofenceEnter;
    public boolean GPSConfirmArrival;
    public int GPSGeofenceEnterDelay;
    public int UnscheduledStopTolerance;

    public static TruckerAccountSettings getInstance() {
        if(instance == null) {
            instance = new TruckerAccountSettings();

            instance.GPSPostGeofenceEnter = true;
            instance.GPSConfirmArrival = true;
            instance.GPSGeofenceEnterDelay = 5 * 60;
            instance.UnscheduledStopTolerance = 5;
        }
        return instance;
    }

    private TruckerAccountSettings() {

    }

    public void initWithStoreProvidedSettings(StoreProviderSiteSettings storeProviderSiteSettings) {
        this.storeProviderSiteSettings = storeProviderSiteSettings;
        for(PropertyValue propertyValue : this.storeProviderSiteSettings.settings) {
            if(propertyValue.property.equalsIgnoreCase("Std:TMS:GPS:PostGeofenceEnter")) {
                GPSPostGeofenceEnter = Boolean.valueOf(propertyValue.value);
            } else if(propertyValue.property.equalsIgnoreCase("Std:TMS:GPS:ConfirmArrival")) {
                GPSConfirmArrival = Boolean.valueOf(propertyValue.value);
            } else if(propertyValue.property.equalsIgnoreCase("Std:TMS:GPS:GeofenceEnterDelay")) {
                GPSGeofenceEnterDelay = Integer.valueOf(propertyValue.value);
            } else if(propertyValue.property.equalsIgnoreCase("StD:UnscheduledStopTolerance")) {
                UnscheduledStopTolerance = Integer.valueOf(propertyValue.value);
            }
        }
    }
}
