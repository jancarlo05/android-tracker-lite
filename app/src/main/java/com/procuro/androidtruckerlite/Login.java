package com.procuro.androidtruckerlite;

import android.Manifest;
import android.app.ActivityManager;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import com.google.android.material.snackbar.Snackbar;
import com.procuro.androidlocationservice.LocationService;
import com.procuro.apimmdatamanagerlib.OnCompleteListeners;
import com.procuro.apimmdatamanagerlib.StoreProviderSiteSettings;
import com.procuro.apimmdatamanagerlib.User;
import com.procuro.apimmdatamanagerlib.aPimmDataManager;

import dmax.dialog.SpotsDialog;

public class Login extends AppCompatActivity {
    private EditText txtUsername;
    private EditText txtPassword;
    private Button btnLogin;
    private SpotsDialog spotsDialog;
    private aPimmDataManager dataManager;
    private String SPID;
    private String username;
    private String password;

    int count = 0;
    private static final int REQUEST_LOCATION_PERMISSION = 1;
    boolean hide = false;

    private View.OnTouchListener onTouchListener;
    private View.OnClickListener onClickListener;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login);

        btnLogin = findViewById(R.id.login);
        txtUsername = findViewById(R.id.username);
        txtPassword = findViewById(R.id.password);

        spotsDialog = (SpotsDialog) new SpotsDialog.Builder().setContext(this).setMessage("Signing in").build();
        dataManager = aPimmDataManager.getInstance();

        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);

        this.setEventListeners();

        txtUsername.setOnTouchListener(onTouchListener);
        txtPassword.setOnTouchListener(onTouchListener);

        txtUsername.setOnClickListener(onClickListener);
        txtPassword.setOnClickListener(onClickListener);

        txtPassword.setOnKeyListener(new View.OnKeyListener() {
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if ((event.getAction() == KeyEvent.ACTION_DOWN) && (keyCode == KeyEvent.KEYCODE_ENTER)) {
                     login(v);
                    return true;
                }
                return false;
            }
        });

        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View v) {
                login(v);
            }
        });

        checkPermissions();
        TruckerDataManager.getInstance().setActivity(this);
    }

    private void setEventListeners() {
        onTouchListener = new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
                return false;
            }
        };

        onClickListener = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
            }
        };
    }

    private void checkPermissions() {

        if (ActivityCompat.checkSelfPermission(this.getApplicationContext(),
                Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]
                            {Manifest.permission.ACCESS_FINE_LOCATION},
                    REQUEST_LOCATION_PERMISSION);
        } else {
            Log.v("LOGIN", "ACCESS_FINE_LOCATION is already granted");
            LocationService.getInstance().setActivity(Login.this);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case REQUEST_LOCATION_PERMISSION: {
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Log.v("LOGIN", "ACCESS_FINE_LOCATION is now granted");
                    LocationService.getInstance().setActivity(Login.this);
                    LocationService.getInstance().startLocationService();

                } else if (grantResults[0] == PackageManager.PERMISSION_DENIED) {
                    String permission = permissions[0];
                    // user rejected the permission
                    Toast.makeText(getApplicationContext(),"PLEASE ACCEPT THE REQUIRED PERMISSION",Toast.LENGTH_SHORT).show();
                    new CountDownTimer(2000, 1000) {
                        public void onTick(long millisUntilFinished) {
                        }
                        public void onFinish() {
                            checkPermissions();
                        }
                    }.start();

                    boolean showRationale = shouldShowRequestPermissionRationale(permission);
                    if (!showRationale) {

                        Toast.makeText(getApplicationContext(),"APP WILL BE EXITING, PLEASE ACCEPT THE  REQUIRED PERMISSION!",Toast.LENGTH_SHORT).show();

                        new CountDownTimer(2000, 1000) {
                            public void onTick(long millisUntilFinished) {
                            }
                            public void onFinish() {
                                clearAppData();
                            }
                        }.start();

                    }
                }
            }
        }
    }

    public void login(final View v){
        SPID  = "procuro";
        username = txtUsername.getText().toString();
        password = txtPassword.getText().toString();

        if (username.equals("")) {
            Snackbar snackbar1 = (Snackbar) Snackbar.make(v, "Insert Username", Snackbar.LENGTH_SHORT);
            View view = snackbar1.getView();
            FrameLayout.LayoutParams params = (FrameLayout.LayoutParams)view.getLayoutParams();
            params.gravity = Gravity.BOTTOM | Gravity.CENTER_HORIZONTAL;
            params.width = FrameLayout.LayoutParams.MATCH_PARENT;
            view.setLayoutParams(params);
            snackbar1.show();
            count++;

        } else if (password.equals("")) {
            Snackbar snackbar1 = (Snackbar) Snackbar.make(v, "Insert Password", Snackbar.LENGTH_SHORT);

            View view = snackbar1.getView();
            // Position snackbar at top
            FrameLayout.LayoutParams params = (FrameLayout.LayoutParams)view.getLayoutParams();
            params.gravity = Gravity.BOTTOM | Gravity.CENTER_HORIZONTAL;
            params.width = FrameLayout.LayoutParams.MATCH_PARENT;
            view.setLayoutParams(params);
            snackbar1.show();
            count++;

        } else {
            hide();
            if (hide == true) {
                spotsDialog.show();
            }
        }

        dataManager.initializeWithSPID(SPID);
        dataManager.loginWithUsernameAndPassword(username, password, new OnCompleteListeners.OnLoginCompleteListener() {
            @Override
            public void onLoginComplete(User user, Error error) {
                spotsDialog.dismiss();

                if (error == null) {
                    final TruckerDataManager truckerDataManager = TruckerDataManager.getInstance();
                    truckerDataManager.setSPID(SPID);
                    truckerDataManager.setUsername(username);
                    truckerDataManager.setPassword(password);
                    truckerDataManager.setUser(user);

                    dataManager.getStoreProviderSiteSettingsForSiteId(user.defaultSiteID, new OnCompleteListeners.getStoreProviderSiteSettingsForSiteIdListener() {
                        @Override
                        public void getStoreProviderSiteSettingsForSiteId(StoreProviderSiteSettings storeProviderSiteSettings, Error error) {
                            TruckerAccountSettings truckerAccountSettings = TruckerAccountSettings.getInstance();
                            truckerAccountSettings.initWithStoreProvidedSettings(storeProviderSiteSettings);
                            LocationService locationService = LocationService.getInstance();
                            locationService.setUnscheduledStopTolerance(truckerAccountSettings.UnscheduledStopTolerance);
                            locationService.setGeofenceEnterDelay(truckerAccountSettings.GPSGeofenceEnterDelay);
                            locationService.setPostGeofenceEnter(truckerAccountSettings.GPSPostGeofenceEnter);
                            locationService.setStoreConfirmArrival(truckerAccountSettings.GPSConfirmArrival);
                            locationService.startLocationService();
                        }
                    });

                    Intent intent = new Intent(Login.this, WelcomeScreen.class);
                    startActivity(intent);
                    finish();
                } else {
                    if (count == 0) {
                        Snackbar snackbar1 = (Snackbar) Snackbar.make(v, "Incorrect Username or Password", Snackbar.LENGTH_SHORT);
                        View view = snackbar1.getView();
                        // Position snackbar at top
                        FrameLayout.LayoutParams params = (FrameLayout.LayoutParams)view.getLayoutParams();
                        params.gravity = Gravity.BOTTOM | Gravity.CENTER_HORIZONTAL;
                        params.width = FrameLayout.LayoutParams.MATCH_PARENT;
                        view.setLayoutParams(params);
                        snackbar1.show();

                    }else {
                        count = 0 ;
                    }
                }
            }

        });
    }

    public void hide(){
        hide = true;
        View view = this.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    private void clearAppData() {
        try {
            // clearing app data
            if (Build.VERSION_CODES.KITKAT <= Build.VERSION.SDK_INT) {
                ((ActivityManager)getSystemService(ACTIVITY_SERVICE)).clearApplicationUserData(); // note: it has a return value!
            } else {
                String packageName = getApplicationContext().getPackageName();
                Runtime runtime = Runtime.getRuntime();
                runtime.exec("pm clear "+packageName);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onBackPressed() {
        finishAndRemoveTask();
    }
}