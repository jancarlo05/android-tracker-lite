package com.procuro.androidtruckerlite;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.fragment.app.Fragment;

import com.procuro.apimmdatamanagerlib.DateTimeFormat;
import com.procuro.apimmdatamanagerlib.User;

public class Profile_personal_fragment extends Fragment {

    View rootView;
    TextView name,usernames,employee_id,drivers_lic,insurance_policy,address,phone_number,email,date_birth,
            hire_date,certification,hazmat,mvr;

    public Profile_personal_fragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.profile_personal_fragment, container, false);
        name = rootView.findViewById(R.id.name);
        usernames = rootView.findViewById(R.id.username);
        employee_id = rootView.findViewById(R.id.employee_id);
        drivers_lic = rootView.findViewById(R.id.driver_lic);
        insurance_policy = rootView.findViewById(R.id.insurance_policy);
        address = rootView.findViewById(R.id.address);
        phone_number = rootView.findViewById(R.id.phone_number);
        email = rootView.findViewById(R.id.email);
        date_birth = rootView.findViewById(R.id.date_birth);
        hire_date = rootView.findViewById(R.id.hire_date);
        certification = rootView.findViewById(R.id.certification);
        hazmat = rootView.findViewById(R.id.hazmat);
        mvr = rootView.findViewById(R.id.mvr);
        this.populateDriverProfile();
        return rootView;
    }

    private void populateDriverProfile() {
        TruckerDataManager truckerDataManager = TruckerDataManager.getInstance();
        User user = truckerDataManager.getUser();
        name.setText(user.getFullName());
        usernames.setText(user.username);
        employee_id.setText(user.employeeID);
        drivers_lic.setText("");
        insurance_policy.setText("");
        address.setText(user.address1);
        phone_number.setText(user.cellPhone);
        email.setText(user.contactEmail);
        if(user.DOB != null) {
            String birthDate = DateTimeFormat.ShortFormatStringForDateTime(user.DOB);
            date_birth.setText(birthDate);
        }
        if(user.hireDate != null) {
            String hireDate = DateTimeFormat.ShortFormatStringForDateTime(user.hireDate);
            hire_date.setText(hireDate);
        }
    }
}