package com.procuro.androidtruckerlite;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.procuro.androidtruckerlite.DBHelper.DBHelper;
import com.procuro.apimmdatamanagerlib.User;
import com.procuro.apimmdatamanagerlib.aPimmDataManager;

import de.hdodenhof.circleimageview.CircleImageView;


public class Help extends AppCompatActivity {

     ImageButton back;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.help);

        back = findViewById(R.id.back);

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Help.this, MainActivity.class);
                startActivity(intent);
                finish();
            }
        });

        final TruckerDataManager truckerDataManager = TruckerDataManager.getInstance();
        User user = truckerDataManager.getUser();
        CircleImageView  imageView = findViewById(R.id.pp);
        DBHelper dbHelper = new DBHelper(this);
        imageView.setImageBitmap(dbHelper.gimage());
        TextView profile_name = findViewById(R.id.profile_name);
        profile_name.setText(user.getFullName());














    }
}
