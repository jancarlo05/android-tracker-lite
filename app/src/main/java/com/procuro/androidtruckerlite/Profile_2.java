package com.procuro.androidtruckerlite;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;
import android.widget.RadioButton;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;

import com.procuro.androidtruckerlite.DBHelper.DBHelper;
import com.procuro.apimmdatamanagerlib.User;

import dmax.dialog.SpotsDialog;
import info.hoang8f.android.segmented.SegmentedGroup;

public class Profile_2 extends AppCompatActivity {

    RadioButton personal, vehicle, tractor, trailer;
    ImageButton edit, back;
    SegmentedGroup segmented2;
    SpotsDialog mDialog;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.profile);

        mDialog = (SpotsDialog) new SpotsDialog.Builder().setContext(this).build();

        getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container,
                new Profile_personal_fragment()).commit();

        personal = findViewById(R.id.personal);
        edit = findViewById(R.id.edit);
        vehicle = findViewById(R.id.vehicle);
        tractor = findViewById(R.id.tractor);
        trailer = findViewById(R.id.trailer);
        segmented2 = findViewById(R.id.segmented22);
        back = findViewById(R.id.back);
        segmented2.setVisibility(View.GONE);

        final TruckerDataManager truckerDataManager = TruckerDataManager.getInstance();
        User user = truckerDataManager.getUser();
        DBHelper dbHelper = new DBHelper(this);
        TextView profile_name = findViewById(R.id.profile_name);
        profile_name.setText(user.getFullName());

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Profile_2.this, Welcome_2.class);
                startActivity(intent);
                finish();
            }
        });

        personal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container,
                        new Profile_personal_fragment()).commit();
                segmented2.setVisibility(View.GONE);
            }
        });

        vehicle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                segmented2.setVisibility(View.VISIBLE);
                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container,
                        new Profile_Vehicle_tractor_fragment()).commit();
            }
        });

        tractor.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container,
                        new Profile_Vehicle_tractor_fragment()).commit();
            }
        });

        trailer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container,
                        new Profile_Vehicle_trailer_fragment()).commit();
            }
        });

        edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (personal.isChecked()) {
                    Intent intent = new Intent(Profile_2.this, Profile_edit_personal.class);
                    startActivity(intent);
                    finish();

                }
                if (vehicle.isChecked() && tractor.isChecked()) {
                    Intent intent = new Intent(Profile_2.this, Profile_edit_tractor.class);
                    startActivity(intent);
                    finish();
                }
                if (vehicle.isChecked() && trailer.isChecked()) {

                    Intent intent = new Intent(Profile_2.this, Profile_edit_trailer.class);
                    startActivity(intent);
                    finish();
                }
            }

        });
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(Profile_2.this, Welcome_2.class);
        startActivity(intent);
        finish();
    }
}
