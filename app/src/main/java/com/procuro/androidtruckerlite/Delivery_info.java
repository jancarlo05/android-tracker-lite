package com.procuro.androidtruckerlite;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;

import com.github.gcacace.signaturepad.views.SignaturePad;
import com.procuro.androidtruckerlite.DBHelper.DBHelper;
import com.procuro.androidtruckerlite.Utils.Utils;
import com.procuro.apimmdatamanagerlib.AssignmentInfo;
import com.procuro.apimmdatamanagerlib.SdrGisSite;
import com.procuro.apimmdatamanagerlib.SdrPointOfInterest;
import com.procuro.apimmdatamanagerlib.SdrStop;
import com.procuro.apimmdatamanagerlib.StoreDeliveryEnums;
import com.procuro.apimmdatamanagerlib.User;
import com.procuro.apimmdatamanagerlib.aPimmDataManager;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import static com.procuro.androidtruckerlite.Dc_info.PERMISSIONS_STORAGE;
import static com.procuro.androidtruckerlite.Dc_info.REQUEST_EXTERNAL_STORAGE;

public class Delivery_info extends Fragment {

    public Delivery_info() {
        // Required empty public constructor
    }

    aPimmDataManager dataManager = aPimmDataManager.getInstance();
    final TruckerDataManager truckerDataManager = TruckerDataManager.getInstance();

    public ArrayList<SdrGisSite> Sites = truckerDataManager.getSdrReport().Hierarchy.GIS.Sites;
    public SdrGisSite site;
    public ArrayList<SdrStop> Stops = truckerDataManager.getSdrReport().Hierarchy.GIS.Stops;
    public  SdrStop stop = null;
    public ArrayList<SdrPointOfInterest> Poi = truckerDataManager.getSdrReport().Hierarchy.GIS.PointsOfInterest;
    public SdrPointOfInterest poi = null;
    DBHelper dbHelper = null;
    ArrayList<Routes> routes = TruckerDataManager.getInstance().getRoutes();
    int j ;

    LinearLayout signature_container;
    static final int REQUEST_EXTERNAL_STORAGE = 1;
    static String[] PERMISSIONS_STORAGE = {Manifest.permission.WRITE_EXTERNAL_STORAGE};
    ImageView getsignature;
    Routes item = null;

    public static TextView dc_name,dc_address,shed_arrival,actual_arrival,delivery_start,delivery_end,departure,ontime,duration,signature_label;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
       final View rootView = inflater.inflate(R.layout.pod, container, false);

        site = Sites.get(0);
        User user = truckerDataManager.getUser();
        ArrayList<AssignmentInfo> assignmentInfo = truckerDataManager.assignmentInfo();
        AssignmentInfo assign = assignmentInfo.get(0);
        dbHelper = new DBHelper(rootView.getContext());


        dc_name = (TextView) rootView.findViewById(R.id.dc_name);
        dc_address = (TextView) rootView.findViewById(R.id.dc_address);
        shed_arrival = (TextView) rootView.findViewById(R.id.sched_arrival);
        actual_arrival = (TextView) rootView.findViewById(R.id.actual_arrival);
        delivery_start = (TextView) rootView.findViewById(R.id.delivery_start);
        delivery_end = (TextView) rootView.findViewById(R.id.delivey_end);
        departure = (TextView) rootView.findViewById(R.id.departure);
        ontime = (TextView) rootView.findViewById(R.id.Ontime);
        duration = (TextView) rootView.findViewById(R.id.duration);
        getsignature = rootView.findViewById(R.id.signature);
        signature_container = rootView.findViewById(R.id.signature_container);
        Delivery_info.dc_name.setText(site.SiteName);
        Delivery_info.dc_address.setText(site.Address);

        Button pod = rootView.findViewById(R.id.pod);


        MainActivity.hide();
        MainActivity.back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                User user = TruckerDataManager.getInstance().getUser();
                MainActivity.fullname.setText(user.getFullName());
                AppCompatActivity activity;
                activity = (AppCompatActivity) view.getContext();
                activity.getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container,
                        new DeliveryFragment()).commit();
                MainActivity.show();

            }
        });


        Bundle bundle = this.getArguments();
        if (bundle != null) {
            int i = bundle.getInt("position");
             item = routes.get(i);
             int j = bundle.getInt("route");
             stop = Stops.get(j);
        }

        if (item.getDelivery_confirmation_time() == null){
            signature_container.setVisibility(View.GONE);
        }else {
            signature_container.setVisibility(View.VISIBLE);
        }

        if (item.getDelivery_number() == 1){
            getsignature.setImageBitmap(dbHelper.getsignature1());
        }else if(item.getDelivery_number() == 2){
            getsignature.setImageBitmap(dbHelper.getsignature2());
        }else if(item.getDelivery_number() == 3){
            getsignature.setImageBitmap(dbHelper.getsignature3());
        }else if(item.getDelivery_number() == 4){
            getsignature.setImageBitmap(dbHelper.getsignature4());
        }
        if (item.getArrival_time() == null){
            pod.setEnabled(false);
        }else {
            pod.setEnabled(true);
        }


        pod.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                departure_alert(view.getContext());
            }
        });

        SimpleDateFormat outputFormat = new SimpleDateFormat("MMM dd, yyyy h:mm a");

        if (stop.ExpectedDeliveryTime == null){
            shed_arrival.setText("---");
        }else {
            shed_arrival.setText(outputFormat.format(stop.ExpectedDeliveryTime));
        }
        if(item.getArrival_time() == null){
            actual_arrival.setText("---");
            delivery_start.setText("---");
        }else {
            actual_arrival.setText(outputFormat.format(item.getArrival_time()));
            delivery_start.setText(outputFormat.format(item.getArrival_time()));
        }
        if (item.getDeparture_time() == null){
            delivery_end.setText("---");
            departure.setText("---");
        }else {
            delivery_end.setText(outputFormat.format(item.getDeparture_time()));
            departure.setText(outputFormat.format(item.getDeparture_time()));
        }
        ontime.setText("---");
        if (stop.DurationText == null){
            duration.setText("---");
        }
        duration.setText(stop.DurationText);


        return rootView;
    }

    public void departure_alert(final Context context){
        final SignaturePad mSignaturePad;
        final Button mClearButton;
        final Button mSaveButton;
        LayoutInflater li = LayoutInflater.from(context);
        View view = li.inflate(R.layout.signature_pad, null);
        final android.app.AlertDialog.Builder alertDialogBuilder = new android.app.AlertDialog.Builder(context);
        mSignaturePad = (SignaturePad) view.findViewById(R.id.signature_pad);
        mClearButton = (Button) view.findViewById(R.id.clear_button);
        mSaveButton = (Button) view.findViewById(R.id.save_button);
        final TextView signature_label = (TextView) view.findViewById(R.id.signature_label);
        alertDialogBuilder.setView(view);
        final android.app.AlertDialog alertDialogCongratulations = alertDialogBuilder.create();
        alertDialogCongratulations.getWindow().setLayout(LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);//
        alertDialogCongratulations.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        alertDialogCongratulations.setCancelable(true);
        alertDialogCongratulations.show();

        dbHelper = new DBHelper(view.getContext());

        signature_label.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                signature_label.setVisibility(View.GONE);
                return false;
            }
        });

        mSignaturePad.setOnSignedListener(new SignaturePad.OnSignedListener() {
            @Override
            public void onStartSigning() {
                Toast.makeText(context, "OnStartSigning", Toast.LENGTH_SHORT).show();
                signature_label.setVisibility(View.GONE);
            }
            @Override
            public void onSigned() {
                mSaveButton.setEnabled(true);
                mClearButton.setEnabled(true);
            }
            @Override
            public void onClear() {
                mSaveButton.setEnabled(false);
                mClearButton.setEnabled(false);
                signature_label.setVisibility(View.VISIBLE);
            }
        });

        mClearButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mSignaturePad.clear();
            }
        });
        mSaveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Bitmap signatureBitmap = mSignaturePad.getSignatureBitmap();
                Date currentTime = Calendar.getInstance().getTime();
                if (addJpgSignatureToGallery(signatureBitmap)) {
//                    Toast.makeText(getContext().getApplicationContext(), "Signature saved into the Gallery", Toast.LENGTH_SHORT).show();
                } else {
//                    Toast.makeText(getContext().getApplicationContext(), "Unable to store the signature", Toast.LENGTH_SHORT).show();
                }
                if (addSvgSignatureToGallery(mSignaturePad.getSignatureSvg())) {
//                    Toast.makeText(getContext().getApplicationContext(), "SVG Signature saved into the Gallery", Toast.LENGTH_SHORT).show();
                } else {
//                    Toast.makeText(getContext().getApplicationContext(), "Unable to store the SVG signature", Toast.LENGTH_SHORT).show();
                }
                item.setDelivery_confirmation_time(currentTime);
                Toast.makeText(getContext().getApplicationContext(), ""+item.getDelivery_confirmation_time(), Toast.LENGTH_SHORT).show();
                signature_container.setVisibility(View.VISIBLE);
                mSignaturePad.clear();
                alertDialogCongratulations.dismiss();
            }
        });
    }
    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           @NonNull String permissions[], @NonNull int[] grantResults) {
        switch (requestCode) {
            case REQUEST_EXTERNAL_STORAGE: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length <= 0
                        || grantResults[0] != PackageManager.PERMISSION_GRANTED) {
                    Toast.makeText(getContext().getApplicationContext(), "Cannot write images to external storage", Toast.LENGTH_SHORT).show();
                }
            }
        }
    }
    public File getAlbumStorageDir(String albumName) {
        // Get the directory for the user's public pictures directory.
        File file = new File(Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_PICTURES), albumName);
        if (!file.mkdirs()) {
            Log.e("SignaturePad", "Directory not created");
        }
        return file;
    }

    public void saveBitmapToJPG(Bitmap bitmap, File photo) throws IOException {
        Bitmap newBitmap = Bitmap.createBitmap(bitmap.getWidth(), bitmap.getHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(newBitmap);
        canvas.drawColor(Color.BLUE);
        canvas.drawBitmap(bitmap, 0, 0, null);
        OutputStream stream = new FileOutputStream(photo);
        newBitmap.compress(Bitmap.CompressFormat.PNG, 50, stream);



        stream.close();
    }

    public boolean addJpgSignatureToGallery(Bitmap signature) {
        boolean result = false;

        if (item.getDelivery_number() == 1){
            dbHelper.deletesignature1();
            dbHelper.signature1(Utils.getsignature(signature));
            getsignature.setImageBitmap(dbHelper.getsignature1());
        }else if (item.getDelivery_number() == 2){
            dbHelper.deletesignature2();
            dbHelper.signature2(Utils.getsignature(signature));
            getsignature.setImageBitmap(dbHelper.getsignature2());
        }
        else if (item.getDelivery_number() == 3){
            dbHelper.deletesignature3();
            dbHelper.signature3(Utils.getsignature(signature));
            getsignature.setImageBitmap(dbHelper.getsignature3());
        }
        else if (item.getDelivery_number() == 4){
            dbHelper.deletesignature4();
            dbHelper.signature4(Utils.getsignature(signature));
            getsignature.setImageBitmap(dbHelper.getsignature4());
        }




//            scanMediaFile(photo);
        result = true;

        return result;
    }

    private void scanMediaFile(File photo) {
        Intent mediaScanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
        Uri contentUri = Uri.fromFile(photo);
        mediaScanIntent.setData(contentUri);
        getContext().sendBroadcast(mediaScanIntent);
    }

    public boolean addSvgSignatureToGallery(String signatureSvg) {
        boolean result = false;
        try {
            File svgFile = new File(getAlbumStorageDir("SignaturePad"), String.format("Signature_%d.svg", System.currentTimeMillis()));
            OutputStream stream = new FileOutputStream(svgFile);
            OutputStreamWriter writer = new OutputStreamWriter(stream);

            writer.write(signatureSvg);
            writer.close();
            stream.flush();
            stream.close();
            scanMediaFile(svgFile);
            result = true;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return result;
    }

    public static void verifyStoragePermissions(Activity activity) {
        // Check if we have write permission
        int permission = ActivityCompat.checkSelfPermission(activity, Manifest.permission.WRITE_EXTERNAL_STORAGE);
        if (permission != PackageManager.PERMISSION_GRANTED) {
            // We don't have permission so prompt the user
            ActivityCompat.requestPermissions(
                    activity,
                    PERMISSIONS_STORAGE,
                    REQUEST_EXTERNAL_STORAGE
            );
        }
    }
}

