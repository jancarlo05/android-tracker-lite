package com.procuro.androidtruckerlite;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import com.baoyz.swipemenulistview.SwipeMenuListView;
import com.daimajia.swipe.SwipeLayout;
import com.daimajia.swipe.adapters.RecyclerSwipeAdapter;
import com.procuro.apimmdatamanagerlib.AssignmentInfo;
import com.procuro.apimmdatamanagerlib.DateTimeFormat;
import com.procuro.apimmdatamanagerlib.JSONDate;
import com.procuro.apimmdatamanagerlib.RouteShipment;
import com.procuro.apimmdatamanagerlib.SdrGisSite;
import com.procuro.apimmdatamanagerlib.SdrPointOfInterest;
import com.procuro.apimmdatamanagerlib.SdrStop;
import com.procuro.apimmdatamanagerlib.User;
import com.procuro.apimmdatamanagerlib.aPimmDataManager;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;

public class SwipeRecyclerViewAdapter extends RecyclerSwipeAdapter<SwipeRecyclerViewAdapter.SimpleViewHolder> {

    private Context mContext;
    aPimmDataManager dataManager = aPimmDataManager.getInstance();
    final TruckerDataManager truckerDataManager = TruckerDataManager.getInstance();
    private ArrayList<Routes> studentList = TruckerDataManager.getInstance().getRoutes();
    public ArrayList<SdrGisSite> Sites = truckerDataManager.getSdrReport().Hierarchy.GIS.Sites;
    public SdrGisSite site;
    public ArrayList<SdrStop> Stops = truckerDataManager.getSdrReport().Hierarchy.GIS.Stops;
    public  SdrStop stop = null;
    public ArrayList<SdrPointOfInterest> Poi = truckerDataManager.getSdrReport().Hierarchy.GIS.PointsOfInterest;
    public SdrPointOfInterest poi = null;
    private int counter_stop = 1;
    boolean arrival_array = true;
    private  int counter_skip= 0;

    SwipeRecyclerViewAdapter(Context context, ArrayList<Routes> objects) {
        this.mContext = context;
        this.studentList = objects;
        notifyDataSetChanged();
    }

    @Override
    public SimpleViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.swipe_layout, parent, false);
        return new SimpleViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final SimpleViewHolder viewHolder, final int position) {

        final Routes item = studentList.get(position);

        Date dcArrivalTime = TruckerDataManager.getInstance().getSdrReport().Hierarchy.ArrivalTime;
        Date dcdeparturetime = TruckerDataManager.getInstance().getSdrReport().Hierarchy.DepartureTime;

        if (item.getSite_desc().equalsIgnoreCase("Site_departure")){
            viewHolder.swipeLayout.setRightSwipeEnabled(false);
            viewHolder.swipeLayout.isRightSwipeEnabled();
            if (Poi.size() >0 || item.getDeparture_time() != null) {
                viewHolder.Icons.setBackgroundResource(R.drawable.dc_blue);
            }else {
                viewHolder.Icons.setBackgroundResource(R.drawable.dc_black);
            }
        }
        if (item.getSite_desc().equalsIgnoreCase("Site_arrival")){
            viewHolder.swipeLayout.setRightSwipeEnabled(false);
            viewHolder.swipeLayout.isRightSwipeEnabled();
            if (item.getArrival_time()!= null) {
                viewHolder.Icons.setBackgroundResource(R.drawable.dc_blue);
            }else {
                viewHolder.Icons.setBackgroundResource(R.drawable.dc_black);
            }

        }
        if (item.getSite_desc().equalsIgnoreCase("Poi")){

            viewHolder.swipeLayout.setRightSwipeEnabled(false);
            viewHolder.swipeLayout.isRightSwipeEnabled();
            if (item.getPoi_type() == 50) {
                viewHolder.Icons.setBackgroundResource(R.drawable.poi_start_point);
            } else if (item.getPoi_type() == 9) {
                viewHolder.Icons.setBackgroundResource(R.drawable.poi_coffee);
            } else if (item.getPoi_type() == 0) {
                viewHolder.Icons.setBackgroundResource(R.drawable.poi_delivery);

            } else if (item.getPoi_type() == 2) {
                viewHolder.Icons.setBackgroundResource(R.drawable.poi_fuel);

            } else if (item.getPoi_type() == 3) {
                viewHolder.Icons.setBackgroundResource(R.drawable.poi_plant_pick_up);
                //DeliveryPOIType_OutOfTerritory = 4
            }  else if (item.getPoi_type() == 5) {
                viewHolder.Icons.setBackgroundResource(R.drawable.poi_rest);

            } else if (item.getPoi_type() == 6) {
                viewHolder.Icons.setBackgroundResource(R.drawable.poi_inspection);

            } else if (item.getPoi_type() == 7) {
                viewHolder.Icons.setBackgroundResource(R.drawable.poi_service);

            } else if (item.getPoi_type() == 8) {
                viewHolder.Icons.setBackgroundResource(R.drawable.poi_layover);

            } else if (item.getPoi_type() == 11) {
                viewHolder.Icons.setBackgroundResource(R.drawable.poi_traffic);

            } else if (item.getPoi_type() == 13) {
                viewHolder.Icons.setBackgroundResource(R.drawable.poi_wash);

            }  else if (item.getPoi_type() == 51) {
                viewHolder.Icons.setBackgroundResource(R.drawable.poi_end_point);

                //DeliveryPOIType_DCFacility = 61,
            } else{
                viewHolder.Icons.setBackgroundResource(R.drawable.poi_undefined);
            }
        }

        if (item.getSite_desc().equalsIgnoreCase("Stop")) {
            viewHolder.swipeLayout.setRightSwipeEnabled(true);
            viewHolder.swipeLayout.isRightSwipeEnabled();
            stop = Stops.get(item.getSpecific_position());
            if (item.getArrival_time() != null) {
                viewHolder.Icons.setBackgroundResource(R.drawable.store_delivery_ontime_blue);
                if (item.getDeparture_time() != null) {
                    viewHolder.Icons.setBackgroundResource(R.drawable.store_delivery_ontime_blue_green_check);
                    if (item.getDelivery_confirmation_time() == null) {
                        viewHolder.Icons.setBackgroundResource(R.drawable.store_delivery_ontime_red_check);
                    }
                } else {
                    if (item.getDelivery_confirmation_time() != null) {
                        viewHolder.Icons.setBackgroundResource(R.drawable.store_delivery_ontime_blue_gray_check);
                    }
                }
            } else {
                viewHolder.Icons.setBackgroundResource(R.drawable.store_delivert_ontime_gray);
            }
        }

        if (item.getArrival_time()== null) {
            viewHolder.btm_wrapper.setBackground(new ColorDrawable(Color.parseColor("#009444")));
        }
        else if (item.getDeparture_time() == null){
            viewHolder.btm_wrapper.setBackground(new ColorDrawable(Color.parseColor("#FF4C00")));
        } else{
            viewHolder.btm_wrapper.setBackgroundColor(Color.GRAY);
        }



        viewHolder.Route_name.setText(item.getRoute_name());
        viewHolder.Address.setText(item.getAddress());
        viewHolder.swipeLayout.setShowMode(SwipeLayout.ShowMode.PullOut);
        viewHolder.swipeLayout.addDrag(SwipeLayout.DragEdge.Right, viewHolder.swipeLayout.findViewById(R.id.bottom_wraper));

        viewHolder.swipeLayout.addSwipeListener(new SwipeLayout.SwipeListener() {

            @Override
            public void onStartOpen(SwipeLayout layout) {

            }

            @Override
            public void onOpen(SwipeLayout layout) {


            }

            @Override
            public void onStartClose(SwipeLayout layout) {


            }

            @Override
            public void onClose(SwipeLayout layout) {


            }

            @Override
            public void onUpdate(SwipeLayout layout, int leftOffset, int topOffset) {


            }

            @Override
            public void onHandRelease(SwipeLayout layout, float xvel, float yvel) {

            }
        });

        viewHolder.swipeLayout.getSurfaceView().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (item.getSite_desc().equalsIgnoreCase("Stop")){
                    Bundle bundle = new Bundle();
                    bundle.putInt("position", position);
                    bundle.putInt("route",item.getSpecific_position());
                    Fragment fragment2 = new Delivery_info();
                    fragment2.setArguments(bundle);
                    ((AppCompatActivity) v.getContext()).getSupportFragmentManager()
                            .beginTransaction()
                            .replace(R.id.fragment_container, fragment2)
                            .commit();
                    MainActivity.hide();
                    stop = Stops.get(item.getSpecific_position());
                    MainActivity.fullname.setText(stop.SiteName);
                }
            }
        });



        viewHolder.btnLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(v.getContext(), "Clicked on Information " + viewHolder.Route_name.getText().toString() + " " + item.getSpecific_position(), Toast.LENGTH_SHORT).show();
                AppCompatActivity activity = (AppCompatActivity) v.getContext();

                if (item.getSite_desc().equalsIgnoreCase("Site_arrival")){

                    activity = (AppCompatActivity) v.getContext();
                    activity.getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container,
                            new Dc_info()).commit();
                    MainActivity.hide();
                    site = Sites.get(item.getSpecific_position());

                    User user = truckerDataManager.getUser();
                    ArrayList<AssignmentInfo> assignmentInfo = truckerDataManager.assignmentInfo();
                    AssignmentInfo assign = assignmentInfo.get(0);
                    MainActivity.fullname.setText(site.SiteName);

                }


                if (item.getSite_desc().equalsIgnoreCase("site_departure")){
                    activity = (AppCompatActivity) v.getContext();
                    activity.getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container,
                            new Dc_info()).commit();
                    MainActivity.hide();
                    site = Sites.get(item.getSpecific_position());

                    User user = truckerDataManager.getUser();
                    ArrayList<AssignmentInfo> assignmentInfo = truckerDataManager.assignmentInfo();
                    AssignmentInfo assign = assignmentInfo.get(0);
                    MainActivity.fullname.setText(site.SiteName);

                }

                if (item.getSite_desc().equalsIgnoreCase("Poi")){
                    if (item.getPoi_type() == 50){
                        activity = (AppCompatActivity) v.getContext();
                        activity.getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container,
                                new Dc_info()).commit();
                        MainActivity.hide();
                        MainActivity.fullname.setText(item.getRoute_name());
                    }else if(item.getPoi_type() == 51){
                        activity = (AppCompatActivity) v.getContext();
                        activity.getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container,
                                new Dc_info()).commit();
                        MainActivity.hide();
                        MainActivity.fullname.setText(item.getRoute_name());
                    }else {
                        Bundle bundle = new Bundle();
                        bundle.putInt("position", position);
                        Fragment fragment2 = new Poi_info();
                        fragment2.setArguments(bundle);

                        ((AppCompatActivity) v.getContext()).getSupportFragmentManager()
                                .beginTransaction()
                                .replace(R.id.fragment_container, fragment2)
                                .commit();
                        MainActivity.hide();
                        MainActivity.poi_save.setVisibility(View.VISIBLE);
                        MainActivity.fullname.setText("Modify POI");
                    }

                }

                if (item.getSite_desc().equalsIgnoreCase("Stop")){
                    Bundle bundle = new Bundle();
                    bundle.putInt("position",item.getSpecific_position());
                    Fragment fragment2 =  new Stops_info();
                    fragment2.setArguments(bundle);
                    ((AppCompatActivity) v.getContext()).getSupportFragmentManager()
                            .beginTransaction()
                            .replace(R.id.fragment_container, fragment2)
                            .commit();
                    MainActivity.hide();
                    stop = Stops.get(item.getSpecific_position());
                    MainActivity.fullname.setText(stop.SiteName);
                    System.out.println(item.getSpecific_position());

                }
            }
        });


        viewHolder.Delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if  (item.getArrival_time() == null){
                    manual_delivery_alert(item.getRoute_name(),position,v.getContext(),viewHolder);

                }
                else if (item.getDeparture_time()==null){
                    manual_store_depart_alert(position,v.getContext(),viewHolder);
                }
                counter_skip = 0;
                counter_stop =1;
                mItemManger.removeShownLayouts(viewHolder.swipeLayout);
                notifyItemRangeChanged(position, studentList.size());
                closeItem(position);
            }

        });
        mItemManger.bindView(viewHolder.itemView, position);
    }

    @Override
    public int getItemCount() {
        return studentList.size();
    }

    @Override
    public int getSwipeLayoutResourceId(int position) {
        return R.id.swipe;

    }

    public static class SimpleViewHolder extends RecyclerView.ViewHolder{
        public SwipeLayout swipeLayout;
        public TextView Route_name;
        public TextView Tractor_name;
        public TextView Address;
        public TextView EmailId;
        public ImageButton Delete;
        public ImageView Icons;
        public ImageButton btnLocation;
        public LinearLayout btm_wrapper;
        public SimpleViewHolder(View itemView) {
            super(itemView);
            swipeLayout = (SwipeLayout) itemView.findViewById(R.id.swipe);
            Route_name = (TextView) itemView.findViewById(R.id.Name);
            Delete = (ImageButton) itemView.findViewById(R.id.Delete);
            btnLocation = (ImageButton) itemView.findViewById(R.id.btnLocation);
            Address = (TextView)itemView.findViewById(R.id.address);
            Icons = (ImageView)itemView.findViewById(R.id.Image_icon);
            btm_wrapper = (LinearLayout) itemView.findViewById(R.id.bottom_wraper);
        }
    }

    public void manual_store_depart_alert(final int position, Context context, final SimpleViewHolder viewHolder) {
        LayoutInflater li = (LayoutInflater) LayoutInflater.from(context);
        View view = li.inflate(R.layout.manual_store_depart_alert, null);
        final android.app.AlertDialog.Builder alertDialogBuilder = new android.app.AlertDialog.Builder(context);

        SimpleDateFormat inputFormat = new SimpleDateFormat(
                "EEE MMM dd HH:mm:ss 'GMT' yyyy", Locale.US);
        inputFormat.setTimeZone(TimeZone.getTimeZone("Etc/UTC"));
        SimpleDateFormat outputFormat = new SimpleDateFormat("MMM dd, yyyy h:mm a");
        Date date = new Date();
        TextView address = view.findViewById(R.id.alert_sitename);
        Button accept = view.findViewById(R.id.accept);
        Button cancel = view.findViewById(R.id.cancel);

        address.setText(outputFormat.format(date));
        alertDialogBuilder.setView(view);

        final android.app.AlertDialog alertDialogCongratulations = alertDialogBuilder.create();
        alertDialogCongratulations.getWindow().setLayout(LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);//
        alertDialogCongratulations.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        alertDialogCongratulations.setCancelable(false);
        alertDialogCongratulations.show();
        accept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Routes item = null;
                Date currentTime = Calendar.getInstance().getTime();
                Date date = new Date();
                item = studentList.get(position);
                int counter_skip =0;
                for (int i = 0; i <Stops.size()+Poi.size(); i++) {
                    final Routes skip = studentList.get(i);
                    if (skip.getSite_desc().contains("Stop")){
                        if (skip.getArrival_time()!=null){
                            counter_skip++;

                            System.out.println("counter Skip:" + counter_skip);
                        }
                    }else if (skip.getSite_desc().contains("Poi")){
                        if (skip.getArrival_time()!=null){
                            counter_skip++;
                            System.out.println("counter Skip poi:" + counter_skip);
                        }
                    }
                }
                item.setDeparture_time(currentTime);
                Collections.sort(studentList.subList((counter_skip+1),studentList.size()-1), new Comparator<Routes>() {
                    @Override
                    public int compare(Routes routes, Routes t1) {
                        if (t1.getArrival_time() == null) {
                            return (routes.getArrival_time() == null) ? 0 : -1;
                        }
                        if (routes.getArrival_time() == null) {
                            return 1;
                        }
                        return routes.getArrival_time().compareTo(t1.getArrival_time());
                    }
                });
                counter_stop =1;
                alertDialogCongratulations.dismiss();
                mItemManger.removeShownLayouts(viewHolder.swipeLayout);
                notifyItemRangeChanged(position, studentList.size());
                closeItem(position);

            }
        });
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alertDialogCongratulations.dismiss();

            }
        });

    }

    public void manual_delivery_alert(String location_names , final int position, Context context, final SimpleViewHolder viewHolder) {
        LayoutInflater li = (LayoutInflater) LayoutInflater.from(context);
        View view = li.inflate(R.layout.manual_delivery_alert, null);
        final android.app.AlertDialog.Builder alertDialogBuilder = new android.app.AlertDialog.Builder(context);
        TextView address = view.findViewById(R.id.alert_sitename);
        Button accept = view.findViewById(R.id.accept);
        Button cancel = view.findViewById(R.id.cancel);
        address.setText(location_names);
        alertDialogBuilder.setView(view);
        final android.app.AlertDialog alertDialogCongratulations = alertDialogBuilder.create();
        alertDialogCongratulations.getWindow().setLayout(LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);//
        alertDialogCongratulations.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        alertDialogCongratulations.setCancelable(false);
        alertDialogCongratulations.show();

        accept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Routes item = null;
                Date currentTime = Calendar.getInstance().getTime();
                Date date = new Date();
                item = studentList.get(position);
                Toast.makeText(mContext, " Click : " + item.getRoute_name() + " \n" + date, Toast.LENGTH_SHORT).show();

                int counter_skip =0;
                for (int i = 0; i <Stops.size()+Poi.size(); i++) {
                    final Routes skip = studentList.get(i);
                    if (skip.getSite_desc().contains("Stop")){
                        if (skip.getArrival_time()!=null){
                            System.out.println("counter Skip:" + counter_skip);
                            counter_skip++;

                        }
                    }else if (skip.getSite_desc().contains("Poi")){
                        if (skip.getArrival_time()!=null){
                            System.out.println("counter Skip poi:" + counter_skip);
                            counter_skip++;
                        }
                    }
                }
                item.setArrival_time(currentTime);
                Collections.sort(studentList.subList(counter_skip+1,studentList.size()-1), new Comparator<Routes>() {
                    @Override
                    public int compare(Routes routes, Routes t1) {
                        if (t1.getArrival_time() == null) {
                            return (routes.getArrival_time() == null) ? 0 : -1;
                        }
                        if (routes.getArrival_time() == null) {
                            return 1;
                        }
                        return routes.getArrival_time().compareTo(t1.getArrival_time());
                    }
                });
                counter_stop =1;
                alertDialogCongratulations.dismiss();
                mItemManger.removeShownLayouts(viewHolder.swipeLayout);
                notifyItemRangeChanged(position, studentList.size());
                closeItem(position);

            }
        });
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alertDialogCongratulations.dismiss();

            }
        });

    }
}
