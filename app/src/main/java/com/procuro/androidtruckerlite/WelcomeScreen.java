package com.procuro.androidtruckerlite;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.Build;
import android.os.Bundle;
import android.os.Looper;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.CustomTarget;
import com.bumptech.glide.request.transition.Transition;
import com.google.android.gms.location.Geofence;
import com.procuro.androidlocationservice.LocationService;
import com.procuro.androidtruckerlite.DBHelper.DBHelper;
import com.procuro.androidtruckerlite.Utils.Utils;
import com.procuro.apimmdatamanagerlib.AssignmentInfo;
import com.procuro.apimmdatamanagerlib.OnCompleteListeners;
import com.procuro.apimmdatamanagerlib.SdrGisSite;
import com.procuro.apimmdatamanagerlib.SdrPointOfInterest;
import com.procuro.apimmdatamanagerlib.SdrReport;
import com.procuro.apimmdatamanagerlib.SdrStop;
import com.procuro.apimmdatamanagerlib.User;
import com.procuro.apimmdatamanagerlib.aPimmDataManager;

import java.text.SimpleDateFormat;
import java.util.ArrayList;

import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.CountDownTimer;
import android.os.Handler;
import android.provider.MediaStore;
import android.widget.Toast;
import com.afollestad.materialdialogs.MaterialDialog;


import java.io.IOException;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;

import de.hdodenhof.circleimageview.CircleImageView;
import dmax.dialog.SpotsDialog;



public class WelcomeScreen extends AppCompatActivity {
    ImageButton route;
    Button line_haul, shuttle;
    TextView notif;
    public String site_name;
    public String site_address;

    private ProgressDialog progressBar;
    private int progressBarStatus = 0;
    private Handler progressBarbHandler = new Handler();

    private static final int CAPTURE_PHOTO = 2;
    private static final int SELECT_PHOTO = 1;
    private static final int profile = 7777;
    private SpotsDialog spotsDialog;
    CircleImageView imageView;
    ImageButton upload;
    DBHelper dbHelper;

    private int show_popup = 0;
    private int arrival = 0 ;
    private int departure = 0 ;
    private double lat = 0;
    private double longt = 0;
    private String location_names ="";
    private String site_names ="";
    private String site_type ="";
    CountDownTimer countDownTimer;
    public ArrayList<Routes> mDataSet = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_welcome_screen);

        dbHelper = new DBHelper(this);
        route = findViewById(R.id.route);
        line_haul = findViewById(R.id.line_haul);
        shuttle = findViewById(R.id.shuttle);
        notif = findViewById(R.id.notif);
        imageView = findViewById(R.id.profile_image);
        upload = findViewById(R.id.upload);
        spotsDialog = (SpotsDialog) new SpotsDialog.Builder().setContext(this).setMessage("Fetching data").build();
        show_popup = 0;
        location_service();
        show_pop_timer();

        route.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(WelcomeScreen.this, Welcome_2.class);
                startActivity(intent);
                finish();
                show_popup = 0;
                countDownTimer.cancel();
            }
        });
        line_haul.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Intent intent = new Intent(WelcomeScreen.this, Welcome_2.class);
//                startActivity(intent);
//                finish();
            }
        });
        shuttle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Intent intent = new Intent(WelcomeScreen.this, Welcome_2.class);
//                startActivity(intent);
//                finish();
            }
        });
        upload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View view) {
                switch (view.getId()) {
                    case R.id.upload:
                        new MaterialDialog.Builder(WelcomeScreen.this)
                                .title("Upload Image")
                                .items(R.array.uploadImages)
                                .itemsIds(R.array.itemIds)
                                .itemsCallback(new MaterialDialog.ListCallback() {
                                    @Override
                                    public void onSelection(MaterialDialog dialog, View view, int which, CharSequence text) {
                                        switch (which) {
                                            case 0:
                                                Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
                                                photoPickerIntent.setType("image/*");
                                                startActivityForResult(photoPickerIntent, SELECT_PHOTO);
                                                imageView.setVisibility(View.VISIBLE);
                                                break;
                                            case 1:
                                                Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                                                startActivityForResult(intent, CAPTURE_PHOTO);
                                                imageView.setVisibility(View.VISIBLE);
                                                break;
                                            case 2:
                                                imageView.setVisibility(View.INVISIBLE);
                                                dbHelper.delete();
                                                break;
                                        }
                                    }
                                })
                                .show();
                        break;

                }
            }
        });
        //display Assignmentinfo
        display_assignment();
        //display Profile

        Glide.with(this)
                .asBitmap()
                .load(dbHelper.gimage())
                .into(new CustomTarget<Bitmap>() {
                    @Override
                    public void onResourceReady(@NonNull Bitmap resource, @Nullable Transition<? super Bitmap> transition) {
                        imageView.setImageBitmap(resource);
                    }
                    @Override
                    public void onLoadCleared(@Nullable Drawable placeholder) {
                    }
                });




    }
        @Override
        protected void onActivityResult ( int requestCode, int resultCode, @Nullable Intent data){
            super.onActivityResult(requestCode, resultCode, data);
            if (requestCode == SELECT_PHOTO && resultCode == RESULT_OK && data != null) {
                Uri uri = data.getData();
                setProgressBar();
                imageView.setImageURI(uri);
                new CountDownTimer(1000, 1000) {
                    public void onTick(long millisUntilFinished) {
                    }

                    public void onFinish() {
                        dbHelper.delete();
                        save();
                    }
                }.start();
            } else if (requestCode == CAPTURE_PHOTO && resultCode == RESULT_OK && data != null) {
//               Uri uri = data.getData();
                Bitmap thumbnail = (Bitmap) data.getExtras().get("data");
                setProgressBar();
//                imageView.setImageURI(uri);
                imageView.setImageBitmap(thumbnail);
                new CountDownTimer(1000, 1000) {
                    public void onTick(long millisUntilFinished) {
                    }

                    public void onFinish() {
                        dbHelper.delete();
                        save();
                    }
                }.start();

            }

        }


    public void display_assignment(){
        spotsDialog.show();
        final aPimmDataManager dataManager = aPimmDataManager.getInstance();
        final TruckerDataManager truckerDataManager = TruckerDataManager.getInstance();
        final User user = truckerDataManager.getUser();
        dataManager.getAssignmentForDCSite(user.defaultSiteID, new OnCompleteListeners.getAssignmentForDCSiteListener() {
            @Override
            public void getAssignmentForDCSite(ArrayList<AssignmentInfo> assignmentInfoList, Error error) {
                truckerDataManager.setAssignmentInfoList(assignmentInfoList);

                String totalRoutes = String.valueOf(assignmentInfoList.size());
                System.out.println("N O T I F : " + totalRoutes);
                notif.setText(totalRoutes);

                if (assignmentInfoList.size()> 0) {
                    AssignmentInfo assignmentInfo1 = assignmentInfoList.get(0);
                    dataManager.getSdrReportForDeliveryId(assignmentInfo1.shipmentID, false, null, new OnCompleteListeners.getSdrReportForDeliveryIdListener() {
                        @Override
                        public void GetSdrReportForDeliveryId(SdrReport sdrReport, Error error) {
                            truckerDataManager.setSdrReport(sdrReport);
                            loadData();
                            spotsDialog.dismiss();
                        }
                    });

                }else {
                    spotsDialog.dismiss();
                }
            }
        });
    }

    public void save(){
        Bitmap bitmap =((BitmapDrawable)imageView.getDrawable()).getBitmap();
        imageView.setImageBitmap(bitmap);
        dbHelper.addImage(Utils.getbytes(bitmap));
        Toast.makeText(getApplicationContext(),"Uploading Success",Toast.LENGTH_LONG).show();
    }

    public void setProgressBar(){
        progressBar = new ProgressDialog(this);
        progressBar.setCancelable(true);
        progressBar.setMessage("Please wait...");
        progressBar.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressBar.setProgress(0);
        progressBar.setMax(100);
        progressBar.show();
        progressBarStatus = 0;
        new Thread(new Runnable() {
            @Override
            public void run() {
                while (progressBarStatus < 100){
                    progressBarStatus += 100;

                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }

                    progressBarbHandler.post(new Runnable() {
                        @Override
                        public void run() {
                            progressBar.setProgress(progressBarStatus);
                        }
                    });
                }
                if (progressBarStatus >= 100) {
                    try {
                        Thread.sleep(2000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    progressBar.dismiss();
                }
            }
        }).start();
    }

    public void unsched_stop_alert(){
        show_popup = 0;
        LayoutInflater li = LayoutInflater.from(WelcomeScreen.this);
        View view = li.inflate(R.layout.unsched_stop_alert, null);
        final android.app.AlertDialog.Builder alertDialogBuilder = new android.app.AlertDialog.Builder(WelcomeScreen.this);
        TextView address = view.findViewById(R.id.location_name);
        address.setText(location_names);
        alertDialogBuilder.setView(view);

        final android.app.AlertDialog alertDialogCongratulations = alertDialogBuilder.create();
        alertDialogCongratulations.getWindow().setLayout(LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);//
        alertDialogCongratulations.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        alertDialogCongratulations.setCancelable(false);
        alertDialogCongratulations.show();

        new CountDownTimer(5000, 1000) {
            public void onTick(long millisUntilFinished) {
            }
            public void onFinish() {
                alertDialogCongratulations.dismiss();
                show_popup = 0 ;
            }
        }.start();

    }

    public void show_pop_timer(){
        countDownTimer = new  CountDownTimer(1000, 1000) {
            public void onTick(long millisUntilFinished) {
            }
            public void onFinish() {

                if (show_popup ==1){
                    unsched_stop_alert();
                }else if(arrival ==1){
                    arrival_alert();
                }else if(departure ==1){
                    departure_alert();
                }
                start();
            }
        }.start();
    }

    public void arrival_alert(){
        arrival = 0;
        LayoutInflater li = LayoutInflater.from(WelcomeScreen.this);
        View view = li.inflate(R.layout.poi_arrival_alert, null);
        final android.app.AlertDialog.Builder alertDialogBuilder = new android.app.AlertDialog.Builder(WelcomeScreen.this);
        TextView address = view.findViewById(R.id.alert_sitename);
        TextView type = view.findViewById(R.id.departure_type);
        type.setText(" ARRIVAL ");
        address.setText(site_names);
        alertDialogBuilder.setView(view);
        final android.app.AlertDialog alertDialogCongratulations = alertDialogBuilder.create();
        alertDialogCongratulations.getWindow().setLayout(LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);//
        alertDialogCongratulations.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        alertDialogCongratulations.setCancelable(false);
        alertDialogCongratulations.show();

        new CountDownTimer(2000, 1000) {
            public void onTick(long millisUntilFinished) {
            }
            public void onFinish() {
                alertDialogCongratulations.dismiss();
                arrival = 0;
            }
        }.start();
    }

    public void departure_alert(){
        departure = 0;
        LayoutInflater li = LayoutInflater.from(WelcomeScreen.this);
        View view = li.inflate(R.layout.poi_departure_alert, null);
        final android.app.AlertDialog.Builder alertDialogBuilder = new android.app.AlertDialog.Builder(WelcomeScreen.this);
        TextView address = view.findViewById(R.id.alert_sitename);
        TextView type = view.findViewById(R.id.departure_type);
        type.setText("DEPARTURE");
        address.setText(site_names);
        alertDialogBuilder.setView(view);

        final android.app.AlertDialog alertDialogCongratulations = alertDialogBuilder.create();
        alertDialogCongratulations.getWindow().setLayout(LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);//
        alertDialogCongratulations.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        alertDialogCongratulations.setCancelable(false);
        alertDialogCongratulations.show();

        new CountDownTimer(2000, 1000) {
            public void onTick(long millisUntilFinished) {
            }
            public void onFinish() {
                alertDialogCongratulations.dismiss();
                show_popup = 0 ;
                departure = 0;
            }
        }.start();

    }

    public void location_service(){

        LocationService.getInstance().setGeofenceTransitionListener(new LocationService.OnGeofenceTransitionListener() {
            @Override
            public void didEnterGeofence(Geofence geofence) {
                String siteName = TruckerDataManager.getInstance().getSiteNameForSiteID(geofence.getRequestId());
                Log.v("Location Service", "triggeringGeofences GEOFENCE_TRANSITION_ENTER: " + siteName);
                displayMessage("GEOFENCE_TRANSITION_ENTER: " + siteName);
                site_names = siteName;
                arrival++;

                // TODO: Display Geofence Enter pop-up
            }
            @Override
            public void didExitGeofence(Geofence geofence) {
                String siteName = TruckerDataManager.getInstance().getSiteNameForSiteID(geofence.getRequestId());
                String type = TruckerDataManager.getInstance().getSiteNameForSiteID(geofence.getRequestId());
                Log.v("Location Service", "triggeringGeofences GEOFENCE_TRANSITION_EXIT: " + siteName);
                displayMessage("GEOFENCE_TRANSITION_EXIT: " + siteName);
                site_names = siteName;
                departure++;
                // TODO: Display Geofence Exit pop-up
            }
        });
        LocationService.getInstance().setOnUnscheduledStopDetectedListener(new LocationService.OnUnscheduledStopDetectedListener() {
            @Override
            public void onUnscheduledStopDetectedListener(Location location) {
                Log.v("Location Service", "unscheduledStopDetected : " + location);
//                displayMessage("UNSCHEDULED_STOP_DETECTION: " + location);
                if (location != null){
                    lat = location.getLatitude();
                    longt = location.getLongitude();
                    List<Address> addresses;
                    Geocoder geocoder = new Geocoder(WelcomeScreen.this, Locale.getDefault());
                    try {
                        addresses = geocoder.getFromLocation(lat, longt, 1);
                        location_names = addresses.get(0).getAddressLine(0);
                        // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
                // TODO: Display Unscheduled Stop Detection pop-up
                show_popup++;
            }
        });
    }

    private void displayMessage(final String message) {
        Handler handler = new Handler(Looper.getMainLooper());
        handler.post(new Runnable() {
            @Override
            public void run() {
                Toast.makeText(getApplicationContext(),
                        message,
                        Toast.LENGTH_LONG).show();
            }
        });
    }

    @Override
    public void onBackPressed() {

        alert();
    }

    public void alert(){

        androidx.appcompat.app.AlertDialog alertDialog = new androidx.appcompat.app.AlertDialog.Builder(this)
                //set icon
                .setIcon(R.drawable.logo)
                //set title
                .setTitle("Warning!")
                //set message
                .setMessage("Do you want to exit?")
                //set positive button
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        Intent intent = new Intent(WelcomeScreen.this, Login.class);
                        startActivity(intent);
                        countDownTimer.cancel();
                        finish();

                    }
                })
                //set negative button
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                    }
                })
                .show();
    }

    public void loadData() {
        aPimmDataManager dataManager = aPimmDataManager.getInstance();
        final TruckerDataManager truckerDataManager = TruckerDataManager.getInstance();
        ArrayList<AssignmentInfo> assignmentInfoList = truckerDataManager.assignmentInfo();


        final ArrayList<SdrGisSite> Sites = truckerDataManager.getSdrReport().Hierarchy.GIS.Sites;
        SdrGisSite site;
        final ArrayList<SdrStop> Stops = truckerDataManager.getSdrReport().Hierarchy.GIS.Stops;
        ArrayList<SdrPointOfInterest> Poi = truckerDataManager.getSdrReport().Hierarchy.GIS.PointsOfInterest;

        SdrPointOfInterest poi = null;
        SdrStop stop = null;

        Date dcArrivalTime = TruckerDataManager.getInstance().getSdrReport().Hierarchy.ArrivalTime;
        Date dcReturnTime = TruckerDataManager.getInstance().getSdrReport().Hierarchy.DepartureTime;
        
        System.out.println("STOP SIZE : " + Stops.size());
        System.out.println("POI SIZE : " + Poi.size());
        System.out.println("SITE SIZE : " + Sites.size());
        System.out.println("TOTAL  SIZE : " + Sites.size() + Stops.size() + Poi.size());

        // Transfer data from  new Array
   

        int count = 0;
        int count_for_poi = 0;
        int count_for_stops = 1;
        int counter_s = 0;
        int counter_p = 0;
        Date date1 = null;
        
        for (int i = 0; i < Stops.size() + Sites.size() + Poi.size() + 1; i++) {

            System.out.println("SITE SIZE : " + Sites.size());
            System.out.println("POI SIZE : " + Poi.size());
            System.out.println("STOP SIZE : " + Stops.size());

            if (count == 0) {
                site = Sites.get(0);
                site_names = site.SiteName;
                site_address = site.Address;
                mDataSet.add(new Routes(site_names, 0, site_address, "Site_departure", dcArrivalTime, dcReturnTime,null
                ,site.LatLon.Lat,site.LatLon.Lon,0,site.GeoFenceRadius,0,site.SiteId));
                count++;
                System.out.println(count);
                System.out.println(site_names);
                System.out.println(site_address);

            } else if (count < Poi.size() + Sites.size()) {
                poi = Poi.get(counter_p);
                site_names = poi.Label;
                site_address = poi.Address;
                mDataSet.add(new Routes(site_names, counter_p, site_address, "Poi", poi.ArrivalTime, poi.DepartureTime,null,
                        poi.LatLon.Lat,poi.LatLon.Lon,0,50.0f,poi.Type.getValue(),poi.SdrPointOfInterestId));
                counter_p++;
                count++;
                System.out.println(count);
                System.out.println(site_names);
                System.out.println(site_address);

            } else if (count < Sites.size() + Poi.size() + Stops.size()) {
                stop = Stops.get(counter_s);
                site_names = stop.SiteName;
                site_address = stop.Address;
                mDataSet.add(new Routes(site_names, counter_s, site_address, "Stop",
                        stop.ArrivalTime, stop.DepartureTime,stop.DeliveryConfirmationTime,stop.LatLon.Lat,stop.LatLon.Lon,
                        stop.DeliveryNumber,50.0f,0,stop.SiteId));
                counter_s++;
                count++;
                count_for_stops++;


            } else if (count >= Sites.size() + Poi.size() + Stops.size()) {
                site = Sites.get(0);
                site_names = site.SiteName;
                site_address = site.Address;
                mDataSet.add(new Routes(site_names, 0, site_address, "Site_arrival", dcArrivalTime, dcReturnTime,
                null,site.LatLon.Lat,site.LatLon.Lon,0,site.GeoFenceRadius,0,site.SiteId));
                count++;
                System.out.println(count);
                System.out.println(site_names);
                System.out.println(site_address);
            }
        }
        int counter_skip =0;
        for (int i = 0; i <Stops.size()+Poi.size(); i++) {
            final Routes skip = mDataSet.get(i);
            if (skip.getSite_desc().contains("Stop")){
                if (skip.getArrival_time()!=null){
                    counter_skip++;
                }
            }else if (skip.getSite_desc().contains("Poi")){
                if (skip.getArrival_time()!=null){
                    counter_skip++;
                }
            }
        }
        Collections.sort(mDataSet.subList(1,mDataSet.size()-1), new Comparator<Routes>() {
            @Override
            public int compare(Routes routes, Routes t1) {
                if (t1.getArrival_time() == null) {
                    return (routes.getArrival_time() == null) ? 0 : -1;
                }
                if (routes.getArrival_time() == null) {
                    return 1;
                }
                return routes.getArrival_time().compareTo(t1.getArrival_time());
            }
        });
        truckerDataManager.setRoutes(mDataSet);
//
    }


}