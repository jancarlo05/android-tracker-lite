package com.procuro.androidtruckerlite;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;

import com.procuro.apimmdatamanagerlib.AssignmentInfo;
import com.procuro.apimmdatamanagerlib.SdrGisSite;
import com.procuro.apimmdatamanagerlib.SdrPointOfInterest;
import com.procuro.apimmdatamanagerlib.SdrStop;
import com.procuro.apimmdatamanagerlib.User;
import com.procuro.apimmdatamanagerlib.aPimmDataManager;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Locale;
import java.util.TimeZone;

public class Stops_info_store_info extends Fragment {


    View rootView;
    aPimmDataManager dataManager = aPimmDataManager.getInstance();
    final TruckerDataManager truckerDataManager = TruckerDataManager.getInstance();

    public ArrayList<SdrGisSite> Sites = truckerDataManager.getSdrReport().Hierarchy.GIS.Sites;
    public SdrGisSite site;
    public ArrayList<SdrStop> Stops = truckerDataManager.getSdrReport().Hierarchy.GIS.Stops;
    public  SdrStop stop = null;
    public ArrayList<SdrPointOfInterest> Poi = truckerDataManager.getSdrReport().Hierarchy.GIS.PointsOfInterest;
    public SdrPointOfInterest poi = null;
    public int value;
    ArrayList<AssignmentInfo> assignmentInfos = truckerDataManager.assignmentInfo();


    TextView trailer_name,tractor_name,driver_name,dispatch;
    RadioButton store_info,instructions;
    TextView dc_name, dc_address,manager_contact,dc_phone,dc_email;
    TextView route_name,stops_sched_arrival,stops_actual_arrival,stops_delivery_start,
    stops_actual_departure,stops_ontime_performance,stops_duration;

    public Stops_info_store_info() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.stops_store_info, container, false);
        dc_name = rootView.findViewById(R.id.dc_name);
        dc_address = rootView.findViewById(R.id.dc_address);
        manager_contact =  rootView.findViewById(R.id.manager_contact);
        dc_phone = rootView.findViewById(R.id.phone);
        dc_email = rootView.findViewById(R.id.email);


        route_name = rootView.findViewById(R.id.stops_route);
        stops_actual_arrival = rootView.findViewById(R.id.stops_actual_arrival);
        stops_sched_arrival = rootView.findViewById(R.id.stops_schedule_arrival);
        stops_delivery_start = rootView.findViewById(R.id.stops_delivery_start);
        stops_actual_departure = rootView.findViewById(R.id.stops_actual_departure);
        stops_ontime_performance = rootView.findViewById(R.id.stops_ontime_performance);
        stops_duration = rootView.findViewById(R.id.stops_duration);


        MainActivity.hide();
        MainActivity.back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                User user = TruckerDataManager.getInstance().getUser();
                MainActivity.fullname.setText(user.getFullName());
                AppCompatActivity activity;
                activity = (AppCompatActivity) view.getContext();
                activity.getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container,
                        new DeliveryFragment()).commit();
                MainActivity.show();

            }
        });








        Bundle bundle = this.getArguments();
        if (bundle != null) {
            value = bundle.getInt("position");
            System.out.println(value);
            assign();
            stop_route();
        }
        else{
            System.out.println("Wlang laman");
            System.out.println(value);
        }

        return rootView;
    }

    public void assign(){
        site = Sites.get(0);
        dc_name.setText(site.SiteName);
        dc_address.setText(site.Address);
    }

    public void stop_route(){

        stop = Stops.get(value);
        SimpleDateFormat inputFormat = new SimpleDateFormat(
                "EEE MMM dd HH:mm:ss 'GMT' yyyy", Locale.US);
        inputFormat.setTimeZone(TimeZone.getTimeZone("Etc/UTC"));
        SimpleDateFormat outputFormat = new SimpleDateFormat("MMM dd, yyyy h:mm a");

        AssignmentInfo assignmentInfo =  assignmentInfos.get(0);
        route_name.setText(assignmentInfo.routeName);
        stops_sched_arrival.setText(outputFormat.format(assignmentInfo.dispatch));

        if (stop.ArrivalTime != null) {
            stops_actual_arrival.setText(outputFormat.format(stop.ArrivalTime));
        }else{
            stops_actual_arrival.setText("---");
        }
        if (stop.DeliveryStartTime != null){
            stops_delivery_start.setText(outputFormat.format(stop.DeliveryStartTime));
        }else {
            stops_delivery_start.setText("---");
        }
        if (stop.DepartureTime != null){
            stops_delivery_start.setText(outputFormat.format(stop.DepartureTime));
        }else {
            stops_delivery_start.setText("---");

        }
        stops_duration.setText(String.valueOf(stop.Duration));
     }

    }