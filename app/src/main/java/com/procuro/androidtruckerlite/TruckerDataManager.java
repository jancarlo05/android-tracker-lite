package com.procuro.androidtruckerlite;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.location.Geofence;
import com.google.android.gms.maps.model.Circle;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.procuro.androidlocationservice.LocationService;
import com.procuro.apimmdatamanagerlib.AddressComponents;
import com.procuro.apimmdatamanagerlib.AssignmentInfo;
import com.procuro.apimmdatamanagerlib.DeliveryPOITypeEnum;
import com.procuro.apimmdatamanagerlib.DeliveryReportActionResult;
import com.procuro.apimmdatamanagerlib.DeliveryStopActionResult;
import com.procuro.apimmdatamanagerlib.GPSEventTypeEnum;
import com.procuro.apimmdatamanagerlib.LatLon;
import com.procuro.apimmdatamanagerlib.OnCompleteListeners;
import com.procuro.apimmdatamanagerlib.SdrGisSite;
import com.procuro.apimmdatamanagerlib.SdrPointOfInterest;
import com.procuro.apimmdatamanagerlib.SdrReport;
import com.procuro.apimmdatamanagerlib.SdrStop;
import com.procuro.apimmdatamanagerlib.StoreProviderSiteSettings;
import com.procuro.apimmdatamanagerlib.User;
import com.procuro.apimmdatamanagerlib.VehicleDataPackage;
import com.procuro.apimmdatamanagerlib.aPimmDataManager;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;

public class TruckerDataManager {
    private static final TruckerDataManager instance = new TruckerDataManager();

    private Activity activity;

    private aPimmDataManager dataManager;

    private String SPID;
    private String username;
    private String password;
    private User user;
    private String firstname ;
    private ArrayList<AssignmentInfo> assignmentInfoList;
    private SdrReport sdrReport;

    private ArrayList<StopListItem> stopListItems;

    ArrayList<Circle> allCircles;

    public ArrayList<Circle> getAllCircles() {
        return allCircles;
    }

    public void setAllCircles(ArrayList<Circle> allCircles) {
        this.allCircles = allCircles;
    }

    private ArrayList<Routes> routes;

    public ArrayList<Routes> getRoutes() {
        return routes;
    }

    public void setRoutes(ArrayList<Routes> routes) {
        this.routes = routes;
    }

    public static TruckerDataManager getInstance() {
        instance.initDataManager();
        return instance;
    }

    private void initDataManager() {
        dataManager = aPimmDataManager.getInstance();
    }

    public void setActivity(Activity activity) {
        this.activity = activity;
    }

    public void setSPID(String SPID) {
        this.SPID = SPID;
    }

    public  String getSPID() {
        return this.SPID;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public  String getUsername() {
        return this.username;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPassword() {
        return this.password;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public User getUser() {
        return this.user;
    }

    public void setAssignmentInfoList(ArrayList<AssignmentInfo> assignmentInfoList) {
        this.assignmentInfoList = assignmentInfoList; }

    public ArrayList<AssignmentInfo> assignmentInfo()
    {
        return this.assignmentInfoList;
    }

    public void setSdrReport(SdrReport sdrReport) {
        this.sdrReport = sdrReport;
        this.generateStopList(sdrReport);
    }

    public SdrReport getSdrReport() {
        return this.sdrReport;
    }

    private void generateStopList(SdrReport sdrReport) {
        this.stopListItems = new ArrayList<>();

        ArrayList<SdrGisSite> sites = sdrReport.Hierarchy.GIS.Sites;
        Date dcArrivalTime = sdrReport.Hierarchy.ArrivalTime;
        Date dcReturnTime = sdrReport.Hierarchy.DepartureTime;

        SdrGisSite dc = null;

        if(sites.size() > 0) {
            dc = sites.get(0);
        }

        StopListItem dcDepartureStopListItem = new StopListItem(
                dc.SiteId,
                dc.SiteName,
                dc.Address,
                dcArrivalTime,
                dcReturnTime,
                DeliveryPOITypeEnum.DeliveryPOITypesEnum.DeliveryPOIType_DCFacility,
                dc.LatLon,
                100.0f,
                dc.AddressComponents,
                false
                ,null);
        this.stopListItems.add(dcDepartureStopListItem);

        for (SdrStop sdrStop : sdrReport.Hierarchy.GIS.Stops) {
            StopListItem stopListItem = new StopListItem(sdrStop.SiteId,
                    sdrStop.SiteName,
                    sdrStop.Address,
                    sdrStop.ArrivalTime,
                    sdrStop.DepartureTime,
                    DeliveryPOITypeEnum.DeliveryPOITypesEnum.DeliveryPOIType_DeliveryStop,
                    sdrStop.LatLon,
                    50.0f,
                    sdrStop.AddressComponents,
                    false,sdrStop.ExpectedDeliveryTime);
            Log.v("TRUCKERDATAMANAGER", "SdrStop SiteName: " + sdrStop.SiteName
                    + " SiteID: " + sdrStop.SiteId
                    + " Lat: " + sdrStop.LatLon.Lat
                    + " Lon: " + sdrStop.LatLon.Lon);
            this.stopListItems.add(stopListItem);
        }

        for (SdrPointOfInterest sdrPointOfInterest : sdrReport.Hierarchy.GIS.PointsOfInterest) {
            Log.v("SdrPointOfInterest", "Lat: " + sdrPointOfInterest.LatLon.Lat + " Lon: " + sdrPointOfInterest.LatLon.Lon);
            StopListItem stopListItem = new StopListItem(sdrPointOfInterest.SdrPointOfInterestId,
                    sdrPointOfInterest.Label,
                    sdrPointOfInterest.Address,
                    sdrPointOfInterest.ArrivalTime,
                    sdrPointOfInterest.DepartureTime,
                    sdrPointOfInterest.Type,
                    sdrPointOfInterest.LatLon,
                    50.0f,
                    sdrPointOfInterest.AddressComponents,
                    false,null);
            this.stopListItems.add(stopListItem);
        }

        StopListItem dcArrivalStopListItem = new StopListItem(dc.SiteId,
                dc.SiteName,
                dc.Address,
                dcArrivalTime,
                dcReturnTime,
                DeliveryPOITypeEnum.DeliveryPOITypesEnum.DeliveryPOIType_DCFacility,
                dc.LatLon,
                100.0f,
                dc.AddressComponents,
                false,null);
        this.stopListItems.add(dcArrivalStopListItem);
        this.initLocationServiceWithStopList(this.stopListItems);

    }

    public ArrayList<StopListItem> getStopListItems() {
        return this.stopListItems;
    }

    public ArrayList<HashMap<String, String>> getStopListHashMap() {
        ArrayList<HashMap<String, String>> stopListHashMap = new ArrayList<>();
        ArrayList<StopListItem> stopListItems = TruckerDataManager.getInstance().getStopListItems();
        for(StopListItem stopListItem : stopListItems) {
            String siteName = stopListItem.siteName;
            String address = stopListItem.address;
            String iconName = TruckerDataManager.getInstance().getIconNameForStopListItem(stopListItem);
            HashMap<String, String> hashMap = new HashMap<String, String>();
            hashMap.put("listview_title", "Stop " + siteName);
            hashMap.put("listview_discription", address);
            hashMap.put("listview_image", iconName);
            stopListHashMap.add(hashMap);
        }
        return stopListHashMap;
    }

    public String getIconNameForStopListItem(StopListItem stopListItem) {
        if(stopListItem.type == DeliveryPOITypeEnum.DeliveryPOITypesEnum.DeliveryPOIType_DCFacility) {
            if(stopListItem.arrivalTime != null || stopListItem.departureTime != null) {
                return Integer.toString(R.drawable.dc_blue);
            }
            return Integer.toString(R.drawable.dc_black);
        } else if(stopListItem.type == DeliveryPOITypeEnum.DeliveryPOITypesEnum.DeliveryPOIType_DeliveryStop) {
            if(stopListItem.departureTime != null) {
                return Integer.toString(R.drawable.store_delivery_ontime_blue_green_check);
            } else if(stopListItem.arrivalTime != null) {
                return Integer.toString(R.drawable.store_delivery_ontime_blue);
            } else {
                return Integer.toString(R.drawable.store_delivert_ontime_gray);
            }
        } else {
            if (stopListItem.type == DeliveryPOITypeEnum.DeliveryPOITypesEnum.DeliveryPOIType_StartPoint) {
                return Integer.toString(R.drawable.poi_start_point);
            } else if (stopListItem.type == DeliveryPOITypeEnum.DeliveryPOITypesEnum.DeliveryPOIType_EndPoint) {
                return Integer.toString(R.drawable.poi_end_point);
            } else if (stopListItem.type == DeliveryPOITypeEnum.DeliveryPOITypesEnum.DeliveryPOIType_FuelStop) {
                return Integer.toString(R.drawable.poi_fuel);
            } else if (stopListItem.type == DeliveryPOITypeEnum.DeliveryPOITypesEnum.DeliveryPOIType_PickUpStop) {
                return Integer.toString(R.drawable.poi_plant_pick_up);
            } else if (stopListItem.type == DeliveryPOITypeEnum.DeliveryPOITypesEnum.DeliveryPOIType_RestStop) {
                return Integer.toString(R.drawable.poi_rest);
            } else if (stopListItem.type == DeliveryPOITypeEnum.DeliveryPOITypesEnum.DeliveryPOIType_Inspection) {
                return Integer.toString(R.drawable.poi_inspection);
            } else if (stopListItem.type == DeliveryPOITypeEnum.DeliveryPOITypesEnum.DeliveryPOIType_Service) {
                return Integer.toString(R.drawable.poi_service);
            } else if (stopListItem.type == DeliveryPOITypeEnum.DeliveryPOITypesEnum.DeliveryPOIType_Layover) {
                return Integer.toString(R.drawable.poi_layover);
            } else if (stopListItem.type == DeliveryPOITypeEnum.DeliveryPOITypesEnum.DeliveryPOIType_Traffic) {
                return Integer.toString(R.drawable.poi_traffic);
            } else if (stopListItem.type == DeliveryPOITypeEnum.DeliveryPOITypesEnum.DeliveryPOIType_TruckWash) {
                return Integer.toString(R.drawable.poi_wash);
            } else {
                return Integer.toString(R.drawable.poi_undefined);
            }
        }
    }
    private void initLocationServiceWithStopList(ArrayList<StopListItem> stopListItems) {
        LocationService locationService = LocationService.getInstance();
        ArrayList<Geofence> geofences = new ArrayList<>();
        for(StopListItem stopListItem : stopListItems) {
            Geofence geofence = locationService.createGeofence(stopListItem.siteID, stopListItem.latlon.Lat, stopListItem.latlon.Lon, stopListItem.geofenceRadius);
            geofences.add(geofence);
        }
        LocationService.getInstance().setGeofenceList(geofences);
    }

    public void addgeofence(ArrayList<Routes> routes) {
        LocationService locationService = LocationService.getInstance();
        ArrayList<Geofence> geofences = new ArrayList<>();
        for(Routes item : routes) {
            Geofence geofence = locationService.createGeofence(item.getSite_id(), item.getLat(), item.getLongt(), item.getGeofenceRadius());
            geofences.add(geofence);
        }
        LocationService.getInstance().setGeofenceList(geofences);
    }


    public String getSiteNameForSiteID(String siteID) {
        for(StopListItem stopListItem : stopListItems) {
            if(stopListItem.siteID.equalsIgnoreCase(siteID)) {
                return stopListItem.siteID;
            }
        }
        return "";
    }


    public String getrouteSiteNameForSiteID(String siteID) {
        for(Routes item : routes) {
            if(item.getSite_id().equalsIgnoreCase(siteID)) {
                return item.getRoute_name();
            }
        }
        return "";
    }

    public StopListItem getStopListItemForSiteID(String siteID) {
        for(StopListItem stopListItem : stopListItems) {
            if(stopListItem.siteID.equalsIgnoreCase(siteID)) {
                return stopListItem;
            }
        }
        return null;
    }

    public String getVendorID() {
        String vendorID;

        SharedPreferences preferences = this.activity.getSharedPreferences("Trucker", Context.MODE_PRIVATE);
        vendorID = preferences.getString("vendorID", null);

        if(vendorID == null) {
            vendorID = UUID.randomUUID().toString();
            SharedPreferences.Editor editor = preferences.edit();
            editor.putString("vendorID", vendorID);
            editor.apply();
        }

        if(BuildConfig.DEBUG) {
            vendorID = "ED2890ED-6615-444B-838C-75B038D1A84E";
        }

        return vendorID;
    }

    public void postGPSToServer(double latitude, double longitude) {
        VehicleDataPackage vehicleDataPackage = new VehicleDataPackage();
        vehicleDataPackage.lat = latitude;
        vehicleDataPackage.lon = longitude;
        vehicleDataPackage.timestamp = new Date();
        dataManager.postGPSDataForDeviceSerialNumber(getVendorID(), "Android", vehicleDataPackage, new OnCompleteListeners.postGPSDataForDeviceSerialNumberListener() {
            @Override
            public void postGPSDataForDeviceSerialNumber(Error error) {

            }
        });
    }

    public void triggerArrivalDetectionForDCSiteWithID(String siteID, Date date, double latitude, double longitude) {
        dataManager.pushGPSEventForSiteWithDeliveryId(this.sdrReport.Hierarchy.DeliveryId,
                siteID,
                getVendorID(),
                "Android",
                date,
                latitude,
                longitude,
                GPSEventTypeEnum.GPSEventTypesEnum.GPSEventType_EnterGeofence,
                new OnCompleteListeners.OnPushGPSEventForSiteWithDeliveryIdListener() {
                    @Override
                    public void pushGPSEventForSiteWithDeliveryId(DeliveryReportActionResult result, Error error) {

                    }
                });
    }

    public void triggerDepartureDetectionForDCSiteWithID(String siteID, Date date, double latitude, double longitude) {
        dataManager.pushGPSEventForSiteWithDeliveryId(this.sdrReport.Hierarchy.DeliveryId,
                siteID,
                getVendorID(),
                "Android",
                date,
                latitude,
                longitude,
                GPSEventTypeEnum.GPSEventTypesEnum.GPSEventType_ExitGeofence,
                new OnCompleteListeners.OnPushGPSEventForSiteWithDeliveryIdListener() {
                    @Override
                    public void pushGPSEventForSiteWithDeliveryId(DeliveryReportActionResult result, Error error) {

                    }
                });
    }

    public void triggerArrivalDetectionForStoreWithID(String deliveryStopID, Date date, double latitude, double longitude) {
        dataManager.pushGPSEventForStopWithDeliveryId(this.sdrReport.Hierarchy.DeliveryId,
                deliveryStopID,
                getVendorID(),
                "Android",
                date,
                latitude,
                longitude,
                GPSEventTypeEnum.GPSEventTypesEnum.GPSEventType_EnterGeofence,
                new OnCompleteListeners.OnPushGPSEventForStopWithDeliveryIdListener() {
            @Override
            public void pushGPSEventForStopWithDeliveryId(DeliveryStopActionResult result, Error error) {

            }
        });
    }
    public void triggerDepartureDetectionForStoreWithID(String deliveryStopID, Date date, double latitude, double longitude) {
        dataManager.pushGPSEventForStopWithDeliveryId(this.sdrReport.Hierarchy.DeliveryId,
                deliveryStopID,
                getVendorID(),
                "Android",
                date,
                latitude,
                longitude,
                GPSEventTypeEnum.GPSEventTypesEnum.GPSEventType_ExitGeofence,
                new OnCompleteListeners.OnPushGPSEventForStopWithDeliveryIdListener() {
                    @Override
                    public void pushGPSEventForStopWithDeliveryId(DeliveryStopActionResult result, Error error) {


                    }
                });
    }

    public void triggerDepartureDetectionForPOIWithID(String siteID, Date date, double latitude, double longitude) {
        dataManager.pushGPSEventForPOIWithDeliveryId(this.sdrReport.Hierarchy.DeliveryId,
                siteID,
                getVendorID(),
                "Android",
                date,
                latitude,
                longitude,
                GPSEventTypeEnum.GPSEventTypesEnum.GPSEventType_ExitGeofence,
                new OnCompleteListeners.OnPushGPSEventForPOIWithDeliveryIdListener() {
                    @Override
                    public void pushGPSEventForPOIWithDeliveryId(DeliveryStopActionResult result, Error error) {
                    }
                });
    }
}
