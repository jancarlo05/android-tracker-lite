package com.procuro.androidtruckerlite;

import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.SimpleAdapter;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;

import com.baoyz.swipemenulistview.SwipeMenu;
import com.baoyz.swipemenulistview.SwipeMenuCreator;
import com.baoyz.swipemenulistview.SwipeMenuItem;
import com.baoyz.swipemenulistview.SwipeMenuListView;
import com.procuro.apimmdatamanagerlib.AssignmentInfo;
import com.procuro.apimmdatamanagerlib.OnCompleteListeners;
import com.procuro.apimmdatamanagerlib.SdrGisSite;
import com.procuro.apimmdatamanagerlib.SdrPointOfInterest;
import com.procuro.apimmdatamanagerlib.SdrReport;
import com.procuro.apimmdatamanagerlib.SdrStop;
import com.procuro.apimmdatamanagerlib.User;
import com.procuro.apimmdatamanagerlib.aPimmDataManager;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;

import static androidx.constraintlayout.widget.Constraints.TAG;

public class Segmented_Pending_route_info extends Fragment {


    View rootView;
    aPimmDataManager dataManager = aPimmDataManager.getInstance();
    final TruckerDataManager truckerDataManager = TruckerDataManager.getInstance();

    public ArrayList<SdrGisSite> Sites = truckerDataManager.getSdrReport().Hierarchy.GIS.Sites;
    public SdrGisSite site;
    public ArrayList<SdrStop> Stops = truckerDataManager.getSdrReport().Hierarchy.GIS.Stops;
    public  SdrStop stop = null;
    public ArrayList<SdrPointOfInterest> Poi = truckerDataManager.getSdrReport().Hierarchy.GIS.PointsOfInterest;
    public SdrPointOfInterest poi = null;
    public int value;

    TextView trailer_name,tractor_name,driver_name,dispatch;

    public Segmented_Pending_route_info() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.segmented_pending_route_info, container, false);

        trailer_name = rootView.findViewById(R.id.trailer_name);
        tractor_name = rootView.findViewById(R.id.tractor_name);
        driver_name = rootView.findViewById(R.id.driver_name);
        dispatch = rootView.findViewById(R.id.dispatch);


        Bundle bundle = this.getArguments();
        if (bundle != null) {
            value = bundle.getInt("position");
            System.out.println(value);
            active();
        }
        else{
            System.out.println("Wlang laman");
            System.out.println(value);

        }


        MainActivity.hide();
        MainActivity.back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                User user = TruckerDataManager.getInstance().getUser();
                MainActivity.fullname.setText(user.getFullName());
                DeliveryFragment.pending.setChecked(true);
                DeliveryFragment.pending.isChecked();
                AppCompatActivity activity;
                activity = (AppCompatActivity) view.getContext();
                activity.getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container,
                        new DeliveryFragment()).commit();


                ArrayList<AssignmentInfo> assignmentInfoList = TruckerDataManager.getInstance().assignmentInfo();
                if (assignmentInfoList.size() > 1) {
                    getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container_app_bar,
                            new Segmented_Pending_Fragment()).commit();

                } else {
                    getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container_app_bar,
                            new Segmented_Completed_Fragment()).commit();
                }


            }
        });


        return rootView;
    }

    public void active() {
        final TruckerDataManager truckerDataManager = TruckerDataManager.getInstance();
        ArrayList<AssignmentInfo> assignmentInfoList = truckerDataManager.assignmentInfo();
        User user = truckerDataManager.getUser();
        final AssignmentInfo assignmentInfo = assignmentInfoList.get(value);

        SimpleDateFormat inputFormat = new SimpleDateFormat(
                "EEE MMM dd HH:mm:ss 'GMT' yyyy", Locale.US);
        inputFormat.setTimeZone(TimeZone.getTimeZone("Etc/UTC"));
        SimpleDateFormat outputFormat = new SimpleDateFormat("MMM dd, yyyy h:mm a");

        trailer_name.setText(String.valueOf(assignmentInfo.trailerName));
        tractor_name.setText(String.valueOf(assignmentInfo.tractorName));
        driver_name.setText(String.valueOf(user.getFullName()));
        dispatch.setText(outputFormat.format(assignmentInfo.dispatch));
        truckerDataManager.getSdrReport();

        List<HashMap<String, String>> aList = new ArrayList<HashMap<String, String>>();
        for (int i = 0; i < Stops.size(); i++) {
            stop = Stops.get(i);
            String site_name = stop.SiteName;
            String site_address = stop.Address;
            HashMap<String, String> hm = new HashMap<String, String>();
            hm.put("listview_title", "Stop " + site_name);
            hm.put("listview_discription", site_address);
            hm.put("listview_image", Integer.toString(R.drawable.store_delivert_ontime_gray));

            aList.add(hm);
        }
        String[] from = {"listview_image", "listview_title", "listview_discription", "time"};
        int[] to = {R.id.listview_image, R.id.listview_item_title, R.id.listview_item_short_description, R.id.time};
        SimpleAdapter simpleAdapter = new SimpleAdapter(rootView.getContext().getApplicationContext(), aList, R.layout.segmented_pending_route_info_data, from, to);
        SwipeMenuListView androidListView = (SwipeMenuListView) rootView.findViewById(R.id.listView);
        androidListView.setAdapter(simpleAdapter);

    }
    }

