package com.procuro.androidtruckerlite;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.MutableContextWrapper;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Environment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;

import com.github.gcacace.signaturepad.views.SignaturePad;
import com.procuro.apimmdatamanagerlib.AssignmentInfo;
import com.procuro.apimmdatamanagerlib.SdrGisSite;
import com.procuro.apimmdatamanagerlib.SdrPointOfInterest;
import com.procuro.apimmdatamanagerlib.SdrStop;
import com.procuro.apimmdatamanagerlib.User;
import com.procuro.apimmdatamanagerlib.aPimmDataManager;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.util.ArrayList;

public class Dc_info extends Fragment {

    public Dc_info() {
        // Required empty public constructor
    }

    aPimmDataManager dataManager = aPimmDataManager.getInstance();
    final TruckerDataManager truckerDataManager = TruckerDataManager.getInstance();

    public ArrayList<SdrGisSite> Sites = truckerDataManager.getSdrReport().Hierarchy.GIS.Sites;
    public SdrGisSite site;
    public ArrayList<SdrStop> Stops = truckerDataManager.getSdrReport().Hierarchy.GIS.Stops;
    public  SdrStop stop = null;
    public ArrayList<SdrPointOfInterest> Poi = truckerDataManager.getSdrReport().Hierarchy.GIS.PointsOfInterest;
    public SdrPointOfInterest poi = null;

    static final int REQUEST_EXTERNAL_STORAGE = 1;
    static String[] PERMISSIONS_STORAGE = {Manifest.permission.WRITE_EXTERNAL_STORAGE};

    TextView dc_name,dc_address,trailer_name,tractor_name,driver_name;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
       View rootView = inflater.inflate(R.layout.dc_info, container, false);

       dc_name = (TextView) rootView.findViewById(R.id.dc_sitename);
        dc_address = (TextView) rootView.findViewById(R.id.dc_address);
        tractor_name = (TextView) rootView.findViewById(R.id.assign_tractorname);
        trailer_name = (TextView) rootView.findViewById(R.id.assign_trailername);
        driver_name = (TextView) rootView.findViewById(R.id.assign_driver);



        MainActivity.hide();
        MainActivity.back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                User user = TruckerDataManager.getInstance().getUser();
                MainActivity.fullname.setText(user.getFullName());
                AppCompatActivity activity;
                activity = (AppCompatActivity) view.getContext();
                activity.getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container,
                        new DeliveryFragment()).commit();
                MainActivity.show();

            }
        });


        site = Sites.get(0);
        User user = truckerDataManager.getUser();
        ArrayList<AssignmentInfo> assignmentInfo = truckerDataManager.assignmentInfo();
        AssignmentInfo assign = assignmentInfo.get(0);
       dc_name.setText(site.SiteName);
        dc_address.setText(site.Address);
        driver_name.setText(user.getFullName());
       tractor_name.setText(assign.tractorName);
       tractor_name.setText(assign.trailerName);


        return rootView;
    }



}

