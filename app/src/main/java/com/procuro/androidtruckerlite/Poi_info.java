package com.procuro.androidtruckerlite;

import android.content.Context;
import android.graphics.Rect;
import android.os.Bundle;
import android.os.Handler;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;

import com.procuro.apimmdatamanagerlib.AssignmentInfo;
import com.procuro.apimmdatamanagerlib.SdrGisSite;
import com.procuro.apimmdatamanagerlib.SdrPointOfInterest;
import com.procuro.apimmdatamanagerlib.SdrReport;
import com.procuro.apimmdatamanagerlib.SdrStop;
import com.procuro.apimmdatamanagerlib.User;
import com.procuro.apimmdatamanagerlib.aPimmDataManager;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

public class Poi_info extends Fragment {

    public Poi_info() {
        // Required empty public constructor
    }

    aPimmDataManager dataManager = aPimmDataManager.getInstance();
    final TruckerDataManager truckerDataManager = TruckerDataManager.getInstance();

    public ArrayList<SdrGisSite> Sites = truckerDataManager.getSdrReport().Hierarchy.GIS.Sites;
    public SdrGisSite site;
    public ArrayList<SdrStop> Stops = truckerDataManager.getSdrReport().Hierarchy.GIS.Stops;
    public  SdrStop stop = null;
    public ArrayList<SdrPointOfInterest> Poi = truckerDataManager.getSdrReport().Hierarchy.GIS.PointsOfInterest;
    public SdrPointOfInterest poi = null;

   public static TextView poi_desc,poi_arrival_time,poi_departure_time,poi_address;
   public static EditText poi_label;
   ImageView fuel_check,coffee_check,delivery_check,plant_check,
           service_check,inspection_check,wash_check,rest_check,layover_check,traffic_check,poi_container;

   ImageButton fuel,cofeee,delivery,plant,service,inspection,wash,rest,layover,traffic;
   ArrayList<Routes>routes = TruckerDataManager.getInstance().getRoutes();
   Routes item = null;
   int poi_type;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
       final View rootView = inflater.inflate(R.layout.modify_poi_2, container, false);

        poi_desc = rootView.findViewById(R.id.poi_desc);
        poi_arrival_time = rootView.findViewById(R.id.poi_arrival_time);
        poi_departure_time = rootView.findViewById(R.id.poi_departure_time);
        poi_address = rootView.findViewById(R.id.poi_address);
        poi_label = rootView.findViewById(R.id.poi_label);
        poi_container = rootView.findViewById(R.id.poi_container);

        fuel = rootView.findViewById(R.id.fuel);
        fuel_check = rootView.findViewById(R.id.fuel_check);
        cofeee = rootView.findViewById(R.id.coffee);
        coffee_check = rootView.findViewById(R.id.coffee_check);
        delivery = rootView.findViewById(R.id.delivery);
        delivery_check = rootView.findViewById(R.id.delivery_check);
        plant = rootView.findViewById(R.id.plant);
        plant_check = rootView.findViewById(R.id.plant_check);
        service = rootView.findViewById(R.id.service);
        service_check = rootView.findViewById(R.id.service_check);
        inspection = rootView.findViewById(R.id.inspection);
        inspection_check = rootView.findViewById(R.id.inspection_check);
        wash = rootView.findViewById(R.id.wash);
        wash_check = rootView.findViewById(R.id.wash_check);
        rest = rootView.findViewById(R.id.rest);
        rest_check = rootView.findViewById(R.id.rest_check);
        layover = rootView.findViewById(R.id.layover);
        layover_check = rootView.findViewById(R.id.layover_check);
        traffic = rootView.findViewById(R.id.traffic);
        traffic_check = rootView.findViewById(R.id.traffic_chec);


        MainActivity.hide();
        MainActivity.back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                User user = TruckerDataManager.getInstance().getUser();
                MainActivity.fullname.setText(user.getFullName());
                AppCompatActivity activity;
                activity = (AppCompatActivity) view.getContext();
                activity.getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container,
                        new DeliveryFragment()).commit();
                MainActivity.poi_save.setVisibility(View.GONE);
                InputMethodManager imm = (InputMethodManager)getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                MainActivity.show_btm_nav();
                MainActivity.show();

            }
        });

        poi_label.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
                MainActivity.hide_btm_nav();
                return false;
            }
        });

        poi_label.setOnKeyListener(new View.OnKeyListener() {
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (KeyEvent.KEYCODE_BACK == event.getKeyCode())
                {
                    MainActivity.show_btm_nav();
                    return true;
                }
                if (KeyEvent.ACTION_DOWN == event.getKeyCode())
                {
                    MainActivity.show_btm_nav();
                    return true;
                }

                return false;
            }
        });

        MainActivity.poi_save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                view= getActivity().getCurrentFocus();
                if (view != null) {
                    InputMethodManager imm = (InputMethodManager)getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                    MainActivity.show_btm_nav();
                }
                item.setRoute_name(poi_label.getText().toString());
                item.setPoi_type(poi_type);


            }
        });

        Bundle bundle = this.getArguments();
        if (bundle != null) {
            int i = bundle.getInt("position");
            item = routes.get(i);
        }


        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
        touch();
        details();


        return rootView;


    }
    public void touch(){

        fuel.setClickable(true);
        fuel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                fuel_check.setVisibility(View.VISIBLE);
                coffee_check.setVisibility(View.GONE);
                delivery_check.setVisibility(View.GONE);
                plant_check.setVisibility(View.GONE);
                service_check.setVisibility(View.GONE);
                inspection_check.setVisibility(View.GONE);
                wash_check.setVisibility(View.GONE);
                rest_check.setVisibility(View.GONE);
                layover_check.setVisibility(View.GONE);
                traffic_check.setVisibility(View.GONE);
                poi_type = 2;


            }
        });

        cofeee.setClickable(true);
        cofeee.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                fuel_check.setVisibility(View.GONE);
                coffee_check.setVisibility(View.VISIBLE);
                delivery_check.setVisibility(View.GONE);
                plant_check.setVisibility(View.GONE);
                service_check.setVisibility(View.GONE);
                inspection_check.setVisibility(View.GONE);
                wash_check.setVisibility(View.GONE);
                rest_check.setVisibility(View.GONE);
                layover_check.setVisibility(View.GONE);
                traffic_check.setVisibility(View.GONE);
                poi_type = 9;
            }
        });

        delivery.setClickable(true);
        delivery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                fuel_check.setVisibility(View.GONE);
                coffee_check.setVisibility(View.GONE);
                delivery_check.setVisibility(View.VISIBLE);
                plant_check.setVisibility(View.GONE);
                service_check.setVisibility(View.GONE);
                inspection_check.setVisibility(View.GONE);
                wash_check.setVisibility(View.GONE);
                rest_check.setVisibility(View.GONE);
                layover_check.setVisibility(View.GONE);
                traffic_check.setVisibility(View.GONE);
                poi_type = 0;
            }
        });

        plant.setClickable(true);
        plant.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                fuel_check.setVisibility(View.GONE);
                coffee_check.setVisibility(View.GONE);
                delivery_check.setVisibility(View.GONE);
                plant_check.setVisibility(View.VISIBLE);
                service_check.setVisibility(View.GONE);
                inspection_check.setVisibility(View.GONE);
                wash_check.setVisibility(View.GONE);
                rest_check.setVisibility(View.GONE);
                layover_check.setVisibility(View.GONE);
                traffic_check.setVisibility(View.GONE);
                poi_type = 3;
            }
        });

        service.setClickable(true);
        service.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                fuel_check.setVisibility(View.GONE);
                coffee_check.setVisibility(View.GONE);
                delivery_check.setVisibility(View.GONE);
                plant_check.setVisibility(View.GONE);
                service_check.setVisibility(View.VISIBLE);
                inspection_check.setVisibility(View.GONE);
                wash_check.setVisibility(View.GONE);
                rest_check.setVisibility(View.GONE);
                layover_check.setVisibility(View.GONE);
                traffic_check.setVisibility(View.GONE);
                poi_type = 7;
            }
        });

        inspection.setClickable(true);
        inspection.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                fuel_check.setVisibility(View.GONE);
                coffee_check.setVisibility(View.GONE);
                delivery_check.setVisibility(View.GONE);
                plant_check.setVisibility(View.GONE);
                service_check.setVisibility(View.GONE);
                inspection_check.setVisibility(View.VISIBLE);
                wash_check.setVisibility(View.GONE);
                rest_check.setVisibility(View.GONE);
                layover_check.setVisibility(View.GONE);
                traffic_check.setVisibility(View.GONE);
                poi_type = 6;
            }
        });
        wash.setClickable(true);
        wash.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                fuel_check.setVisibility(View.GONE);
                coffee_check.setVisibility(View.GONE);
                delivery_check.setVisibility(View.GONE);
                plant_check.setVisibility(View.GONE);
                service_check.setVisibility(View.GONE);
                inspection_check.setVisibility(View.GONE);
                wash_check.setVisibility(View.VISIBLE);
                rest_check.setVisibility(View.GONE);
                layover_check.setVisibility(View.GONE);
                traffic_check.setVisibility(View.GONE);
                poi_type = 13;
            }
        });

        rest.setClickable(true);
        rest.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                fuel_check.setVisibility(View.GONE);
                coffee_check.setVisibility(View.GONE);
                delivery_check.setVisibility(View.GONE);
                plant_check.setVisibility(View.GONE);
                service_check.setVisibility(View.GONE);
                inspection_check.setVisibility(View.GONE);
                wash_check.setVisibility(View.GONE);
                rest_check.setVisibility(View.VISIBLE);
                layover_check.setVisibility(View.GONE);
                traffic_check.setVisibility(View.GONE);
                poi_type = 5;
            }
        });

        layover.setClickable(true);
        layover.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                fuel_check.setVisibility(View.GONE);
                coffee_check.setVisibility(View.GONE);
                delivery_check.setVisibility(View.GONE);
                plant_check.setVisibility(View.GONE);
                service_check.setVisibility(View.GONE);
                inspection_check.setVisibility(View.GONE);
                wash_check.setVisibility(View.GONE);
                rest_check.setVisibility(View.GONE);
                layover_check.setVisibility(View.VISIBLE);
                traffic_check.setVisibility(View.GONE);
                poi_type = 8;
            }
        });

        traffic.setClickable(true);
        traffic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                fuel_check.setVisibility(View.GONE);
                coffee_check.setVisibility(View.GONE);
                delivery_check.setVisibility(View.GONE);
                plant_check.setVisibility(View.GONE);
                service_check.setVisibility(View.GONE);
                inspection_check.setVisibility(View.GONE);
                wash_check.setVisibility(View.GONE);
                rest_check.setVisibility(View.GONE);
                layover_check.setVisibility(View.GONE);
                traffic_check.setVisibility(View.VISIBLE);
                poi_type = 11;
            }
        });

    }


    public void details(){
        if (item.getPoi_type() == 50 ) {
           poi_container.setBackgroundResource(R.drawable.poi_start_point);
        } else if (item.getPoi_type() == 9 ){
          poi_container.setBackgroundResource(R.drawable.poi_coffee);
        } else if (item.getPoi_type() == -1 ){
          poi_container.setBackgroundResource(R.drawable.poi_undefined);
        } else if (item.getPoi_type() == 0 ) {
          poi_container.setBackgroundResource(R.drawable.poi_delivery);
        } else if (item.getPoi_type() == 1 ){
          poi_container.setBackgroundResource(R.drawable.poi_undefined);
        } else if (item.getPoi_type() == 2 ){
          poi_container.setBackgroundResource(R.drawable.poi_fuel);
        } else if (item.getPoi_type() == 3 ) {
          poi_container.setBackgroundResource(R.drawable.poi_plant_pick_up);
            //DeliveryPOIType_OutOfTerritory = 4
        } else if (item.getPoi_type() == 4 ) {
          poi_container.setBackgroundResource(R.drawable.poi_undefined);
        } else if (item.getPoi_type() == 5 ) {
          poi_container.setBackgroundResource(R.drawable.poi_rest);
        } else if (item.getPoi_type() == 6 ) {
          poi_container.setBackgroundResource(R.drawable.poi_inspection);
        } else if (item.getPoi_type() == 7 ) {
          poi_container.setBackgroundResource(R.drawable.poi_service);
        } else if (item.getPoi_type() == 8 ) {
          poi_container.setBackgroundResource(R.drawable.poi_layover);
        } else if (item.getPoi_type() == 10 ) {
          poi_container.setBackgroundResource(R.drawable.poi_undefined);
        } else if (item.getPoi_type() == 11) {
          poi_container.setBackgroundResource(R.drawable.poi_traffic);
        } else if (item.getPoi_type() == 13 ) {
          poi_container.setBackgroundResource(R.drawable.poi_wash);
        } else if (item.getPoi_type() == 20 ) {
          poi_container.setBackgroundResource(R.drawable.poi_undefined);
        } else if (item.getPoi_type() == 21 ) {
          poi_container.setBackgroundResource(R.drawable.poi_undefined);
        } else if (item.getPoi_type() == 51 ){
          poi_container.setBackgroundResource(R.drawable.poi_end_point);
        } else if (item.getPoi_type() == 60 ){
          poi_container.setBackgroundResource(R.drawable.poi_undefined);
            //DeliveryPOIType_DCFacility = 61,
        } else if (item.getPoi_type() == 61 ) {
          poi_container.setBackgroundResource(R.drawable.poi_undefined);
            //DeliveryPOIType_StoppedWhileUnassigned = 62,
        } else if (item.getPoi_type() == 62 ) {
          poi_container.setBackgroundResource(R.drawable.poi_undefined);
        } else if (item.getPoi_type() == 100 ) {
          poi_container.setBackgroundResource(R.drawable.poi_undefined);
        } else if (item.getPoi_type() == 101 ) {
          poi_container.setBackgroundResource(R.drawable.poi_undefined);
        }

        SimpleDateFormat inputFormat = new SimpleDateFormat(
                "EEE MMM dd HH:mm:ss 'GMT' yyyy", Locale.US);
        inputFormat.setTimeZone(TimeZone.getTimeZone("Etc/UTC"));
        SimpleDateFormat outputFormat = new SimpleDateFormat("MMM dd, yyyy h:mm a");
         Date arrival_time = null;
         Date departure_time = null;

        poi_desc.setText(item.getRoute_name());
        poi_arrival_time.setText(outputFormat.format(item.getArrival_time()));
        if (item.getDeparture_time() !=null){
            poi_departure_time.setText(outputFormat.format(item.getDeparture_time()));
        }else {
            poi_departure_time.setText("");
        }
        poi_label.setText(item.getRoute_name());
        poi_address.setText(item.getAddress());
        poi_type = item.getPoi_type();

    }
}

