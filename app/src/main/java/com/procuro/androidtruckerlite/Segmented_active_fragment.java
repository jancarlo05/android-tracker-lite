package com.procuro.androidtruckerlite;

import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Icon;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.BounceInterpolator;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import android.widget.Toast;

import androidx.fragment.app.Fragment;

import com.baoyz.swipemenulistview.SwipeMenu;
import com.baoyz.swipemenulistview.SwipeMenuCreator;
import com.baoyz.swipemenulistview.SwipeMenuItem;
import com.baoyz.swipemenulistview.SwipeMenuListView;
import com.procuro.apimmdatamanagerlib.AssignmentInfo;
import com.procuro.apimmdatamanagerlib.DateTimeFormat;
import com.procuro.apimmdatamanagerlib.OnCompleteListeners;
import com.procuro.apimmdatamanagerlib.POISiteListDTO;
import com.procuro.apimmdatamanagerlib.SdrGisSite;
import com.procuro.apimmdatamanagerlib.SdrMetric;
import com.procuro.apimmdatamanagerlib.SdrPointOfInterest;
import com.procuro.apimmdatamanagerlib.SdrReport;
import com.procuro.apimmdatamanagerlib.SdrStop;
import com.procuro.apimmdatamanagerlib.aPimmDataManager;

import java.sql.Wrapper;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import static android.view.View.combineMeasuredStates;
import static androidx.constraintlayout.widget.Constraints.TAG;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.daimajia.swipe.util.Attributes;

import java.util.ArrayList;
import java.util.Locale;
import java.util.TimeZone;

public class Segmented_active_fragment extends Fragment {

    View rootView;
    SwipeMenuItem end_route;

    public String site_name;
    public String site_address;
    public String route_name_txt, trailer_name_txt, dispatch_txt;
    public ArrayList<String> title = new ArrayList<>();
    public ArrayList<Integer> position = new ArrayList<>();
    public int pos;
    public Button button;
    private TextView tvEmptyTextView;
    private RecyclerView mRecyclerView;
    private ArrayList<Routes> mDataSet;
    public LinearLayout one, two, three, two_box1, two_box2, three_box1, three_box2, three_box3;
    public TextView name_, name1, name2, threee_name1, three_name2, three_name3,
            value, value1, value2, three_value1, three_value2, three_value3;
    public SimpleAdapter simpleAdapter;
    public SwipeMenuListView androidListView;

    aPimmDataManager dataManager = aPimmDataManager.getInstance();
    final TruckerDataManager truckerDataManager = TruckerDataManager.getInstance();
    ArrayList<AssignmentInfo> assignmentInfoList = truckerDataManager.assignmentInfo();


    final ArrayList<SdrGisSite> Sites = truckerDataManager.getSdrReport().Hierarchy.GIS.Sites;
    SdrGisSite site;
    final ArrayList<SdrStop> Stops = truckerDataManager.getSdrReport().Hierarchy.GIS.Stops;
    ArrayList<SdrPointOfInterest> Poi = truckerDataManager.getSdrReport().Hierarchy.GIS.PointsOfInterest;

    SdrPointOfInterest poi = null;
    SdrStop stop = null;


    TextView route_name, trailer_name, dispatch;
    ImageView route_icon;
    ImageButton info;


    public Segmented_active_fragment() {
        // Required empty public constructor
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.segmented_active_fragment, container, false);

        route_icon = rootView.findViewById(R.id.route_icon);
        trailer_name = rootView.findViewById(R.id.trailer_name);
        route_name = rootView.findViewById(R.id.route_name);
        dispatch = rootView.findViewById(R.id.dispatch);
        one = rootView.findViewById(R.id.one);
        two = rootView.findViewById(R.id.two);
        three = rootView.findViewById(R.id.three);
        name_ = rootView.findViewById(R.id.one_name);
        name1 = rootView.findViewById(R.id.name1);
        name2 = rootView.findViewById(R.id.name2);
        threee_name1 = rootView.findViewById(R.id.three_name1);
        three_name2 = rootView.findViewById(R.id.three_name2);
        three_name3 = rootView.findViewById(R.id.three_name3);
        value = rootView.findViewById(R.id.one_value);
        value1 = rootView.findViewById(R.id.value1);
        value2 = rootView.findViewById(R.id.value2);
        three_value1 = rootView.findViewById(R.id.three_value1);
        three_value2 = rootView.findViewById(R.id.three_value2);
        three_value3 = rootView.findViewById(R.id.three_value3);
        two_box1 = rootView.findViewById(R.id.two_box1);
        two_box2 = rootView.findViewById(R.id.two_box2);
        three_box1 = rootView.findViewById(R.id.three_box1);
        three_box2 = rootView.findViewById(R.id.three_box2);
        three_box3 = rootView.findViewById(R.id.three_box3);

        mRecyclerView = (RecyclerView) rootView.findViewById(R.id.my_recycler_view);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(rootView.getContext().getApplicationContext()));
        mDataSet = TruckerDataManager.getInstance().getRoutes();

        loadData();
        metrics();


//        if(mDataSet.isEmpty()){
//            Toast.makeText(getContext(),"NULL",Toast.LENGTH_LONG).show();
//        }else {
//            Toast.makeText(getContext(),"NOT NULL",Toast.LENGTH_LONG).show();
//        }

        SwipeRecyclerViewAdapter mAdapter = new SwipeRecyclerViewAdapter(rootView.getContext().getApplicationContext(), mDataSet);
        ((SwipeRecyclerViewAdapter) mAdapter).setMode(Attributes.Mode.Single);
        mRecyclerView.setAdapter(mAdapter);
        mRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                Log.e("RecyclerView", "onScrollStateChanged");
            }
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
            }
        });
        return rootView;
    }


    public void loadData() {
        View view = getLayoutInflater().inflate(R.layout.swipe_layout, null);
        final ImageView icons = (ImageView) view.findViewById(R.id.Image_icon);

        aPimmDataManager dataManager = aPimmDataManager.getInstance();
        final TruckerDataManager truckerDataManager = TruckerDataManager.getInstance();


        ArrayList<AssignmentInfo> assignmentInfoList = truckerDataManager.assignmentInfo();
        final AssignmentInfo assignmentInfo = assignmentInfoList.get(0);

        SimpleDateFormat inputFormat = new SimpleDateFormat(
                "EEE MMM dd HH:mm:ss 'GMT' yyyy", Locale.US);
        inputFormat.setTimeZone(TimeZone.getTimeZone("Etc/UTC"));
        SimpleDateFormat outputFormat = new SimpleDateFormat("MMM dd, yyyy h:mm a");
        // Displaying Active Route
        final String route_name_txt = assignmentInfo.routeName;
        final String trailer_name_txt = assignmentInfo.tractorName;
        route_name.setText("Route : " + route_name_txt);
        trailer_name.setText(trailer_name_txt);
        dispatch.setText(outputFormat.format(assignmentInfo.dispatch));


        System.out.println("STOP SIZE : " + Stops.size());
        System.out.println("POI SIZE : " + Poi.size());
        System.out.println("SITE SIZE : " + Sites.size());
        System.out.println("TOTAL  SIZE : " + Sites.size() + Stops.size() + Poi.size());

        // Transfer data from  new Array
        int icon = 0;
        for (int i = 0; i <mDataSet.size(); i++) {
            Routes item = mDataSet.get(i);
            if (item.getSite_desc().equalsIgnoreCase("Poi")){
                icon++;
            }
            else if (item.getSite_desc().equalsIgnoreCase("Stop") && item.getArrival_time() !=null ){

                icon++;
            }
        }
        Date dcArrivalTime = TruckerDataManager.getInstance().getSdrReport().Hierarchy.ArrivalTime;
        Date dcReturnTime = TruckerDataManager.getInstance().getSdrReport().Hierarchy.DepartureTime;

        Routes item = mDataSet.get(0);
        if (icon <=0 && Poi.size() <= 0 && item.getDeparture_time() == null) {
            route_icon.setBackgroundResource(R.drawable.route_icon_dispatch_gray);
        } else {
            route_icon.setBackgroundResource(R.drawable.route_blue_green_check);
        }
    }

    public void metrics() {

        List<SdrMetric> Metrics = truckerDataManager.getSdrReport().Hierarchy.Metrics;
        int metrics_counter = 0;
        int counter_freezer = 0;
        int counter_dry = 0;
        int counter_cooler = 0;
        ArrayList<Integer> Freezer_position = new ArrayList<>();
        ArrayList<String> Freezer_title = new ArrayList<>();
        ArrayList<Integer> Cooler_position = new ArrayList<>();
        ArrayList<String> Cooler_title = new ArrayList<>();
        ArrayList<Integer> Dry_position = new ArrayList<>();
        ArrayList<String> Dry_title = new ArrayList<>();
        ArrayList<String> Merge_title = new ArrayList<>();

        SdrMetric metric = null;

        System.out.println("SIZE : " + Metrics.size());

        for (int i = 0; i < Metrics.size(); i++) {
            metric = Metrics.get(i);

            System.out.println("COmpartment : " + metric.Compartment);
            System.out.println("Compartment Name : " + metric.CompartmentName);
            System.out.println("Last Value : " + metric.LastValue);
            System.out.println("Primary : " + metric.Primary);

            if (metric.CompartmentName.equalsIgnoreCase("") ||
                    metric.CompartmentName.equalsIgnoreCase(null)
                    || metric.CompartmentName.equalsIgnoreCase("null")) {
            } else {
                if (metric.Primary == true) {
                    if (metric.Compartment.toString().equalsIgnoreCase("MetricCompartment_Freezer")) {
                        metrics_counter++;
                        System.out.println(metrics_counter);
                        Freezer_position.add(i);
                        Freezer_title.add(String.valueOf(metric.Compartment));
                        System.out.println("Freezer TITLE : " + Freezer_title);
                        System.out.println("Freezer Position : " + Freezer_position);
                        if (counter_freezer == 0) {
                            Merge_title.addAll(Freezer_title);
                            counter_freezer++;
                        }
                    } else if (metric.Compartment.toString().equalsIgnoreCase("MetricCompartment_Cooler")) {
                        metrics_counter++;
                        System.out.println(metrics_counter);
                        Cooler_position.add(i);
                        Cooler_title.add(String.valueOf(metric.Compartment));
                        System.out.println("Freezer TITLE : " + Cooler_title);
                        System.out.println("Freezer Position : " + Cooler_position);
                        if (counter_cooler == 0) {
                            Merge_title.addAll(Cooler_title);
                            counter_cooler++;
                        }
                    } else if (metric.Compartment.toString().equalsIgnoreCase("MetricCompartment_Dry")) {
                        metrics_counter++;
                        System.out.println(metrics_counter);
                        position.add(i);
                        Dry_position.add(i);
                        Dry_title.add(String.valueOf(metric.Compartment));
                        System.out.println("Freezer TITLE : " + Dry_title);
                        System.out.println("Freezer Position : " + Dry_position);
                        if (counter_dry == 0) {
                            Merge_title.addAll(Dry_title);
                            counter_dry++;
                        }
                    }
                }
            }
        }
        if (Merge_title.size() == 1) {
            one.setVisibility(View.VISIBLE);
            two.setVisibility(View.GONE);
            three.setVisibility(View.GONE);
            if (Merge_title.get(0).equalsIgnoreCase("MetricCompartment_Dry")) {
                metric = Metrics.get(Dry_position.get(Dry_position.size() - 1));
                name_.setText("Dry");
                name_.setTextColor(Color.WHITE);
                if (metric.LastValue == 0.0){
                    value.setText("--");
                }else{
                    value.setText(String.valueOf(metric.LastValue));
                }
                value.setTextColor(Color.WHITE);
                if (metric.CompartmentName.equalsIgnoreCase("Freezer")) {
                    one.setBackgroundColor(Color.parseColor("#75B3E9"));
                    one.setBackground(new ColorDrawable(Color.parseColor("#75B3E9")));
                } else if (metric.CompartmentName.equalsIgnoreCase("Cooler")) {
                    one.setBackgroundColor(Color.parseColor("#009444"));
                    one.setBackground(new ColorDrawable(Color.parseColor("#009444")));
                } else if (metric.CompartmentName.equalsIgnoreCase("Dry")) {
                    one.setBackground(new ColorDrawable(Color.parseColor("#E9B247")));
                    one.setBackgroundColor(Color.parseColor("#E9B247"));
                }
            } else if (Merge_title.get(0).equalsIgnoreCase("MetricCompartment_Cooler")) {
                metric = Metrics.get(Cooler_position.get(Cooler_position.size() - 1));
                name_.setText("Chilled");
                name_.setTextColor(Color.WHITE);
                if (metric.LastValue == 0.0){
                    value.setText("--");
                }else{
                    value.setText(String.valueOf(metric.LastValue));
                }
                value.setTextColor(Color.WHITE);
                if (metric.CompartmentName.equalsIgnoreCase("Freezer")) {
                    one.setBackgroundColor(Color.parseColor("#75B3E9"));
                    one.setBackground(new ColorDrawable(Color.parseColor("#75B3E9")));
                } else if (metric.CompartmentName.equalsIgnoreCase("Cooler")) {
                    one.setBackgroundColor(Color.parseColor("#009444"));
                    one.setBackground(new ColorDrawable(Color.parseColor("#009444")));
                } else if (metric.CompartmentName.equalsIgnoreCase("Dry")) {
                    one.setBackground(new ColorDrawable(Color.parseColor("#E9B247")));
                    one.setBackgroundColor(Color.parseColor("#E9B247"));
                }
            } else if (Merge_title.get(0).equalsIgnoreCase("MetricCompartment_Freezer")) {
                metric = Metrics.get(Freezer_position.get(Freezer_position.size() - 1));
                name_.setText("Frozen");
                name_.setTextColor(Color.WHITE);
                if (metric.LastValue == 0.0){
                    value.setText("--");
                }else{
                    value.setText(String.valueOf(metric.LastValue));
                }
                value.setTextColor(Color.WHITE);
                if (metric.CompartmentName.equalsIgnoreCase("Freezer")) {
                    one.setBackgroundColor(Color.parseColor("#75B3E9"));
                    one.setBackground(new ColorDrawable(Color.parseColor("#75B3E9")));
                } else if (metric.CompartmentName.equalsIgnoreCase("Cooler")) {
                    one.setBackgroundColor(Color.parseColor("#009444"));
                    one.setBackground(new ColorDrawable(Color.parseColor("#009444")));
                } else if (metric.CompartmentName.equalsIgnoreCase("Dry")) {
                    one.setBackground(new ColorDrawable(Color.parseColor("#E9B247")));
                    one.setBackgroundColor(Color.parseColor("#E9B247"));
                }
            }

        } else if (Merge_title.size() == 2) {
            one.setVisibility(View.GONE);
            two.setVisibility(View.VISIBLE);
            three.setVisibility(View.GONE);
            for (int j = 0; j < 2; j++) {
                if (Merge_title.get(j).equalsIgnoreCase("MetricCompartment_Dry")) {
                    metric = Metrics.get(Dry_position.get(Dry_position.size() - 1));
                    if (j == 0) {
                        name1.setText("Dry");
                        name1.setTextColor(Color.WHITE);
                        if (metric.LastValue == 0.0){
                            value1.setText("--");
                        }else{
                            value1.setText(String.valueOf(metric.LastValue));
                        }
                        value1.setTextColor(Color.WHITE);
                        if (metric.CompartmentName.equalsIgnoreCase("Freezer")) {
                            two_box1.setBackgroundColor(Color.parseColor("#75B3E9"));
                        } else if (metric.CompartmentName.equalsIgnoreCase("Cooler")) {
                            two_box1.setBackgroundColor(Color.parseColor("#009444"));
                        } else if (metric.CompartmentName.equalsIgnoreCase("Dry")) {
                            two_box1.setBackgroundColor(Color.parseColor("#E9B247"));
                        }
                    } else if (j == 1) {
                        name2.setText("Dry");
                        name2.setTextColor(Color.WHITE);
                        if (metric.LastValue == 0.0){
                            value2.setText("--");
                        }else{
                            value2.setText(String.valueOf(metric.LastValue));
                        }
                        value2.setTextColor(Color.WHITE);
                        if (metric.CompartmentName.equalsIgnoreCase("Freezer")) {
                            two_box2.setBackgroundColor(Color.parseColor("#75B3E9"));
                        } else if (metric.CompartmentName.equalsIgnoreCase("Cooler")) {
                            two_box2.setBackgroundColor(Color.parseColor("#009444"));
                        } else if (metric.CompartmentName.equalsIgnoreCase("Dry")) {
                            two_box2.setBackgroundColor(Color.parseColor("#E9B247"));
                        }
                    }
                } else if (Merge_title.get(j).equalsIgnoreCase("MetricCompartment_Cooler")) {
                    metric = Metrics.get(Cooler_position.get(Cooler_position.size() - 1));
                    if (j == 0) {
                        name1.setText("Chilled");
                        name1.setTextColor(Color.WHITE);
                        if (metric.LastValue == 0.0){
                            value1.setText("--");
                        }else{
                            value1.setText(String.valueOf(metric.LastValue));
                        }
                        value1.setTextColor(Color.WHITE);
                        if (metric.CompartmentName.equalsIgnoreCase("Freezer")) {
                            two_box1.setBackgroundColor(Color.parseColor("#75B3E9"));
                        } else if (metric.CompartmentName.equalsIgnoreCase("Cooler")) {
                            two_box1.setBackgroundColor(Color.parseColor("#009444"));
                        } else if (metric.CompartmentName.equalsIgnoreCase("Dry")) {
                            two_box1.setBackgroundColor(Color.parseColor("#E9B247"));
                        }
                    } else if (j == 1) {
                        name2.setText("Chilled");
                        name2.setTextColor(Color.WHITE);
                        if (metric.LastValue == 0.0){
                            value2.setText("--");
                        }else{
                            value2.setText(String.valueOf(metric.LastValue));
                        }
                        value2.setTextColor(Color.WHITE);
                        if (metric.CompartmentName.equalsIgnoreCase("Freezer")) {
                            two_box2.setBackgroundColor(Color.parseColor("#75B3E9"));
                        } else if (metric.CompartmentName.equalsIgnoreCase("Cooler")) {
                            two_box2.setBackgroundColor(Color.parseColor("#009444"));
                        } else if (metric.CompartmentName.equalsIgnoreCase("Dry")) {
                            two_box2.setBackgroundColor(Color.parseColor("#E9B247"));
                        }
                    }
                } else if (Merge_title.get(j).equalsIgnoreCase("MetricCompartment_Freezer")) {
                    metric = Metrics.get(Freezer_position.get(Freezer_position.size() - 1));
                    if (j == 0) {
                        name1.setText("Frozen");
                        name1.setTextColor(Color.WHITE);
                        if (metric.LastValue == 0.0){
                            value1.setText("--");
                        }else{
                            value1.setText(String.valueOf(metric.LastValue));
                        }
                        value1.setTextColor(Color.WHITE);
                        if (metric.CompartmentName.equalsIgnoreCase("Freezer")) {
                            two_box1.setBackgroundColor(Color.parseColor("#75B3E9"));
                        } else if (metric.CompartmentName.equalsIgnoreCase("Cooler")) {
                            two_box1.setBackgroundColor(Color.parseColor("#009444"));
                        } else if (metric.CompartmentName.equalsIgnoreCase("Dry")) {
                            two_box1.setBackgroundColor(Color.parseColor("#E9B247"));
                        }
                    } else if (j == 1) {
                        name2.setText("Frozen");
                        name2.setTextColor(Color.WHITE);
                        if (metric.LastValue == 0.0){
                            value2.setText("--");
                        }else{
                            value2.setText(String.valueOf(metric.LastValue));
                        }
                        value2.setTextColor(Color.WHITE);
                        if (metric.CompartmentName.equalsIgnoreCase("Freezer")) {
                            two_box2.setBackgroundColor(Color.parseColor("#75B3E9"));
                        } else if (metric.CompartmentName.equalsIgnoreCase("Cooler")) {
                            two_box2.setBackgroundColor(Color.parseColor("#009444"));
                        } else if (metric.CompartmentName.equalsIgnoreCase("Dry")) {
                            two_box2.setBackgroundColor(Color.parseColor("#E9B247"));
                        }
                    }
                }
            }
        } else if (Merge_title.size() == 3) {
            one.setVisibility(View.GONE);
            two.setVisibility(View.GONE);
            three.setVisibility(View.VISIBLE);
            for (int j = 0; j < 3; j++) {
                if (Merge_title.get(j).equalsIgnoreCase("MetricCompartment_Dry")) {
                    metric = Metrics.get(Dry_position.get(Dry_position.size() - 1));
                    if (j == 0) {
                        threee_name1.setText("Dry");
                        threee_name1.setTextColor(Color.WHITE);
                        if (metric.LastValue == 0.0){
                            three_value1.setText("--");
                        }else{
                            three_value1.setText(String.valueOf(metric.LastValue));
                        }
                        three_value1.setTextColor(Color.WHITE);
                        if (metric.CompartmentName.equalsIgnoreCase("Freezer")) {
                            three_box1.setBackgroundColor(Color.parseColor("#75B3E9"));
                        } else if (metric.CompartmentName.equalsIgnoreCase("Cooler")) {
                            three_box1.setBackgroundColor(Color.parseColor("#009444"));
                        } else if (metric.CompartmentName.equalsIgnoreCase("Dry")) {
                            three_box1.setBackgroundColor(Color.parseColor("#E9B247"));
                        }
                    } else if (j == 1) {
                        threee_name1.setText("Dry");
                        threee_name1.setTextColor(Color.WHITE);
                        if (metric.LastValue == 0.0){
                            three_value1.setText("--");
                        }else{
                            three_value1.setText(String.valueOf(metric.LastValue));
                        }
                        three_value1.setTextColor(Color.WHITE);
                        if (metric.CompartmentName.equalsIgnoreCase("Freezer")) {
                            three_box1.setBackgroundColor(Color.parseColor("#75B3E9"));
                        } else if (metric.CompartmentName.equalsIgnoreCase("Cooler")) {
                            three_box1.setBackgroundColor(Color.parseColor("#009444"));
                        } else if (metric.CompartmentName.equals("Dry")) {
                            three_box1.setBackgroundColor(Color.parseColor("#E9B247"));
                        }
                    } else if (j == 2) {
                        threee_name1.setText("Dry");
                        threee_name1.setTextColor(Color.WHITE);
                        if (metric.LastValue == 0.0){
                            three_value1.setText("--");
                        }else{
                            three_value1.setText(String.valueOf(metric.LastValue));
                        }
                        three_value1.setTextColor(Color.WHITE);
                        if (metric.CompartmentName.equals("Freezer")) {
                            three_box1.setBackgroundColor(Color.parseColor("#75B3E9"));
                        } else if (metric.CompartmentName.equalsIgnoreCase("Cooler")) {
                            three_box1.setBackgroundColor(Color.parseColor("#009444"));
                        } else if (metric.CompartmentName.equalsIgnoreCase("Dry")) {
                            three_box1.setBackgroundColor(Color.parseColor("#E9B247"));
                        }
                    }
                } else if (Merge_title.get(j).equalsIgnoreCase("MetricCompartment_Cooler")) {
                    metric = Metrics.get(Cooler_position.get(Cooler_position.size() - 1));

                    if (j == 0) {
                        three_name2.setText("Chilled");
                        three_name2.setTextColor(Color.WHITE);
                        if (metric.LastValue == 0.0){
                            three_value2.setText("--");
                        }else{
                            three_value2.setText(String.valueOf(metric.LastValue));
                        }
                        three_value2.setTextColor(Color.WHITE);

                        if (metric.CompartmentName.equalsIgnoreCase("Freezer")) {
                            three_box2.setBackgroundColor(Color.parseColor("#75B3E9"));
                        } else if (metric.CompartmentName.equalsIgnoreCase("Cooler")) {
                            three_box2.setBackgroundColor(Color.parseColor("#009444"));
                        } else if (metric.CompartmentName.equalsIgnoreCase("Dry")) {
                            three_box2.setBackgroundColor(Color.parseColor("#E9B247"));
                        }
                    } else if (j == 1) {
                        three_name2.setText("Chilled");
                        three_name2.setTextColor(Color.WHITE);
                        if (metric.LastValue == 0.0){
                            three_value2.setText("--");
                        }else{
                            three_value2.setText(String.valueOf(metric.LastValue));
                        }
                        three_value2.setTextColor(Color.WHITE);

                        if (metric.CompartmentName.equalsIgnoreCase("Freezer")) {
                            three_box2.setBackgroundColor(Color.parseColor("#75B3E9"));
                        } else if (metric.CompartmentName.equalsIgnoreCase("Cooler")) {
                            three_box2.setBackgroundColor(Color.parseColor("#009444"));
                        } else if (metric.CompartmentName.equalsIgnoreCase("Dry")) {
                            three_box2.setBackgroundColor(Color.parseColor("#E9B247"));
                        }
                    } else if (j == 2) {
                        three_name2.setText("Chilled");
                        three_name2.setTextColor(Color.WHITE);
                        if (metric.LastValue == 0.0){
                            three_value2.setText("--");
                        }else{
                            three_value2.setText(String.valueOf(metric.LastValue));
                        }
                        three_value2.setTextColor(Color.WHITE);

                        if (metric.CompartmentName.equals("Freezer")) {
                            three_box2.setBackgroundColor(Color.parseColor("#75B3E9"));
                        } else if (metric.CompartmentName.equals("Cooler")) {
                            three_box2.setBackgroundColor(Color.parseColor("#009444"));
                        } else if (metric.CompartmentName.equals("Dry")) {
                            three_box2.setBackgroundColor(Color.parseColor("#E9B247"));
                        }
                    }
                } else if (Merge_title.get(j).equalsIgnoreCase("MetricCompartment_Freezer")) {
                    metric = Metrics.get(Freezer_position.get(Freezer_position.size() - 1));
                    if (j == 0) {
                        three_name3.setText("Frozen");
                        three_name3.setTextColor(Color.WHITE);
                        if (metric.LastValue == 0.0){
                            three_value3.setText("--");
                        }else{
                            three_value3.setText(String.valueOf(metric.LastValue));
                        }
                        three_value3.setTextColor(Color.WHITE);

                        if (metric.CompartmentName.equalsIgnoreCase("Freezer")) {
                            three_box3.setBackgroundColor(Color.parseColor("#75B3E9"));
                        } else if (metric.CompartmentName.equalsIgnoreCase("Cooler")) {
                            three_box3.setBackgroundColor(Color.parseColor("#009444"));
                        } else if (metric.CompartmentName.equalsIgnoreCase("Dry")) {
                            three_box3.setBackgroundColor(Color.parseColor("#E9B247"));
                        }
                    } else if (j == 1) {
                        three_name3.setText("Frozen");
                        three_name3.setTextColor(Color.WHITE);
                        if (metric.LastValue == 0.0){
                            three_value3.setText("--");
                        }else{
                            three_value3.setText(String.valueOf(metric.LastValue));
                        }
                        three_value3.setTextColor(Color.WHITE);

                        if (metric.CompartmentName.equalsIgnoreCase("Freezer")) {
                            three_box3.setBackgroundColor(Color.parseColor("#75B3E9"));
                        } else if (metric.CompartmentName.equalsIgnoreCase("Cooler")) {
                            three_box3.setBackgroundColor(Color.parseColor("#009444"));
                        } else if (metric.CompartmentName.equalsIgnoreCase("Dry")) {
                            three_box3.setBackgroundColor(Color.parseColor("#E9B247"));
                        }
                    } else if (j == 2) {
                        three_name3.setText("Frozen");
                        three_name3.setTextColor(Color.WHITE);
                        if (metric.LastValue == 0.0){
                            three_value3.setText("--");
                        }else{
                            three_value3.setText(String.valueOf(metric.LastValue));
                        }
                        three_value3.setTextColor(Color.WHITE);

                        if (metric.CompartmentName.equalsIgnoreCase("Freezer")) {
                            three_box3.setBackgroundColor(Color.parseColor("#75B3E9"));
                        } else if (metric.CompartmentName.equalsIgnoreCase("Cooler")) {
                            three_box3.setBackgroundColor(Color.parseColor("#009444"));
                        } else if (metric.CompartmentName.equalsIgnoreCase("Dry")) {
                            three_box3.setBackgroundColor(Color.parseColor("#E9B247"));
                        }
                    }
                }
            }
            System.out.println(Merge_title);
        }
    }
}

