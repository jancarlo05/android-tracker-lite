package com.procuro.androidtruckerlite;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;

import com.google.android.gms.location.Geofence;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.Circle;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.navigation.NavigationView;
import com.procuro.androidlocationservice.LocationService;
import com.procuro.androidtruckerlite.DBHelper.DBHelper;
import com.procuro.apimmdatamanagerlib.AssignmentInfo;
import com.procuro.apimmdatamanagerlib.DeliveryPOITypeEnum;
import com.procuro.apimmdatamanagerlib.LatLon;
import com.procuro.apimmdatamanagerlib.SdrPointOfInterest;
import com.procuro.apimmdatamanagerlib.SdrStop;
import com.procuro.apimmdatamanagerlib.User;
import com.procuro.apimmdatamanagerlib.aPimmDataManager;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.UUID;

import de.hdodenhof.circleimageview.CircleImageView;
import dmax.dialog.SpotsDialog;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    public static BottomNavigationView navView;
    private GoogleMap mMap;
    public static TextView textid, fullname;

    LinearLayout truck;
    Button end_route;

    aPimmDataManager dataManager;
    final TruckerDataManager truckerDataManager = TruckerDataManager.getInstance();
    ArrayList<AssignmentInfo> assignmentInfos = truckerDataManager.assignmentInfo();
    CountDownTimer countDownTimer;

    public static ImageButton poi_save;
    public static Toolbar toolbar;
    private SpotsDialog spotsDialog;
    public static ImageButton back;
    CircleImageView imageView;
    DBHelper dbHelper;

    private int show_popup = 0;
    private int arrival = 0;
    private int departure = 0;
    private double lat = 0;
    private double longt = 0;
    private String location_names = "";
    private String site_name = "";
    private String site_id = "";
    private  int positions;
    private int start_point = 0;
    private int site_arrival = 0;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(null);

        dataManager = aPimmDataManager.getInstance();
        User user = truckerDataManager.getUser();
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);

        navView = findViewById(R.id.btm_nav_view);
        truck = findViewById(R.id.truck);
        end_route = findViewById(R.id.end_route);
        fullname = findViewById(R.id.fullname);
        poi_save = findViewById(R.id.poi_save);
        imageView = findViewById(R.id.pp);
        back = findViewById(R.id.back);
        dbHelper = new DBHelper(this);

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        NavigationView navigationView = findViewById(R.id.nav_view);
        View headerview = navigationView.getHeaderView(0);
        CircleImageView imageView1 = headerview.findViewById(R.id.drawer_profile_Image);
        TextView profile_name1 = headerview.findViewById(R.id.header_profile_name);
        imageView1.setImageBitmap(dbHelper.gimage());
        profile_name1.setText(user.getFullName());
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();
        navigationView.setNavigationItemSelectedListener(this);

        fullname.setText(user.getFullName());
        imageView.setImageBitmap(dbHelper.gimage());

        show_popup = 0;
        show_pop_timer();
        spotsDialog = (SpotsDialog) new SpotsDialog.Builder().setContext(this).setMessage("Reloading").build();
        navView.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);

        initLocationServiceListeners();
        displayDeliveryModule();

        ArrayList<AssignmentInfo> assignmentInfos=TruckerDataManager.getInstance().assignmentInfo();
        AssignmentInfo assignmentInfo = assignmentInfos.get(0);

    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            alert();
        }
        show_btm_nav();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();
        if (id == R.id.nav_home) {
            Intent intent = new Intent(MainActivity.this, WelcomeScreen.class);
            startActivity(intent);
            countDownTimer.cancel();
            finish();
            // Handle the camera action
        } else if (id == R.id.nav_gallery) {
            Intent intent = new Intent(MainActivity.this, Profile.class);
            startActivity(intent);
            finish();
            countDownTimer.cancel();
        } else if (id == R.id.nav_slideshow) {
            Intent intent = new Intent(MainActivity.this, Help.class);
            startActivity(intent);
            finish();
            countDownTimer.cancel();
        } else if (id == R.id.nav_tools) {
            Intent intent = new Intent(MainActivity.this, Login.class);
            startActivity(intent);
            finish();
            countDownTimer.cancel();
        }
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            final int previousItem = navView.getSelectedItemId();
            final int nextItem = item.getItemId();
            User user = truckerDataManager.getUser();
            setTitle(user.getFullName());
            hideKeyboard();

            if (previousItem != nextItem) {
                switch (item.getItemId()) {
                    case R.id.nav_home:
                        displayDeliveryModule();
                        return true;
                    case R.id.nav_nav:
                        displayNavigationModule();
                        return true;
                    case R.id.nav_report:
                        displayReportsModule();
                        return true;
                }
            }
            return false;
        }
    };

    private void hideKeyboard() {
        // Hiding keyboard and save button
        poi_save.setVisibility(View.GONE);
        if (navView != null) {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(navView.getWindowToken(), 0);
        }
    }

    public void alert() {
        AlertDialog alertDialog = new AlertDialog.Builder(this)
                //set icon
                .setIcon(R.drawable.logo)
                //set title
                .setTitle("Warning!")
                //set message
                .setMessage("Do you want to exit?")
                //set positive button
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                        Intent intent = new Intent(MainActivity.this, Login.class);
                        startActivity(intent);
                        countDownTimer.cancel();
                        finish();
                    }
                })
                //set negative button
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                    }
                })
                .show();
    }

    public static void hide() {
        toolbar.setVisibility(View.GONE);
        back.setVisibility(View.VISIBLE);
    }

    public static void show() {
        toolbar.setVisibility(View.VISIBLE);
        back.setVisibility(View.GONE);
    }

    public void unsched_stop_alert() {
        show_popup = 0;
        LayoutInflater li = LayoutInflater.from(MainActivity.this);
        View view = li.inflate(R.layout.unsched_stop_alert, null);
        final android.app.AlertDialog.Builder alertDialogBuilder = new android.app.AlertDialog.Builder(MainActivity.this);
        TextView address = view.findViewById(R.id.location_name);
        address.setText(location_names);
        alertDialogBuilder.setView(view);

        final android.app.AlertDialog alertDialogCongratulations = alertDialogBuilder.create();
        alertDialogCongratulations.getWindow().setLayout(LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);//
        alertDialogCongratulations.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        alertDialogCongratulations.setCancelable(false);
        alertDialogCongratulations.show();

        new CountDownTimer(5000, 1000) {
            public void onTick(long millisUntilFinished) {
            }

            public void onFinish() {
                alertDialogCongratulations.dismiss();
                show_popup = 0;
            }
        }.start();

    }

    public void show_pop_timer() {
        countDownTimer = new CountDownTimer(1000, 1000) {
            public void onTick(long millisUntilFinished) {
            }
            public void onFinish() {
                checklocation(positions);

                //checking startpoint location

                checkstartpoint_location();


                // ADDING NEW POI - UNSCHEDULED STOP
                if (show_popup == 1) {
                    int count = 0;
                    UUID uuid = UUID.randomUUID();
                    final String poi_id = uuid.toString();
                    Date currentTime = Calendar.getInstance().getTime();
                    ArrayList<Routes> routes = TruckerDataManager.getInstance().getRoutes();
                    ArrayList<SdrStop> Stops = TruckerDataManager.getInstance().getSdrReport().Hierarchy.GIS.Stops;
                    ArrayList<SdrPointOfInterest> Poi = TruckerDataManager.getInstance().getSdrReport().Hierarchy.GIS.PointsOfInterest;
                    ArrayList<StopListItem> stopListItems = TruckerDataManager.getInstance().getStopListItems();
                    LatLon latLng = new LatLon();
                    latLng.Lon = longt;
                    latLng.Lat = lat;
                    int counter_skip = 0;

                    for (int i = 0; i < Stops.size() + Poi.size(); i++) {
                        final Routes skip = routes.get(i);
                        if (skip.getSite_desc().contains("Stop")) {
                            if (skip.getArrival_time() != null) {
                                counter_skip++;
                            }
                        } else if (skip.getSite_desc().contains("Poi")) {
                            if (skip.getArrival_time() != null) {
                                counter_skip++;
                            }
                        }
                    }
                    if (count == 0) {
                        routes.add(new Routes("Unscheduled Stop", 0, location_names, "Poi", currentTime, null,
                                null, lat, longt, 0, 50.0f, -1, poi_id));
                        count = 1;
                        StopListItem newpoi = new StopListItem(poi_id,
                                "Unscheduled Stop",
                                location_names,
                                currentTime,
                                null,
                                DeliveryPOITypeEnum.DeliveryPOITypesEnum.setIntValue(-1),
                                latLng,
                                50.0f,
                                null,
                                false, null);
                        stopListItems.add(newpoi);

                        site_id = poi_id;

                        LocationService locationService = LocationService.getInstance();
                        ArrayList<Geofence> geofences = new ArrayList<>();
                        Geofence geofence = locationService.createGeofence(poi_id, lat, longt, 50.0f);
                        geofences.add(geofence);
                        LocationService.getInstance().setGeofenceList(geofences);
                    }


                    Collections.sort(routes.subList(1 + counter_skip, routes.size()), new Comparator<Routes>() {
                        @Override
                        public int compare(Routes routes, Routes t1) {
                            if (t1.getArrival_time() == null) {
                                return (routes.getArrival_time() == null) ? 0 : -1;
                            }
                            if (routes.getArrival_time() == null) {
                                return 1;
                            }
                            return routes.getArrival_time().compareTo(t1.getArrival_time());
                        }
                    });
                    truckerDataManager.setRoutes(routes);

                    unsched_stop_alert();


                }



                //adding arrivaltime

                else if (arrival == 1) {
                    Date currentTime = Calendar.getInstance().getTime();
                    int count = 0;
                    ArrayList<Routes> routes = TruckerDataManager.getInstance().getRoutes();
                    for (int i = 0; i < routes.size(); i++) {
                        Routes item = routes.get(i);
                        if (count == 0) {
                            if (site_id.equalsIgnoreCase(item.getSite_id())) {
                                if (item.getSite_desc().equalsIgnoreCase("site_departure")) {
                                    if (item.getArrival_time() == null){
//                                        arrival_alert(i, item.getRoute_name(), item.getAddress());
                                        site_arrival_alert(i,item.getRoute_name());
                                        item.setArrival_time(currentTime);
                                        count = 1;
                                    }
                                } else if (item.getSite_desc().equalsIgnoreCase("site_arrival")) {
                                    if (item.getArrival_time() == null) {
//                                        site_arrival_alert(i,item.getRoute_name());
                                        site_arrival_alert(i,item.getRoute_name());
                                        item.setArrival_time(currentTime);
//                                        arrival_alert(i, item.getRoute_name(), item.getAddress());
                                        count = 1;
                                    }
                                } else {
                                    if (item.getArrival_time() == null) {
                                        arrival_alert(i, item.getRoute_name(), item.getAddress());
                                        count = 1;
                                    }

                                }
                            }
                        }
                    }
                }


                // Adding departure time
                else if (departure == 1) {
                    int count = 0;
                    boolean find = true;
                    ArrayList<Routes> routes = TruckerDataManager.getInstance().getRoutes();
                    for (int i = 0; i < routes.size(); i++) {
                        Routes item = routes.get(i);
                        if (count == 0) {
                            if (site_id.equalsIgnoreCase(item.getSite_id())) {
                                if (item.getSite_desc().equalsIgnoreCase("site_departure")&& item.getPoi_type() != 50) {
                                    //  IF DC ARRIVAL TIME IS NULL THEN WE CAN ADD VALUE FOR DEPARTURE TIME
                                    if (item.getDeparture_time() == null) {
                                        departure_alert(i, item.getRoute_name());
                                        count = 1;
                                    }
                                } else if (item.getSite_desc().equalsIgnoreCase("site_arrival")&& item.getPoi_type() != 50) {
                                    if (item.getDeparture_time() == null) {
                                        departure_alert(i, item.getRoute_name());

                                        count = 1;
                                    }
                                } else {
                                    // IF STORE ARRIVAL ARE STILL NULL WE CANNOT ADD DEPARTURE
                                    if (item.getDeparture_time() == null) {
                                        if (count == 0) {
                                            departure_alert(i, item.getRoute_name());
                                            count = 1;
                                        }

                                    }
                                }
                            }
                        }
                    }
                }


                start();
            }
        }.start();
    }

    public void arrival_alert(int position, String site_name, String address) {
        arrival = 0;
        final ArrayList<Routes> routes = TruckerDataManager.getInstance().getRoutes();
        final Routes item = routes.get(position);
        LayoutInflater li = LayoutInflater.from(MainActivity.this);
        View view = li.inflate(R.layout.start_point_alert, null);
        final android.app.AlertDialog.Builder alertDialogBuilder = new android.app.AlertDialog.Builder(MainActivity.this);
        TextView sitename = view.findViewById(R.id.site_name);
        TextView location_name = view.findViewById(R.id.location_name);
        Button accept = view.findViewById(R.id.accept);
        Button ignore = view.findViewById(R.id.ignore);

        sitename.setText(site_name);
        location_name.setText(address);
        alertDialogBuilder.setView(view);
        final android.app.AlertDialog alertDialogCongratulations = alertDialogBuilder.create();
        alertDialogCongratulations.getWindow().setLayout(LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);//
        alertDialogCongratulations.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        alertDialogCongratulations.setCancelable(false);
        alertDialogCongratulations.show();

        accept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Date currentTime = Calendar.getInstance().getTime();
                int counter_skip = 0;
                for (int i = 0; i < routes.size() - 2; i++) {
                    final Routes skip = routes.get(i);
                    if (skip.getSite_desc().contains("Stop")) {
                        if (skip.getArrival_time() != null) {
                            System.out.println("counter Skip:" + counter_skip);
                            counter_skip++;
                        }
                    } else if (skip.getSite_desc().contains("Poi")) {
                        if (skip.getArrival_time() != null) {
                            System.out.println("counter Skip poi:" + counter_skip);
                            counter_skip++;
                        }
                    }
                }
                item.setArrival_time(currentTime);
                Collections.sort(routes.subList(counter_skip + 1, routes.size() - 1), new Comparator<Routes>() {
                    @Override
                    public int compare(Routes routes, Routes t1) {
                        if (t1.getArrival_time() == null) {
                            return (routes.getArrival_time() == null) ? 0 : -1;
                        }
                        if (routes.getArrival_time() == null) {
                            return 1;
                        }
                        return routes.getArrival_time().compareTo(t1.getArrival_time());

                    }
                });

                alertDialogCongratulations.dismiss();
                arrival = 0;
            }
        });

        ignore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alertDialogCongratulations.dismiss();
                arrival = 0;
            }
        });


        new CountDownTimer(5000, 1000) {
            public void onTick(long millisUntilFinished) {
            }

            public void onFinish() {
                alertDialogCongratulations.dismiss();
                arrival = 0;
            }
        }.start();
    }

    public void departure_alert(int position, String site_name) {
        departure = 0;
        final ArrayList<Routes> routes = TruckerDataManager.getInstance().getRoutes();
        final Routes item = routes.get(position);
        LayoutInflater li = LayoutInflater.from(MainActivity.this);
        View view = li.inflate(R.layout.poi_departure_alert, null);
        final android.app.AlertDialog.Builder alertDialogBuilder = new android.app.AlertDialog.Builder(MainActivity.this);
        TextView location_name = view.findViewById(R.id.alert_sitename);
        TextView type = view.findViewById(R.id.departure_type);
        if (item.getSite_desc().equalsIgnoreCase("site_arrival") ||
                item.getSite_desc().equalsIgnoreCase("site_departure")) {
            type.setText("DC Departure");
        } else if (item.getSite_desc().equalsIgnoreCase("Stop")) {
            type.setText("Store Departure");
        } else if (item.getSite_desc().equalsIgnoreCase("Poi")) {
            type.setText("Poi Departure");
        }
        location_name.setText(site_name);
        alertDialogBuilder.setView(view);

        final android.app.AlertDialog alertDialogCongratulations = alertDialogBuilder.create();
        alertDialogCongratulations.getWindow().setLayout(LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);//
        alertDialogCongratulations.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        alertDialogCongratulations.setCancelable(false);
        alertDialogCongratulations.show();
        Date currentTime = Calendar.getInstance().getTime();


        int counter_skip = 0;
        for (int i = 0; i < routes.size() - 2; i++) {

            final Routes skip = routes.get(i);
            if (skip.getSite_desc().contains("Stop")) {
                if (skip.getArrival_time() != null) {
                    System.out.println("counter Skip:" + counter_skip);
                    counter_skip++;
                }
            } else if (skip.getSite_desc().contains("Poi")) {
                if (skip.getArrival_time() != null) {
                    System.out.println("counter Skip poi:" + counter_skip);
                    counter_skip++;
                }
            }
        }
        item.setDeparture_time(currentTime);
        Collections.sort(routes.subList(counter_skip + 1, routes.size() - 1), new Comparator<Routes>() {
            @Override
            public int compare(Routes routes, Routes t1) {
                if (t1.getArrival_time() == null) {
                    return (routes.getArrival_time() == null) ? 0 : -1;
                }
                if (routes.getArrival_time() == null) {
                    return 1;
                }
                return routes.getArrival_time().compareTo(t1.getArrival_time());

            }
        });

        new CountDownTimer(5000, 1000) {
            public void onTick(long millisUntilFinished) {
            }

            public void onFinish() {
                alertDialogCongratulations.dismiss();
                show_popup = 0;
                departure = 0;
            }
        }.start();

    }

    public void site_arrival_alert(int position, String site_name) {
        departure = 0;
        final ArrayList<Routes> routes = TruckerDataManager.getInstance().getRoutes();
        final Routes item = routes.get(position);
        LayoutInflater li = LayoutInflater.from(MainActivity.this);
        View view = li.inflate(R.layout.poi_departure_alert, null);
        final android.app.AlertDialog.Builder alertDialogBuilder = new android.app.AlertDialog.Builder(MainActivity.this);
        TextView location_name = view.findViewById(R.id.alert_sitename);
        TextView type = view.findViewById(R.id.departure_type);
            type.setText("DC Arrival");
        location_name.setText(site_name);
        alertDialogBuilder.setView(view);

        final android.app.AlertDialog alertDialogCongratulations = alertDialogBuilder.create();
        alertDialogCongratulations.getWindow().setLayout(LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);//
        alertDialogCongratulations.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        alertDialogCongratulations.setCancelable(false);
        alertDialogCongratulations.show();
        Date currentTime = Calendar.getInstance().getTime();


        int counter_skip = 0;
        item.setArrival_time(currentTime);
        new CountDownTimer(5000, 1000) {
            public void onTick(long millisUntilFinished) {
            }

            public void onFinish() {
                alertDialogCongratulations.dismiss();
                show_popup = 0;
                departure = 0;
            }
        }.start();

    }



    public void initLocationServiceListeners() {
        LocationService.getInstance().setGeofenceTransitionListener(new LocationService.OnGeofenceTransitionListener() {
            @Override
            public void didEnterGeofence(Geofence geofence) {
                ArrayList<Routes> routes = TruckerDataManager.getInstance().getRoutes();
                String id = TruckerDataManager.getInstance().getSiteNameForSiteID(geofence.getRequestId());
                String siteName = null;
                for (Routes item : routes) {
                    if (item.getSite_id().equalsIgnoreCase(id)) {
                        siteName = item.getRoute_name();
                    }
                }
                Log.v("Location Service", "triggeringGeofences GEOFENCE_TRANSITION_EXIT: " + siteName);
                displayMessage("GEOFENCE_TRANSITION_EXIT: " + siteName);
                site_id = id;
                site_name = siteName;
                arrival++;
                LocationService.getInstance().startUnscheduledStopTimer();
                // TODO: Display Geofence Enter pop-up
            }

            @Override
            public void didExitGeofence(Geofence geofence) {
                ArrayList<Routes> routes = TruckerDataManager.getInstance().getRoutes();
                String id = TruckerDataManager.getInstance().getSiteNameForSiteID(geofence.getRequestId());
                String siteName = null;
                for (Routes item : routes) {
                    if (item.getSite_id().equalsIgnoreCase(id)) {
                        siteName = item.getRoute_name();
                    }
                }
                Log.v("Location Service", "triggeringGeofences GEOFENCE_TRANSITION_EXIT: " + siteName);
                displayMessage("GEOFENCE_TRANSITION_EXIT: " + siteName);
                site_id = id;
                site_name = siteName;
                departure++;
                LocationService.getInstance().startUnscheduledStopTimer();
                // TODO: Display Geofence Exit pop-up
            }
        });

        LocationService.getInstance().setOnUnscheduledStopDetectedListener(new LocationService.OnUnscheduledStopDetectedListener() {
            @Override
            public void onUnscheduledStopDetectedListener(Location location) {
                Log.v("Location Service", "unscheduledStopDetected : " + location);
                if (location != null) {
                    lat = location.getLatitude();
                    longt = location.getLongitude();
                    List<Address> addresses;
                    Geocoder geocoder = new Geocoder(MainActivity.this, Locale.getDefault());
                    try {
                        addresses = geocoder.getFromLocation(lat, longt, 1);
                        location_names = addresses.get(0).getAddressLine(0);
                        // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
                // TODO: Display Unscheduled Stop Detection pop-up
                show_popup++;
            }
        });
    }

    private void displayMessage(final String message) {
        Handler handler = new Handler(Looper.getMainLooper());
        handler.post(new Runnable() {
            @Override
            public void run() {
                Toast.makeText(getApplicationContext(),
                        message,
                        Toast.LENGTH_LONG).show();
            }
        });
    }

    public static void hide_btm_nav() {
        MainActivity.navView.setVisibility(View.GONE);
    }

    public static void show_btm_nav() {
        MainActivity.navView.setVisibility(View.VISIBLE);
    }
    private void displayDeliveryModule() {
        getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container,
                new DeliveryFragment()).commit();
    }

    private void displayNavigationModule() {
        getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container,
                new MapFragment()).commit();
        show();
    }

    private void displayReportsModule() {
        getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container,
                new ReportFragment()).commit();
        poi_save.setVisibility(View.GONE);
    }

    //checking location if inside the radius of marker
    public void checklocation(int position) {
        Location location = LocationService.getInstance().getLastKnownLocation();
        ArrayList<Routes> routes = TruckerDataManager.getInstance().getRoutes();
        CircleOptions c = new CircleOptions();
                float[] distance = new float[2];
                 for (int i = 0; i <routes.size() ; i++) {
                     Routes item = routes.get(i);
                         LatLng latLng = new LatLng(item.getLat(), item.getLongt());
                         c.center(latLng);
                         c.radius(item.getGeofenceRadius());
                         Location.distanceBetween(location.getLatitude(), location.getLongitude(), c.getCenter().latitude, c.getCenter().longitude, distance);
                         if (distance[0] > c.getRadius()) {
//                             Toast.makeText(getApplicationContext(), "outside", Toast.LENGTH_LONG).show();
//                             System.out.println("OUTSIDE");

                         } else {
                             LocationService.getInstance().stopUnscheduledStopTimer();
                         }
                     }
                }
    //start point
    public void startpoint_alert(final Location location) {
        start_point = 0;
        final ArrayList<Routes> routes = TruckerDataManager.getInstance().getRoutes();
        LayoutInflater li = LayoutInflater.from(MainActivity.this);
        final View view = li.inflate(R.layout.start_point_alert, null);
        final android.app.AlertDialog.Builder alertDialogBuilder = new android.app.AlertDialog.Builder(MainActivity.this);
        TextView sitename = view.findViewById(R.id.site_name);
        TextView location_name = view.findViewById(R.id.location_name);
        TextView title = view.findViewById(R.id.title);
        Button accept = view.findViewById(R.id.accept);
        final Button ignore = view.findViewById(R.id.ignore);
        final LatLon latLon = new LatLon();
        latLon.Lat = location.getLatitude();
        latLon.Lon = location.getLongitude();
        lat = location.getLatitude();
        longt = location.getLongitude();
        title.setText("AUTO DETECTION");

        List<Address> addresses;
        Geocoder geocoder = new Geocoder(MainActivity.this, Locale.getDefault());
        try {
            addresses = geocoder.getFromLocation(lat, longt, 1);
            location_names = addresses.get(0).getAddressLine(0);
            // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
        } catch (IOException e) {
            e.printStackTrace();
        }
        sitename.setText("Start point");
        location_name.setText(location_names);
        alertDialogBuilder.setView(view);
        final android.app.AlertDialog alertDialogCongratulations = alertDialogBuilder.create();
        alertDialogCongratulations.getWindow().setLayout(LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);//
        alertDialogCongratulations.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        alertDialogCongratulations.setCancelable(false);
        alertDialogCongratulations.show();

        accept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Routes item = null;
                item = routes.get(0);
                item.setPoi_type(50);
                Toast.makeText(getApplicationContext(), "Startpoint", Toast.LENGTH_LONG).show();
                int count = 0;
                UUID uuid = UUID.randomUUID();
                final String poi_id = uuid.toString();
                Date currentTime = Calendar.getInstance().getTime();
                ArrayList<SdrStop> Stops = TruckerDataManager.getInstance().getSdrReport().Hierarchy.GIS.Stops;
                ArrayList<SdrPointOfInterest> Poi = TruckerDataManager.getInstance().getSdrReport().Hierarchy.GIS.PointsOfInterest;
                ArrayList<StopListItem> stopListItems = TruckerDataManager.getInstance().getStopListItems();
                int counter_skip = 0;

                for (int k = 0; k < Stops.size() + Poi.size(); k++) {
                    final Routes skip = routes.get(k);
                    if (skip.getSite_desc().contains("Stop")) {
                        if (skip.getArrival_time() != null) {
                            counter_skip++;
                        }
                    } else if (skip.getSite_desc().contains("Poi")) {
                        if (skip.getArrival_time() != null) {
                            counter_skip++;
                        }
                    }
                }
                if (count == 0) {
                    routes.add(new Routes("Start Point", 0, location_names, "Poi", currentTime, null,
                            null, lat, longt, 0, 50.0f, 50, poi_id));
                    count = 1;
                    StopListItem newpoi = new StopListItem(poi_id,
                            "Start Point",
                            location_names,
                            currentTime,
                            null,
                            DeliveryPOITypeEnum.DeliveryPOITypesEnum.setIntValue(50),
                            latLon,
                            50.0f,
                            null,
                            false, null);
                    stopListItems.add(newpoi);
                    Collections.sort(routes.subList(1 + counter_skip, routes.size()), new Comparator<Routes>() {
                        @Override
                        public int compare(Routes routes, Routes t1) {
                            if (t1.getArrival_time() == null) {
                                return (routes.getArrival_time() == null) ? 0 : -1;
                            }
                            if (routes.getArrival_time() == null) {
                                return 1;
                            }
                            return routes.getArrival_time().compareTo(t1.getArrival_time());
                        }
                    });
                    truckerDataManager.setRoutes(routes);
                }
                alertDialogCongratulations.dismiss();
            }
        });
        ignore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alertDialogCongratulations.dismiss();
                arrival = 0;
            }
        });

        new CountDownTimer(10000, 1000) {
            public void onTick(long millisUntilFinished) {
            }

            public void onFinish() {
                alertDialogCongratulations.dismiss();
                arrival = 0;
            }
        }.start();
    }

    public void checkstartpoint_location(){
        Location location = LocationService.getInstance().getLastKnownLocation();

        ArrayList<Routes> routes = TruckerDataManager.getInstance().getRoutes();
        CircleOptions c = new CircleOptions();
        float[] distance = new float[2];
            Routes item = routes.get(0);
                LatLng latLng = new LatLng(item.getLat(), item.getLongt());
                c.center(latLng);
                c.radius(item.getGeofenceRadius());
                Location.distanceBetween(location.getLatitude(), location.getLongitude(), c.getCenter().latitude, c.getCenter().longitude, distance);
                if (distance[0] > c.getRadius()) {
//                             Toast.makeText(getApplicationContext(), "outside", Toast.LENGTH_LONG).show();
//                             System.out.println("OUTSIDE");
                    item = routes.get(0);
                    if (item.getDeparture_time() == null && item.getPoi_type() != 50 && item.getArrival_time() == null) {
                        if (start_point == 0) {
                            startpoint_alert(location);
                            start_point = 1;
                        }
                    }
                } else {
                    LocationService.getInstance().stopUnscheduledStopTimer();
                }
        }

}
