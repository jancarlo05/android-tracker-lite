package com.procuro.androidtruckerlite;

import com.procuro.apimmdatamanagerlib.AddressComponents;
import com.procuro.apimmdatamanagerlib.DeliveryPOITypeEnum;
import com.procuro.apimmdatamanagerlib.JSONDate;
import com.procuro.apimmdatamanagerlib.LatLon;
import com.procuro.apimmdatamanagerlib.PimmBaseObject;

import org.json.JSONObject;

import java.util.Date;

public class StopListItem extends PimmBaseObject {
    public String siteID;
    public String siteName;
    public String address;
    public Date arrivalTime;
    public Date departureTime;
    public DeliveryPOITypeEnum.DeliveryPOITypesEnum type;
    public LatLon latlon;
    public float geofenceRadius;
    public AddressComponents addressComponents;
    public boolean verified;
    public Date sched_arrival_time;

    public StopListItem(String siteID,
                        String siteName,
                        String address,
                        Date arrivalTime,
                        Date departureTime,
                        DeliveryPOITypeEnum.DeliveryPOITypesEnum type,
                        LatLon latlon,
                        float geofenceRadius,
                        AddressComponents addressComponents,
                        boolean verified,
                        Date sched_arrival_time) {
        this.siteID = siteID;
        this.siteName = siteName;
        this.address = address;
        this.arrivalTime = arrivalTime;
        this.departureTime = departureTime;
        this.type = type;
        this.latlon = latlon;
        this.geofenceRadius = geofenceRadius;
        this.addressComponents = addressComponents;
        this.verified = verified;
        this.sched_arrival_time = sched_arrival_time;
    }

    @Override
    public void setValueForKey(Object value, String key) {
        if (key.equalsIgnoreCase("arrivalTime")) {
            this.arrivalTime = JSONDate.convertJSONDateToNSDate(value.toString());
        } else if (key.equalsIgnoreCase("departureTime")) {
            this.departureTime = JSONDate.convertJSONDateToNSDate(value.toString());
        } else {
            super.setValueForKey(value, key);
        }
    }
}
