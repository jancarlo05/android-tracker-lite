package com.procuro.androidtruckerlite;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;

import androidx.fragment.app.Fragment;

public class Profile_Vehicle_trailer_fragment extends Fragment {

    View rootView;
    RadioButton tractor,trailer;


    public Profile_Vehicle_trailer_fragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.vehicle_trailer_fragment, container, false);

        return rootView;
    }
}