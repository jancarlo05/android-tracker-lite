package com.procuro.androidtruckerlite;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

public class Profile_edit_tractor extends AppCompatActivity {

    ImageButton back , save;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.profile_edit_vehicle_tractor);




        back = findViewById(R.id.back);
        save = findViewById(R.id.save);

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(Profile_edit_tractor.this, Profile.class);
                startActivity(intent);
                finish();

            }
        });
        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(Profile_edit_tractor.this, Profile.class);
                startActivity(intent);
                finish();
                Toast.makeText(Profile_edit_tractor.this, "Saved !", Toast.LENGTH_SHORT).show();

            }
        });


    }
}
